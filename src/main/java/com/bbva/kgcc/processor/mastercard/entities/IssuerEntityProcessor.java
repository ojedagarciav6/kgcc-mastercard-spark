package com.bbva.kgcc.processor.mastercard.entities;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Row;

import com.bbva.kgcc.mastercard.entities.AcceptanceBrandType;
import com.bbva.kgcc.mastercard.entities.AccountEntityType;
import com.bbva.kgcc.mastercard.entities.AccountInformation4300Type;
import com.bbva.kgcc.mastercard.entities.AddressTypeCodeType;
import com.bbva.kgcc.mastercard.entities.AuthorizationLimits4430Type;
import com.bbva.kgcc.mastercard.entities.BillingType;
import com.bbva.kgcc.mastercard.entities.CorporateEntityType;
import com.bbva.kgcc.mastercard.entities.CorporateInformation4000Type;
import com.bbva.kgcc.mastercard.entities.CycleType;
import com.bbva.kgcc.mastercard.entities.HierarchyAddress4410Type;
import com.bbva.kgcc.mastercard.entities.HierarchyRecordHeaderType;
import com.bbva.kgcc.mastercard.entities.HierarchyStatusCodeType;
import com.bbva.kgcc.mastercard.entities.IssuerEntityType;
import com.bbva.kgcc.mastercard.entities.IssuerInformation3000Type;
import com.bbva.kgcc.mastercard.entities.Portfolio4420Type;
import com.bbva.kgcc.mastercard.entities.TrueFalseType;
import com.bbva.kgcc.visa.entities.VisaMergeEntity;

/**
 * Clase con funciones para rellenar los diferentes bloques del Issuer Entity
 */

public class IssuerEntityProcessor {

	public static IssuerEntityType getIssuerEntityRow(Row row, VisaMergeEntity rowVisaM) {

		// Bloque principal
		IssuerEntityType issuerEntity = new IssuerEntityType();

		// Bloque anidado dentro del anterior
		HierarchyRecordHeaderType hierarchyHeader = new HierarchyRecordHeaderType();

		// Rellenamos el objeto hierarchyHeader - Sacarlo a función
		hierarchyHeader.setSequenceNum(rowVisaM.nextSequence());
		hierarchyHeader.setStatusCode(HierarchyStatusCodeType.A);

		// Subbloque de la entidad principal IssuerEntityType
		IssuerInformation3000Type issuerInformation = getIssuerInformation3000Block(row, hierarchyHeader);

		// Subbloque de la entidad principal IssuerEntityType
		
		// Obtenemos el bloque a partir de cada fila (Podemos devolver la lista
		// directamente)
		List<HierarchyAddress4410Type> hierarchyAddress = getHierarchyAddressBlocks(row, rowVisaM);

		// Subbloque de la entidad principal IssuerEntityType
		//List<CorporateEntityType> corporateEntity = getCorporateEntityIssuer(row, rowVisaM);

		// Seteamos la entidad principal IssuerEntityType con su subloques

		issuerEntity.setIssuerInformation3000(issuerInformation);
		issuerEntity.setHierarchyAddress4410(hierarchyAddress);
		//issuerEntity.setCorporateEntity(corporateEntity);

		return issuerEntity;
	}

	public static List<HierarchyAddress4410Type> getHierarchyAddressBlocks(Row row, VisaMergeEntity rowVisaM) {

		List<HierarchyAddress4410Type> listHierarchyAddress = new ArrayList<HierarchyAddress4410Type>();
		
		HierarchyAddress4410Type hierarchyAddress = new HierarchyAddress4410Type();

		// Habria que sacar a función
		HierarchyRecordHeaderType hierarchyRecordHeader = new HierarchyRecordHeaderType();
		hierarchyRecordHeader.setSequenceNum(rowVisaM.nextSequence());
		hierarchyRecordHeader.setStatusCode(HierarchyStatusCodeType.A);

		// Datos van mockeados para las pruebas
		hierarchyAddress.setHierarchyRecordHeader(hierarchyRecordHeader);
		hierarchyAddress.setLocaleCode("ENU");
		hierarchyAddress.setPrimaryAddressIndicator(TrueFalseType.Y);
		hierarchyAddress.setAddressTypeCode(AddressTypeCodeType.OTR);
		hierarchyAddress.setAddressLine(row.getString(38));//"DES_COM_ADL1 -  Address Line1 (T6) - String(100)"
		hierarchyAddress.setAddressLine2(row.getString(39));//"DES_COM_ADL2 -  Address Line2 (T6) - String(100)"
		hierarchyAddress.setCity(row.getString(40));//DES_CITY_C - City (T6) 
		hierarchyAddress.setStateProvince(row.getString(41));//COD_STATE_C - State / ProviceCode (T6) 
		hierarchyAddress.setCountryCode(row.getString(42));//COD_ISO_CTRC - ISO Country Code (T6)
		hierarchyAddress.setPostalCode(row.getString(43));//COD_COM_POST
		hierarchyAddress.setEMailAddress(row.getString(44));//DES_EMAIL_C
		
		listHierarchyAddress.add(hierarchyAddress);

		return listHierarchyAddress;
	}

	public static List<CorporateEntityType> getCorporateEntityIssuer(Row row, VisaMergeEntity rowVisaM) {

		List<CorporateEntityType> listCorporateEntity = new ArrayList<CorporateEntityType>();

		CorporateEntityType corporateEntity = new CorporateEntityType();

		// Hay que obtener el objeto CorporateInformation

		CorporateInformation4000Type corporateInformation = getCorporateInformation4000Issuer(row, rowVisaM);

		// Vamos a obtener el portfolio

		Portfolio4420Type portfolioCorporate = getPortFolioCorporate(row, rowVisaM);
		
		//Vamos a obtener el Authorization Corporate
		
		//AuthorizationLimits4430Type authorizationLimit = getAuthorizationLimitCorporate(row, rowVisaM);
		
		
		// VAmos a obtener del Account List del Corporate
		List<AccountEntityType> accountsCorporate = getListAccountCorporate(row);

		// Asignamos a Corporate Entity

		corporateEntity.setCorporateInformation4000(corporateInformation);
		corporateEntity.setPortfolio4420(portfolioCorporate);
		
		corporateEntity.setCorporationNumber(null);
		

		listCorporateEntity.add(corporateEntity);

		return listCorporateEntity;

	}

	public static CorporateInformation4000Type getCorporateInformation4000Issuer(Row row, VisaMergeEntity rowVisaM) {

		CorporateInformation4000Type corporateInformation = new CorporateInformation4000Type();

		// Hay que sacar este header a una función para controlar la secuencia
		HierarchyRecordHeaderType hierarchyRecordHeader = new HierarchyRecordHeaderType();
		hierarchyRecordHeader.setSequenceNum(rowVisaM.nextSequence());
		hierarchyRecordHeader.setStatusCode(HierarchyStatusCodeType.A);
		corporateInformation.setHierarchyRecordHeader(hierarchyRecordHeader);
		corporateInformation.setAllowMasterCardTaxEstimation(TrueFalseType.Y);
		corporateInformation.setBillingType(BillingType.C);
		corporateInformation.setCycle(CycleType.W);
		corporateInformation.setBillingCurrencyCode(row.get(17).toString());//COD_BILL_CUR (T1)
		corporateInformation.setUseSmartDataNextGeneration(TrueFalseType.N);

		return corporateInformation;
	}

	public static IssuerInformation3000Type getIssuerInformation3000Block(Row company, HierarchyRecordHeaderType hierarchyHeader) {

		IssuerInformation3000Type issuerInformation = new IssuerInformation3000Type();

		issuerInformation.setHierarchyRecordHeader(hierarchyHeader);
		issuerInformation.setNameLocaleCode("ENU");//TODO constante
		issuerInformation.setNameLine1(company.get(45).toString()); //DES_ISSU_NM - Issuer Name (T6) - String(100) Primera línea de nombre * Idioma nacional aceptable
		issuerInformation.setPostedCurrencyCode(company.get(17).toString());


		return issuerInformation;
	}

	public static Portfolio4420Type getPortFolioCorporate(Row row, VisaMergeEntity rowVisaM) {
		
		Portfolio4420Type portfolioCorporate = new Portfolio4420Type();
		
		
		// Habria que sacar a función
		HierarchyRecordHeaderType hierarchyRecordHeader = new HierarchyRecordHeaderType();
		hierarchyRecordHeader.setSequenceNum(rowVisaM.nextSequence());
		hierarchyRecordHeader.setStatusCode(HierarchyStatusCodeType.A);
		portfolioCorporate.setHierarchyRecordHeader(hierarchyRecordHeader);
		portfolioCorporate.setPortfolioStatementDate(LocalDate.now());//T3 -QNU_EFFEC_DT
		return portfolioCorporate;
		
	}
	
	public static AuthorizationLimits4430Type getAuthorizationLimitCorporate(Row row, VisaMergeEntity rowVisaM) {
		
		AuthorizationLimits4430Type authorizationLimit = new AuthorizationLimits4430Type();
		
		// Habria que sacar a función
		HierarchyRecordHeaderType hierarchyRecordHeader = new HierarchyRecordHeaderType();
		hierarchyRecordHeader.setSequenceNum(rowVisaM.nextSequence());
		hierarchyRecordHeader.setStatusCode(HierarchyStatusCodeType.A);
		
		// Seteamos el Authorization Limit
		authorizationLimit.setHierarchyRecordHeader(hierarchyRecordHeader);
		authorizationLimit.setDailyNumLimitOfTransactions(null);
		authorizationLimit.setSingleTransactionAmountLimit(null);
		authorizationLimit.setDailyTransactionAmountLimit(null);
		authorizationLimit.setCycleNumOfTransactionsLimit(null);
		authorizationLimit.setCycleAmountLimit(null);
		authorizationLimit.setMonthlyNumLimitOfTransactions(null);
		authorizationLimit.setMonthlyAmountLimit(null);
		authorizationLimit.setOtherNumOfTransactionsLimit(null);
		authorizationLimit.setOtherAmountLimit(null);
		authorizationLimit.setOtherNumOfDays(null);
		authorizationLimit.setOtherNextRefreshDate(null);
		authorizationLimit.setQuarterlyNumTransactions(null);
		authorizationLimit.setQuarterlyAmountTransactions(null);
		authorizationLimit.setYearlyNumTransactions(null);
		authorizationLimit.setYearlyAmountTransactions(null);
		authorizationLimit.setFiscalYearBegin(null);
		authorizationLimit.setBeginEffectiveDate(null);
		authorizationLimit.setEndEffectiveDate(null);
		authorizationLimit.setUseParentLimitsFlag(null);
		authorizationLimit.setElectronicCommerceTransactions(null);
		authorizationLimit.setCheckIndividualVelocities(null);
		
		
		return authorizationLimit;
	}
	
	public static List<AccountEntityType> getListAccountCorporate(Row row){
		
		List<AccountEntityType> accountsCorporate = new ArrayList<AccountEntityType>();
		AccountEntityType accountCorporate = new AccountEntityType();
		
		accountCorporate.setAccountInformation4300(null);
		accountCorporate.setAuthorizationLimits4430(null);
		accountCorporate.setPortfolio4420(null);
		accountCorporate.setHierarchyAddress4410(null);
		
		accountsCorporate.add(accountCorporate);
		
		
		return accountsCorporate;
	}
	
	public static AccountInformation4300Type accountInf4300(Row account, VisaMergeEntity rowVisaM) {
		
		AccountInformation4300Type accountInformation4300Type = new AccountInformation4300Type();
		HierarchyRecordHeaderType hierarchyRecordHeader = new HierarchyRecordHeaderType();
		
		hierarchyRecordHeader.setSequenceNum(rowVisaM.nextSequence());
		hierarchyRecordHeader.setStatusCode(HierarchyStatusCodeType.A);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		accountInformation4300Type.setHierarchyRecordHeader(hierarchyRecordHeader);
		accountInformation4300Type.setPostedCurrencyCode(account.get(17).toString());//COD_BILL_CUR (T1)  - CCB NUMBER(5)
		accountInformation4300Type.setBillingType(BillingType.C);
		accountInformation4300Type.setAccountTypeCode(account.get(28).toString());//-COD_ACC_TYPE Number(1) 
		accountInformation4300Type.setExpirationDate(LocalDate.parse(account.get(30).toString(),formatter));// QNU_CARD_EXP (T3)
		accountInformation4300Type.setNameLine1(account.getString(32));// DES_FIRST_NM (T4)
		accountInformation4300Type.setReportsTo("");
		accountInformation4300Type.setAcceptanceBrandIdCode(AcceptanceBrandType.MCC);
		accountInformation4300Type.setCorporateProduct(null);//TODO nos lo dan ellos
		accountInformation4300Type.setNameLocaleCode("ENU");
		
		return accountInformation4300Type;
	}

}
