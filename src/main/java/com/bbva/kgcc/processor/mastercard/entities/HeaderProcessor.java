package com.bbva.kgcc.processor.mastercard.entities;

import com.bbva.kgcc.mastercard.entities.RunModeType;
import com.bbva.kgcc.mastercard.entities.TransRecordHeaderType;
import com.bbva.kgcc.mastercard.entities.TransmissionHeader1000Type;
import com.bbva.kgcc.visa.entities.VisaMergeEntity;

public class HeaderProcessor {
	
	// Funcion para rellenar los campos del header - Algunos serán constantes
	public static TransmissionHeader1000Type getTransmissionHeader1000(VisaMergeEntity row) {
		
		TransRecordHeaderType transRecordHeader = new TransRecordHeaderType();
		transRecordHeader.setSequenceNum(row.nextSequence());
		
		TransmissionHeader1000Type header = new TransmissionHeader1000Type();
		header.setTransRecordHeader(transRecordHeader);
		header.setFileReferenceNum(row.getCodFileId());
		header.setCDFVersionNum("3.00");//TODO contante
		header.setRunModeIndicator(RunModeType.V);
		header.setProcessorNum("1111");//TODO nos lo da MC?
		header.setSchemaVersionNum("17.0.0.1");//TODO contante
		
		return header;
	}

}
