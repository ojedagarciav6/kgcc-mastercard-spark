package com.bbva.kgcc.processor.mastercard.entities;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.spark.sql.Row;

import com.bbva.kgcc.mastercard.entities.CardAcceptor5001Type;
import com.bbva.kgcc.mastercard.entities.CurrencyAmountType;
import com.bbva.kgcc.mastercard.entities.FinancialRecordHeaderType;
import com.bbva.kgcc.mastercard.entities.FinancialTransaction5000Type;
import com.bbva.kgcc.mastercard.entities.FinancialTransactionEntityType;
import com.bbva.kgcc.mastercard.entities.LineItemDetailEntityType;
import com.bbva.kgcc.mastercard.entities.SignCodeType;
import com.bbva.kgcc.mastercard.entities.TransactionStatusCodeType;
import com.bbva.kgcc.visa.entities.VisaMergeEntity;

public class TransactionProcessor {

	public static FinancialTransactionEntityType getFinancialTransactionMastercard(Row row, VisaMergeEntity rowVisaM) {

		FinancialTransactionEntityType financialTransaction = new FinancialTransactionEntityType();

		// Get FinancialTransaction5000Type

		FinancialTransaction5000Type financialTransaction5000 = getFinancialTransaction5000(row, rowVisaM);

		// Get CardAcceptor5001Type

		CardAcceptor5001Type cardAcceptor = getCardAcceptorTransaction(row, rowVisaM);
		
		// Seteamos todo en el objeto global de transacciones
		financialTransaction.setCardAcceptor5001(cardAcceptor);
		financialTransaction.setFinancialTransaction5000(financialTransaction5000);
		financialTransaction.setProcessorTransactionId(row.getString(2));

		return financialTransaction;
	}

	public static FinancialTransaction5000Type getFinancialTransaction5000(Row row, VisaMergeEntity rowVisaM) {

		FinancialTransaction5000Type financialTransaction5000 = new FinancialTransaction5000Type();

		// <FinancialRecordHeader >
		FinancialRecordHeaderType financialRecordHeader = getFinancialRecordHeader(rowVisaM);

	   //Formato inicial.
       String inputFormat = "yyyyMMdd";
       //Formato deseado.
       String outputFormat = "yyyy-MM-dd";
          
       String inputDate = row.get(1).toString();
	       
	   String outputDate = inputDate;
	   try {
	        outputDate = new SimpleDateFormat(outputFormat).format(new SimpleDateFormat(inputFormat).parse(inputDate));
	   } catch (Exception e) {
	    System.out.println("formateDateFromstring(): " + e.getMessage());
	            outputDate = "";
	   }
		
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		// Seteamos Header
		financialTransaction5000.setFinancialRecordHeader(financialRecordHeader);
		financialTransaction5000.setProcessorTransactionId(row.getString(2));
		financialTransaction5000.setAcquirerReferenceData(row.get(5).toString());
		financialTransaction5000.setCardHolderTransactionType(row.get(4).toString());
		financialTransaction5000.setPostingDate(LocalDate.parse(outputDate));
		financialTransaction5000.setProcessingDate(LocalDate.now());
		financialTransaction5000.setTransactionDate(LocalDate.parse(row.get(18).toString(),formatter));
		financialTransaction5000.setDebitOrCreditIndicator(SignCodeType.D);
		financialTransaction5000.setAmountInOriginalCurrency(new CurrencyAmountType(row.getLong(12), "840", (short) 2, SignCodeType.C));
		financialTransaction5000.setAmountInPostedCurrency(new CurrencyAmountType(row.getLong(13), "840", (short) 2, SignCodeType.C));
		financialTransaction5000.setOriginalCurrencyCode("840");
		financialTransaction5000.setPostedCurrencyCode(row.get(17).toString());
		financialTransaction5000.setPostedConversionRate(null);//TODO conversiones
		financialTransaction5000.setPostedCurrencyConversionDate(null);//TODO conversiones
		
		// Faltan mas campos

		return financialTransaction5000;
	}

	public static CardAcceptor5001Type getCardAcceptorTransaction(Row row, VisaMergeEntity rowVisaM) {
		CardAcceptor5001Type cardAcceptor = new CardAcceptor5001Type();
		// <FinancialRecordHeader >
		FinancialRecordHeaderType financialRecordHeader = getFinancialRecordHeader(rowVisaM);

		// Seteamos RecordHeader y resto de campos
		cardAcceptor.setFinancialRecordHeader(financialRecordHeader);
		cardAcceptor.setAcquiringICA(row.get(5).toString());
		cardAcceptor.setCardAcceptorName(row.getString(7));
		cardAcceptor.setCardAcceptorStreetAddress(null);//TODO direccion empresa??
		cardAcceptor.setCardAcceptorCity(row.getString(8));
		cardAcceptor.setCardAcceptorStateProvince(row.getString(9));
		cardAcceptor.setCardAcceptorLocationPostalCode(row.getString(11));
		cardAcceptor.setCardAcceptorCountryCode(row.getString(10));		
		cardAcceptor.setCardAcceptorBusinessCode(null);//TODO MCC??????
		
		return cardAcceptor;
	}

	public static List<LineItemDetailEntityType> getLineItemTransaction(Row row) {
		return null;
	}

	public static FinancialRecordHeaderType getFinancialRecordHeader(VisaMergeEntity rowVisaM) {
		FinancialRecordHeaderType recordHeaderTransaction = new FinancialRecordHeaderType();

		recordHeaderTransaction.setSequenceNum(rowVisaM.nextSequence());
		recordHeaderTransaction.setMaintenanceCode(TransactionStatusCodeType.A);

		return recordHeaderTransaction;
	}

}
