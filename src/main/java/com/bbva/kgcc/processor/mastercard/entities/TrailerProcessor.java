package com.bbva.kgcc.processor.mastercard.entities;

import com.bbva.kgcc.mastercard.entities.TransRecordHeaderType;
import com.bbva.kgcc.mastercard.entities.TransmissionTrailer9999Type;
import com.bbva.kgcc.visa.entities.VisaMergeEntity;

public class TrailerProcessor {

	// Función para rellenar el trailer - Algun dato mockeado por tema de pruebas
	public static TransmissionTrailer9999Type getTransmissionTrailer_9999(VisaMergeEntity row) {
		
		TransmissionTrailer9999Type trailer = new TransmissionTrailer9999Type();
		TransRecordHeaderType transRecordHeaderTrailer = new TransRecordHeaderType();
		transRecordHeaderTrailer.setSequenceNum(row.nextSequence());
		trailer.setTransRecordHeader(transRecordHeaderTrailer);
		return trailer;
	}
}
