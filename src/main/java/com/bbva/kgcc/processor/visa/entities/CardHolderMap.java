package com.bbva.kgcc.processor.visa.entities;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import com.bbva.kgcc.visa.blocks.BlockCardHolder;
import com.bbva.kgcc.visa.entities.CardHolder;

public class CardHolderMap {
	
	public static BlockCardHolder procesamientoLineaEntrante(Dataset<Row> cardHolders) {
		
		List<Row> cardHolderList = cardHolders.collectAsList();
		List<CardHolder> cardHolderEntities = new ArrayList<CardHolder>();
		
		
		
		for(Row cardHolder : cardHolderList) {
			String optionalField4 = cardHolder.get(34).toString().trim();
			String optionalField3 = cardHolder.get(33).toString().trim();
			String optionalField2 = cardHolder.get(32).toString().trim();
			String optionalField1 = cardHolder.get(31).toString().trim();
			String miscField2DS = cardHolder.get(30).toString().trim();
			String miscField2 = cardHolder.get(29).toString().trim();
			String miscField1DS = cardHolder.get(28).toString().trim();
			String miscField1 = cardHolder.get(27).toString().trim();
			String codVehicle = cardHolder.get(26).toString().trim();
			String visaBuyer = cardHolder.get(25).toString().trim();
			String middleName = cardHolder.get(24).toString().trim();
			String homePhone = cardHolder.get(23).toString().trim();
			String codEmployee = cardHolder.get(22).toString().trim();
			String authorizduser3 = cardHolder.get(21).toString().trim();
			String authorizduser2 = cardHolder.get(20).toString().trim();
			String authorizduser1 = cardHolder.get(19).toString().trim();
			String emailAddress = cardHolder.get(18).toString().trim();
			Long trainingDate = cardHolder.get(17).toString().trim().equals("") || cardHolder.get(17).toString().trim().isEmpty() ? null : Long.valueOf(cardHolder.get(17).toString().trim());
			String codCardHolderOTH = cardHolder.get(16).toString().trim();
			String faxNumber = cardHolder.get(15).toString().trim();
			String phoneNumber = cardHolder.get(14).toString().trim();
			String mailStop = cardHolder.get(13).toString().trim();
			String addressLine3 = cardHolder.get(12).toString().trim();
			String codPostal = cardHolder.get(11).toString().trim();
			String codISOCountry = cardHolder.get(10).toString().trim().equals("") || cardHolder.get(10).toString().trim().isEmpty() ? null : cardHolder.get(10).toString().trim();
			String state = cardHolder.get(9).toString().trim();
			String city = cardHolder.get(8).toString().trim();
			String addressLine2 = cardHolder.get(7).toString().trim();
			String addressLine1 = cardHolder.get(6).toString().trim();
			String lastName = cardHolder.get(5).toString().trim();
			String firstName = cardHolder.get(4).toString().trim();
			String hierarchyNode = cardHolder.get(3).toString().trim();
			String idCardHolder = cardHolder.get(2).toString().trim();
			Long codCompId = Long.valueOf(cardHolder.get(1).toString());
			String codCarga = cardHolder.get(1).toString();
		
			CardHolder cardHolderLines = new CardHolder(codCarga, codCompId, idCardHolder, hierarchyNode, firstName, lastName, addressLine1, addressLine2, city, state, codISOCountry, codPostal, addressLine3, mailStop, phoneNumber, faxNumber, codCardHolderOTH, trainingDate, emailAddress, authorizduser1, authorizduser2, authorizduser3, codEmployee, homePhone, middleName, visaBuyer, codVehicle, miscField1, miscField1DS, miscField2, miscField2DS, optionalField1, optionalField2, optionalField3, optionalField4);
			
			cardHolderEntities.add(cardHolderLines);
		}
		
		BlockCardHolder blockCardHolder = new BlockCardHolder(null, null, cardHolderEntities);
		
		System.out.println("CardHolders List: " + cardHolders);
		
		
		return blockCardHolder ;
		
	}

}
