package com.bbva.kgcc.processor.visa.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import com.bbva.kgcc.utils.Constantes;
import com.bbva.kgcc.visa.blocks.BlockCardHolder;
import com.bbva.kgcc.visa.blocks.BlockPeriod;
import com.bbva.kgcc.visa.blocks.CompanySequenceBlock;
import com.bbva.kgcc.visa.blocks.FileBlock;

public class GlobalMap {

	public static FileBlock procesarSource(final Map<String, Dataset<Row>> dataSetsFromRead) {
		
		//Creamos objeto de Company Sequence - Cuando vengan mas compañias lo vemos
		List<CompanySequenceBlock> companySequenceblockList = new ArrayList<CompanySequenceBlock>();
		CompanySequenceBlock companyBlock = new CompanySequenceBlock();

		/* Sacamos los datos correspondientes a cada uno de los dataSets por archivos*/
		
		Dataset<Row> periodos = dataSetsFromRead.get(Constantes.INPUT_PERIOD);
		Dataset<Row> cardHolders = dataSetsFromRead.get(Constantes.INPUT_CARDHOLDER);
		
		if(periodos != null && !periodos.isEmpty()) {
			
			BlockPeriod blockPeriod = PeriodMap.procesamientoLineaEntrante(periodos); // Función que mapea los datos del Source con nuestra entidad Period
			System.out.println("Block Period: " + blockPeriod);
			companyBlock.setBlockPeriod(blockPeriod);
			
		}
		
		if(cardHolders != null && !cardHolders.isEmpty()) {
			//funcion relativa a los cardHolders
			BlockCardHolder blockCardHolder = CardHolderMap.procesamientoLineaEntrante(cardHolders); // Función que mapea los datos de Source con nuestra entidad CardHolder
			System.out.println("Block CardHolders: " + blockCardHolder);
			companyBlock.setBlockCardHolder(blockCardHolder);
			
		}
		
		companySequenceblockList.add(companyBlock);
		
		FileBlock fileBlock = new FileBlock(companySequenceblockList);

		return fileBlock;
	}
}
