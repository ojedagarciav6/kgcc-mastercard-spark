package com.bbva.kgcc.processor.visa.entities;



import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import com.bbva.kgcc.visa.blocks.BlockPeriod;
import com.bbva.kgcc.visa.entities.Period;

public class PeriodMap {
	
	public static BlockPeriod procesamientoLineaEntrante(Dataset<Row> periodos) {
		
		List<Row> listaPeriodos = periodos.collectAsList();
		
		List<Period> periods = new ArrayList<Period>();
		
		for(Row period : listaPeriodos) {
			
//			String codCountry = period.get(2).toString();
//			String secuencia = period.get(1).toString();
//			String company = period.get(0).toString();
			String codCarga = period.get(0).toString();
			Long codCompId = Long.valueOf(period.get(1).toString());
			Integer codPeriod = Integer.valueOf(period.get(2).toString());
			Integer codCardType = period.get(3).toString().trim().equals("") ? Integer.valueOf(3) : Integer.valueOf(period.get(3).toString());
			// Pasar a funci�n
			Long startDate = Long.parseLong(period.get(4).toString());
			// Pasar a funci�n
			Long endDate = Long.parseLong(period.get(5).toString());
			Integer codPeriodComplet = Integer.valueOf(period.get(6).toString());
			String optionalField1 = period.get(7).toString().trim();
			String optionalField2 = period.get(8).toString().trim();
			String optionalField3 = period.get(9).toString().trim();
			String optionalField4 = period.get(10).toString().trim();
			
			Period periodEntity = new Period(codCarga, codCompId, codPeriod, codCardType, startDate, endDate, codPeriodComplet, optionalField1, optionalField2, optionalField3, optionalField4);
			
			periods.add(periodEntity);
		
		}
		
		BlockPeriod blockPeriod = new BlockPeriod(null, null, periods);
		

		
		return blockPeriod;
		
	}

}
