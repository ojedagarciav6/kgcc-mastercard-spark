package com.bbva.kgcc.mastercard.converters;

import com.bbva.kgcc.mastercard.entities.CurrencyAmountType;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class CurrencyAmountTypeConverter implements Converter {
	
	
	public boolean canConvert(Class clazz) {
	    return clazz.equals(CurrencyAmountType.class);
	}

	public void marshal(Object value, HierarchicalStreamWriter writer,
	        MarshallingContext context) {
		CurrencyAmountType documentation = (CurrencyAmountType) value;
	    writer.addAttribute("CurrencyCode",documentation.getCurrencyCode().toString());
	    writer.addAttribute("CurrencyExponent",documentation.getCurrencyExponent() + "");
	    writer.addAttribute("CurrencySign",documentation.getCurrencySign().toString());
	    writer.setValue(documentation.getValue() + "");
	}

	public Object unmarshal(HierarchicalStreamReader reader,
	        UnmarshallingContext context) {
		CurrencyAmountType documentation = new CurrencyAmountType(Long.parseLong(reader.getValue()));
	    reader.moveDown();
	   /* documentation.setCurrencyCode(reader.getAttribute("CurrencyCode"));
	    documentation.setCurrencyExponent(reader.getAttribute("CurrencyExponent"));
	    documentation.setCurrencySign(reader.getAttribute("CurrencySign"));	 */   
	    documentation.setValue(Long.parseLong(reader.getValue()));
	    reader.moveUp();
	    return documentation;
	}

	}

