package com.bbva.kgcc.mastercard.converters;

import com.bbva.kgcc.mastercard.entities.NumExp16Type;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class CorporateInformation4000Converter implements Converter {
	
	
	public boolean canConvert(Class clazz) {
	    return clazz.equals(NumExp16Type.class);
	}

	public void marshal(Object value, HierarchicalStreamWriter writer,
	        MarshallingContext context) {
		NumExp16Type documentation = (NumExp16Type) value;
	    writer.addAttribute("Exponent",documentation.getExponent());
	    writer.setValue(documentation.getValue());
	}

	public Object unmarshal(HierarchicalStreamReader reader,
	        UnmarshallingContext context) {
		NumExp16Type documentation = new NumExp16Type(reader.getValue());
	    reader.moveDown();
	    documentation.setExponent(reader.getAttribute("Exponent"));
	    documentation.setValue(reader.getValue());
	    reader.moveUp();
	    return documentation;
	}

	}

