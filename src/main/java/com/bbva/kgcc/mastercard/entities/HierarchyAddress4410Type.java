

package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("HierarchyAddress_4410")
public class HierarchyAddress4410Type implements Serializable{

   
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("HierarchyRecordHeader")
	private HierarchyRecordHeaderType hierarchyRecordHeader;
    
	@XStreamAlias("LocaleCode")
	private String localeCode;
    
	@XStreamAlias("PrimaryAddressIndicator")
	private TrueFalseType primaryAddressIndicator;
    
	@XStreamAlias("AddressTypeCode")
	private AddressTypeCodeType addressTypeCode;
    
	@XStreamAlias("AddressNum")
	private short addressNum;
   
	@XStreamAlias("AddressLine")
	private String addressLine;
    
	@XStreamAlias("AddressLine2")
	private String addressLine2;
    
	@XStreamAlias("AddressLine3")
	private String addressLine3;
    
	@XStreamAlias("AddressLine4")
	private String addressLine4;
   
	@XStreamAlias("City")
	private String city;
    
	@XStreamAlias("StateProvince")
	private String stateProvince;
    
	@XStreamAlias("CountryCode")
	private String countryCode;
    
	@XStreamAlias("PostalCode")
	private String postalCode;
    
	@XStreamAlias("ContactName")
	private String contactName;
    
	@XStreamAlias("PhoneNum")
	private String phoneNum;
   
	@XStreamAlias("FaxNum")
	private String faxNum;
    
	@XStreamAlias("EMailAddress")
	private String eMailAddress;

    /**
     * Obtiene el valor de la propiedad hierarchyRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public HierarchyRecordHeaderType getHierarchyRecordHeader() {
        return hierarchyRecordHeader;
    }

    /**
     * Define el valor de la propiedad hierarchyRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public void setHierarchyRecordHeader(HierarchyRecordHeaderType value) {
        this.hierarchyRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad localeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocaleCode() {
        return localeCode;
    }

    /**
     * Define el valor de la propiedad localeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocaleCode(String value) {
        this.localeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryAddressIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getPrimaryAddressIndicator() {
        return primaryAddressIndicator;
    }

    /**
     * Define el valor de la propiedad primaryAddressIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setPrimaryAddressIndicator(TrueFalseType value) {
        this.primaryAddressIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad addressTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link AddressTypeCodeType }
     *     
     */
    public AddressTypeCodeType getAddressTypeCode() {
        return addressTypeCode;
    }

    /**
     * Define el valor de la propiedad addressTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressTypeCodeType }
     *     
     */
    public void setAddressTypeCode(AddressTypeCodeType value) {
        this.addressTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad addressNum.
     * 
     */
    public short getAddressNum() {
        return addressNum;
    }

    /**
     * Define el valor de la propiedad addressNum.
     * 
     */
    public void setAddressNum(short value) {
        this.addressNum = value;
    }

    /**
     * Obtiene el valor de la propiedad addressLine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine() {
        return addressLine;
    }

    /**
     * Define el valor de la propiedad addressLine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine(String value) {
        this.addressLine = value;
    }

    /**
     * Obtiene el valor de la propiedad addressLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * Define el valor de la propiedad addressLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine2(String value) {
        this.addressLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad addressLine3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine3() {
        return addressLine3;
    }

    /**
     * Define el valor de la propiedad addressLine3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine3(String value) {
        this.addressLine3 = value;
    }

    /**
     * Obtiene el valor de la propiedad addressLine4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine4() {
        return addressLine4;
    }

    /**
     * Define el valor de la propiedad addressLine4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine4(String value) {
        this.addressLine4 = value;
    }

    /**
     * Obtiene el valor de la propiedad city.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Define el valor de la propiedad city.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Obtiene el valor de la propiedad stateProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * Define el valor de la propiedad stateProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateProvince(String value) {
        this.stateProvince = value;
    }

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad postalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Define el valor de la propiedad postalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad contactName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Define el valor de la propiedad contactName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Obtiene el valor de la propiedad phoneNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * Define el valor de la propiedad phoneNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNum(String value) {
        this.phoneNum = value;
    }

    /**
     * Obtiene el valor de la propiedad faxNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNum() {
        return faxNum;
    }

    /**
     * Define el valor de la propiedad faxNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNum(String value) {
        this.faxNum = value;
    }

    /**
     * Obtiene el valor de la propiedad eMailAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMailAddress() {
        return eMailAddress;
    }

    /**
     * Define el valor de la propiedad eMailAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMailAddress(String value) {
        this.eMailAddress = value;
    }

}
