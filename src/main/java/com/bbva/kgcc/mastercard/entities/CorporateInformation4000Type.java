//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class CorporateInformation4000Type  implements Serializable{

   
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("HierarchyRecordHeader")
	private HierarchyRecordHeaderType hierarchyRecordHeader;
    
	@XStreamAlias("AllowMasterCardTaxEstimation")
    private TrueFalseType allowMasterCardTaxEstimation;
    
	@XStreamAlias("ProcessingCompanyNum")
    private String processingCompanyNum;
    
	
    private String processingCorporateAccount;
    
	@XStreamAlias("BillingType")
    private BillingType billingType;
    
	@XStreamAlias("Cycle")
    private CycleType cycle;
	
	@XStreamAlias("PostalCode")
    private String cycleDateIndicator;
    
	@XStreamAlias("CycleDateIndicator")
    private String cycleDateIndicator2;
    
	@XStreamAlias("NameLocaleCode")
    private String nameLocaleCode;

	@XStreamAlias("NameLine1")
    private String nameLine1;

	@XStreamAlias("NameLine2")
	private String nameLine2;
   
	
	private String alternateNameLocaleCode;
  

	private String alternateNameLine1;
  

	private String alternateNameLine2;

	@XStreamAlias("BillingCurrencyCode")
	private String billingCurrencyCode;
   
	@XStreamAlias("AnnualFeeControlAccount")
	private String annualFeeControlAccount;
    
	
	private Short annualFeeRenewalMonth;
   
	
	private LocalDate paymentDueDate;
    
	
	private String costCenter;
    
	
	private String internalAccountingCode;
    
	@XStreamAlias("CreditLimit")
	private NumExp16Type creditLimit;
    
	@XStreamAlias("UseSmartDataNextGeneration")
	private TrueFalseType useSmartDataNextGeneration;

    
    
    public CorporateInformation4000Type() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad hierarchyRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public HierarchyRecordHeaderType getHierarchyRecordHeader() {
        return hierarchyRecordHeader;
    }

    /**
     * Define el valor de la propiedad hierarchyRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public void setHierarchyRecordHeader(HierarchyRecordHeaderType value) {
        this.hierarchyRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad allowMasterCardTaxEstimation.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getAllowMasterCardTaxEstimation() {
        return allowMasterCardTaxEstimation;
    }

    /**
     * Define el valor de la propiedad allowMasterCardTaxEstimation.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setAllowMasterCardTaxEstimation(TrueFalseType value) {
        this.allowMasterCardTaxEstimation = value;
    }

    /**
     * Obtiene el valor de la propiedad processingCompanyNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessingCompanyNum() {
        return processingCompanyNum;
    }

    /**
     * Define el valor de la propiedad processingCompanyNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessingCompanyNum(String value) {
        this.processingCompanyNum = value;
    }

    /**
     * Obtiene el valor de la propiedad processingCorporateAccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessingCorporateAccount() {
        return processingCorporateAccount;
    }

    /**
     * Define el valor de la propiedad processingCorporateAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessingCorporateAccount(String value) {
        this.processingCorporateAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad billingType.
     * 
     * @return
     *     possible object is
     *     {@link BillingType }
     *     
     */
    public BillingType getBillingType() {
        return billingType;
    }

    /**
     * Define el valor de la propiedad billingType.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingType }
     *     
     */
    public void setBillingType(BillingType value) {
        this.billingType = value;
    }

    /**
     * Obtiene el valor de la propiedad cycle.
     * 
     * @return
     *     possible object is
     *     {@link CycleType }
     *     
     */
    public CycleType getCycle() {
        return cycle;
    }

    /**
     * Define el valor de la propiedad cycle.
     * 
     * @param value
     *     allowed object is
     *     {@link CycleType }
     *     
     */
    public void setCycle(CycleType value) {
        this.cycle = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleDateIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleDateIndicator() {
        return cycleDateIndicator;
    }

    /**
     * Define el valor de la propiedad cycleDateIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleDateIndicator(String value) {
        this.cycleDateIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleDateIndicator2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleDateIndicator2() {
        return cycleDateIndicator2;
    }

    /**
     * Define el valor de la propiedad cycleDateIndicator2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleDateIndicator2(String value) {
        this.cycleDateIndicator2 = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLocaleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLocaleCode() {
        return nameLocaleCode;
    }

    /**
     * Define el valor de la propiedad nameLocaleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLocaleCode(String value) {
        this.nameLocaleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLine1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine1() {
        return nameLine1;
    }

    /**
     * Define el valor de la propiedad nameLine1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine1(String value) {
        this.nameLine1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine2() {
        return nameLine2;
    }

    /**
     * Define el valor de la propiedad nameLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine2(String value) {
        this.nameLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLocaleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLocaleCode() {
        return alternateNameLocaleCode;
    }

    /**
     * Define el valor de la propiedad alternateNameLocaleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLocaleCode(String value) {
        this.alternateNameLocaleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLine1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLine1() {
        return alternateNameLine1;
    }

    /**
     * Define el valor de la propiedad alternateNameLine1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLine1(String value) {
        this.alternateNameLine1 = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLine2() {
        return alternateNameLine2;
    }

    /**
     * Define el valor de la propiedad alternateNameLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLine2(String value) {
        this.alternateNameLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad billingCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCurrencyCode() {
        return billingCurrencyCode;
    }

    /**
     * Define el valor de la propiedad billingCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCurrencyCode(String value) {
        this.billingCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad annualFeeControlAccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualFeeControlAccount() {
        return annualFeeControlAccount;
    }

    /**
     * Define el valor de la propiedad annualFeeControlAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualFeeControlAccount(String value) {
        this.annualFeeControlAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad annualFeeRenewalMonth.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAnnualFeeRenewalMonth() {
        return annualFeeRenewalMonth;
    }

    /**
     * Define el valor de la propiedad annualFeeRenewalMonth.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAnnualFeeRenewalMonth(Short value) {
        this.annualFeeRenewalMonth = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentDueDate.
     * 
     * @return
     *     possible object is

     *     
     */
    public LocalDate getPaymentDueDate() {
        return paymentDueDate;
    }

    /**
     * Define el valor de la propiedad paymentDueDate.
     * 
     * @param value
     *     allowed object i
     *     
     */
    public void setPaymentDueDate(LocalDate value) {
        this.paymentDueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad costCenter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostCenter() {
        return costCenter;
    }

    /**
     * Define el valor de la propiedad costCenter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostCenter(String value) {
        this.costCenter = value;
    }

    /**
     * Obtiene el valor de la propiedad internalAccountingCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalAccountingCode() {
        return internalAccountingCode;
    }

    /**
     * Define el valor de la propiedad internalAccountingCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalAccountingCode(String value) {
        this.internalAccountingCode = value;
    }

    /**
     * Obtiene el valor de la propiedad creditLimit.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getCreditLimit() {
        return creditLimit;
    }

    /**
     * Define el valor de la propiedad creditLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setCreditLimit(NumExp16Type value) {
        this.creditLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad useSmartDataNextGeneration.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getUseSmartDataNextGeneration() {
        return useSmartDataNextGeneration;
    }

    /**
     * Define el valor de la propiedad useSmartDataNextGeneration.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setUseSmartDataNextGeneration(TrueFalseType value) {
        this.useSmartDataNextGeneration = value;
    }

}
