//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class HierarchyRecordHeaderType implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("SequenceNum")
	private long sequenceNum;
    
	@XStreamAlias("StatusCode")
	private HierarchyStatusCodeType statusCode;
    
	@XStreamAlias("MasterCardRecSeqNum")
	private Long masterCardRecSeqNum;
    
	@XStreamAlias("MasterCardLoadDate")
    private LocalDate masterCardLoadDate;
    
	@XStreamAlias("MasterCardLoadTime")
    private LocalTime masterCardLoadTime;
    
	@XStreamAlias("RejectionReason")
    private String rejectionReason;
   
	@XStreamAlias("RejectionReason2")
    private String rejectionReason2;
    
	@XStreamAlias("RejectionReason3")
    private String rejectionReason3;

    /**
     * Obtiene el valor de la propiedad sequenceNum.
     * 
     */
    public long getSequenceNum() {
        return sequenceNum;
    }

    /**
     * Define el valor de la propiedad sequenceNum.
     * 
     */
    public void setSequenceNum(long value) {
        this.sequenceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad statusCode.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyStatusCodeType }
     *     
     */
    public HierarchyStatusCodeType getStatusCode() {
        return statusCode;
    }

    /**
     * Define el valor de la propiedad statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyStatusCodeType }
     *     
     */
    public void setStatusCode(HierarchyStatusCodeType value) {
        this.statusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardRecSeqNum.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMasterCardRecSeqNum() {
        return masterCardRecSeqNum;
    }

    /**
     * Define el valor de la propiedad masterCardRecSeqNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMasterCardRecSeqNum(Long value) {
        this.masterCardRecSeqNum = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardLoadDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getMasterCardLoadDate() {
        return masterCardLoadDate;
    }

    /**
     * Define el valor de la propiedad masterCardLoadDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setMasterCardLoadDate(LocalDate value) {
        this.masterCardLoadDate = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardLoadTime.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalTime getMasterCardLoadTime() {
        return masterCardLoadTime;
    }

    /**
     * Define el valor de la propiedad masterCardLoadTime.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setMasterCardLoadTime(LocalTime value) {
        this.masterCardLoadTime = value;
    }

    /**
     * Obtiene el valor de la propiedad rejectionReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * Define el valor de la propiedad rejectionReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRejectionReason(String value) {
        this.rejectionReason = value;
    }

    /**
     * Obtiene el valor de la propiedad rejectionReason2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRejectionReason2() {
        return rejectionReason2;
    }

    /**
     * Define el valor de la propiedad rejectionReason2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRejectionReason2(String value) {
        this.rejectionReason2 = value;
    }

    /**
     * Obtiene el valor de la propiedad rejectionReason3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRejectionReason3() {
        return rejectionReason3;
    }

    /**
     * Define el valor de la propiedad rejectionReason3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRejectionReason3(String value) {
        this.rejectionReason3 = value;
    }

}
