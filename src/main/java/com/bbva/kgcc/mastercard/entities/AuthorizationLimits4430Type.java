//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class AuthorizationLimits4430Type implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("HierarchyRecordHeader")
	private HierarchyRecordHeaderType hierarchyRecordHeader;
    
	@XStreamAlias("DailyNumLimitOfTransactions")
    private Long dailyNumLimitOfTransactions;
   
	@XStreamAlias("SingleTransactionAmountLimit")
	private CurrencyAmountType singleTransactionAmountLimit;
    
	@XStreamAlias("DailyTransactionAmountLimit")
	private CurrencyAmountType dailyTransactionAmountLimit;
    
	@XStreamAlias("CycleNumOfTransactionsLimit")
    private Long cycleNumOfTransactionsLimit;
    
	@XStreamAlias("CycleAmountLimit")
	private CurrencyAmountType cycleAmountLimit;

	@XStreamAlias("MonthlyNumLimitOfTransactions")
	private Long monthlyNumLimitOfTransactions;
    
	@XStreamAlias("MonthlyAmountLimit")
	private CurrencyAmountType monthlyAmountLimit;
    
	@XStreamAlias("OtherNumOfTransactionsLimit")
	private Long otherNumOfTransactionsLimit;
    
	@XStreamAlias("OtherAmountLimit")
	private CurrencyAmountType otherAmountLimit;
    
	@XStreamAlias("OtherNumOfDays")
	private Integer otherNumOfDays;

	@XStreamAlias("OtherNextRefreshDate")
	private LocalDate otherNextRefreshDate;
    
    @XStreamAlias("QuarterlyNumTransactions")
    private Long quarterlyNumTransactions;
    
    @XStreamAlias("QuarterlyAmountTransactions")
    private CurrencyAmountType quarterlyAmountTransactions;
    
    @XStreamAlias("YearlyNumTransactions")
    private Long yearlyNumTransactions;
    
    @XStreamAlias("YearlyAmountTransactions")
    private CurrencyAmountType yearlyAmountTransactions;
    
    @XStreamAlias("FiscalYearBegin")
    private String fiscalYearBegin;
   
    @XStreamAlias("BeginEffectiveDate")
    private LocalDate beginEffectiveDate;
    
    @XStreamAlias("EndEffectiveDate")
    private LocalDate endEffectiveDate;
    
    @XStreamAlias("UseParentLimitsFlag")
    private TrueFalseType useParentLimitsFlag;
   
    @XStreamAlias("UseParentVelocitiesFlag")
    private TrueFalseType useParentVelocitiesFlag;
    
    @XStreamAlias("ElectronicCommerceTransactions")
    private String electronicCommerceTransactions;
    
    @XStreamAlias("CheckIndividualVelocities")
    private String checkIndividualVelocities;
    
    private TrueFalseType useMCCGFlag;
    
    private TrueFalseType useParentMCCGFlag;

    /**
     * Obtiene el valor de la propiedad hierarchyRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public HierarchyRecordHeaderType getHierarchyRecordHeader() {
        return hierarchyRecordHeader;
    }

    /**
     * Define el valor de la propiedad hierarchyRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public void setHierarchyRecordHeader(HierarchyRecordHeaderType value) {
        this.hierarchyRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad dailyNumLimitOfTransactions.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDailyNumLimitOfTransactions() {
        return dailyNumLimitOfTransactions;
    }

    /**
     * Define el valor de la propiedad dailyNumLimitOfTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDailyNumLimitOfTransactions(Long value) {
        this.dailyNumLimitOfTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad singleTransactionAmountLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getSingleTransactionAmountLimit() {
        return singleTransactionAmountLimit;
    }

    /**
     * Define el valor de la propiedad singleTransactionAmountLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setSingleTransactionAmountLimit(CurrencyAmountType value) {
        this.singleTransactionAmountLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad dailyTransactionAmountLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getDailyTransactionAmountLimit() {
        return dailyTransactionAmountLimit;
    }

    /**
     * Define el valor de la propiedad dailyTransactionAmountLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setDailyTransactionAmountLimit(CurrencyAmountType value) {
        this.dailyTransactionAmountLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleNumOfTransactionsLimit.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCycleNumOfTransactionsLimit() {
        return cycleNumOfTransactionsLimit;
    }

    /**
     * Define el valor de la propiedad cycleNumOfTransactionsLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCycleNumOfTransactionsLimit(Long value) {
        this.cycleNumOfTransactionsLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleAmountLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getCycleAmountLimit() {
        return cycleAmountLimit;
    }

    /**
     * Define el valor de la propiedad cycleAmountLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setCycleAmountLimit(CurrencyAmountType value) {
        this.cycleAmountLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad monthlyNumLimitOfTransactions.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMonthlyNumLimitOfTransactions() {
        return monthlyNumLimitOfTransactions;
    }

    /**
     * Define el valor de la propiedad monthlyNumLimitOfTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMonthlyNumLimitOfTransactions(Long value) {
        this.monthlyNumLimitOfTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad monthlyAmountLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getMonthlyAmountLimit() {
        return monthlyAmountLimit;
    }

    /**
     * Define el valor de la propiedad monthlyAmountLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setMonthlyAmountLimit(CurrencyAmountType value) {
        this.monthlyAmountLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad otherNumOfTransactionsLimit.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOtherNumOfTransactionsLimit() {
        return otherNumOfTransactionsLimit;
    }

    /**
     * Define el valor de la propiedad otherNumOfTransactionsLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOtherNumOfTransactionsLimit(Long value) {
        this.otherNumOfTransactionsLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad otherAmountLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getOtherAmountLimit() {
        return otherAmountLimit;
    }

    /**
     * Define el valor de la propiedad otherAmountLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setOtherAmountLimit(CurrencyAmountType value) {
        this.otherAmountLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad otherNumOfDays.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOtherNumOfDays() {
        return otherNumOfDays;
    }

    /**
     * Define el valor de la propiedad otherNumOfDays.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOtherNumOfDays(Integer value) {
        this.otherNumOfDays = value;
    }

    /**
     * Obtiene el valor de la propiedad otherNextRefreshDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getOtherNextRefreshDate() {
        return otherNextRefreshDate;
    }

    /**
     * Define el valor de la propiedad otherNextRefreshDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setOtherNextRefreshDate(LocalDate value) {
        this.otherNextRefreshDate = value;
    }

    /**
     * Obtiene el valor de la propiedad quarterlyNumTransactions.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getQuarterlyNumTransactions() {
        return quarterlyNumTransactions;
    }

    /**
     * Define el valor de la propiedad quarterlyNumTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setQuarterlyNumTransactions(Long value) {
        this.quarterlyNumTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad quarterlyAmountTransactions.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getQuarterlyAmountTransactions() {
        return quarterlyAmountTransactions;
    }

    /**
     * Define el valor de la propiedad quarterlyAmountTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setQuarterlyAmountTransactions(CurrencyAmountType value) {
        this.quarterlyAmountTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad yearlyNumTransactions.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getYearlyNumTransactions() {
        return yearlyNumTransactions;
    }

    /**
     * Define el valor de la propiedad yearlyNumTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setYearlyNumTransactions(Long value) {
        this.yearlyNumTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad yearlyAmountTransactions.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getYearlyAmountTransactions() {
        return yearlyAmountTransactions;
    }

    /**
     * Define el valor de la propiedad yearlyAmountTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setYearlyAmountTransactions(CurrencyAmountType value) {
        this.yearlyAmountTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad fiscalYearBegin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiscalYearBegin() {
        return fiscalYearBegin;
    }

    /**
     * Define el valor de la propiedad fiscalYearBegin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiscalYearBegin(String value) {
        this.fiscalYearBegin = value;
    }

    /**
     * Obtiene el valor de la propiedad beginEffectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getBeginEffectiveDate() {
        return beginEffectiveDate;
    }

    /**
     * Define el valor de la propiedad beginEffectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setBeginEffectiveDate(LocalDate value) {
        this.beginEffectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad endEffectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getEndEffectiveDate() {
        return endEffectiveDate;
    }

    /**
     * Define el valor de la propiedad endEffectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setEndEffectiveDate(LocalDate value) {
        this.endEffectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad useParentLimitsFlag.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getUseParentLimitsFlag() {
        return useParentLimitsFlag;
    }

    /**
     * Define el valor de la propiedad useParentLimitsFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setUseParentLimitsFlag(TrueFalseType value) {
        this.useParentLimitsFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad useParentVelocitiesFlag.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getUseParentVelocitiesFlag() {
        return useParentVelocitiesFlag;
    }

    /**
     * Define el valor de la propiedad useParentVelocitiesFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setUseParentVelocitiesFlag(TrueFalseType value) {
        this.useParentVelocitiesFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad electronicCommerceTransactions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElectronicCommerceTransactions() {
        return electronicCommerceTransactions;
    }

    /**
     * Define el valor de la propiedad electronicCommerceTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElectronicCommerceTransactions(String value) {
        this.electronicCommerceTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad checkIndividualVelocities.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckIndividualVelocities() {
        return checkIndividualVelocities;
    }

    /**
     * Define el valor de la propiedad checkIndividualVelocities.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckIndividualVelocities(String value) {
        this.checkIndividualVelocities = value;
    }

    /**
     * Obtiene el valor de la propiedad useMCCGFlag.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getUseMCCGFlag() {
        return useMCCGFlag;
    }

    /**
     * Define el valor de la propiedad useMCCGFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setUseMCCGFlag(TrueFalseType value) {
        this.useMCCGFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad useParentMCCGFlag.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getUseParentMCCGFlag() {
        return useParentMCCGFlag;
    }

    /**
     * Define el valor de la propiedad useParentMCCGFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setUseParentMCCGFlag(TrueFalseType value) {
        this.useParentMCCGFlag = value;
    }

}
