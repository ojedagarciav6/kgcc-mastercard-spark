//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.bbva.kgcc.utils.Constantes;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


public class CorporateEntityType  implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("CorporateInformation_4000")
	private CorporateInformation4000Type corporateInformation4000; // Candidato a Map Partition (bloque básico)
    
	@XStreamImplicit(itemFieldName="HierarchyAddress4410")
    private List<HierarchyAddress4410Type> hierarchyAddress4410; // Ver la relacion que puede tener con el HierarchyAddress4410Type del bloque superior
    // Ver a que dirección hace referencia aquí, ya que en el ejemplo es diferente a la del bloque superior (Issuer Entity)
    
    @XStreamAlias("Portfolio_4420")
    private Portfolio4420Type portfolio4420; // Hay que ver si lo usamos (??) Seria un candidato Map Partition en el caso de uso
    
    @XStreamAlias("AuthorizationLimits_4430")
    private AuthorizationLimits4430Type authorizationLimits4430; // Hay que ver si lo usamos (??) Dato Básico Candidato Map Partition en caso de que se use
      
    @XStreamImplicit(itemFieldName="AccountEntity")
    private List<AccountEntityType> accountEntity;
    
    @XStreamAsAttribute
    @XStreamAlias("CorporationNumber")
    private String corporationNumber = Constantes.ISSUERNUMBER;

    
    
    public CorporateEntityType() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad corporateInformation4000.
     * 
     * @return
     *     possible object is
     *     {@link CorporateInformation4000Type }
     *     
     */
    public CorporateInformation4000Type getCorporateInformation4000() {
        return corporateInformation4000;
    }

    /**
     * Define el valor de la propiedad corporateInformation4000.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateInformation4000Type }
     *     
     */
    public void setCorporateInformation4000(CorporateInformation4000Type value) {
        this.corporateInformation4000 = value;
    }

    /**
     * Gets the value of the hierarchyAddress4410 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hierarchyAddress4410 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHierarchyAddress4410().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HierarchyAddress4410Type }
     * 
     * 
     */
    public List<HierarchyAddress4410Type> getHierarchyAddress4410() {
        if (hierarchyAddress4410 == null) {
            hierarchyAddress4410 = new ArrayList<HierarchyAddress4410Type>();
        }
        return this.hierarchyAddress4410;
    }

    

    public void setHierarchyAddress4410(List<HierarchyAddress4410Type> hierarchyAddress4410) {
		this.hierarchyAddress4410 = hierarchyAddress4410;
	} 
    
    public Portfolio4420Type getPortfolio4420() {
        return portfolio4420;
    }


    public void setPortfolio4420(Portfolio4420Type value) {
        this.portfolio4420 = value;
    }


    public AuthorizationLimits4430Type getAuthorizationLimits4430() {
        return authorizationLimits4430;
    }


    public void setAuthorizationLimits4430(AuthorizationLimits4430Type value) {
        this.authorizationLimits4430 = value;
    }

    public List<AccountEntityType> getAccountEntity() {
        if (accountEntity == null) {
            accountEntity = new ArrayList<AccountEntityType>();
        }
        return this.accountEntity;
    }



	public void setAccountEntity(List<AccountEntityType> accountEntity) {
		this.accountEntity = accountEntity;
	}

	public String getCorporationNumber() {
        return corporationNumber;
    }

   
    public void setCorporationNumber(String value) {
        this.corporationNumber = value;
    }

}
