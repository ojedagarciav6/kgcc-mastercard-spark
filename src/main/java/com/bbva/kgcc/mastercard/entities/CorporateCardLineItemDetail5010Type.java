//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;



public class CorporateCardLineItemDetail5010Type implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private FinancialRecordHeaderType financialRecordHeader;
    
    private Integer lineItemNum;
    
    private Integer subLineItemNum;
   
    private LocalDate dateOfPurchase;
   
    private String customerCode;
    
    private String productCode;
    
    private ProductCodeQualifierType productCodeQualifier;
   
    private String itemDescription;
    
    private NumExp16Type itemQuantity;
  
    private String itemUnitOfMeasurementType;
    
    private CurrencyAmountType unitPrice;
    
    private CurrencyAmountType extendedItemAmount;
    
    private String discountIndicator;
    
    private CurrencyAmountType discountAmount;
    
    private NumExp16Type itemDiscountRate;
    
    private String commodityCode;
    
    private CurrencyAmountType totalTaxAmount;
    
    private String typeOfSupply;
    
    private String taxExemptIndicator;
    
    private String uniqueVATInvoiceReferenceNum;
    
    private CurrencyAmountType lineItemTotalAmount;
    
    private Integer poLineNum;
    
    private String specialCondition;
    
    private String generalText;
   
    private String zeroCostToCustomerIndicator;

    /**
     * Obtiene el valor de la propiedad financialRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public FinancialRecordHeaderType getFinancialRecordHeader() {
        return financialRecordHeader;
    }

    /**
     * Define el valor de la propiedad financialRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public void setFinancialRecordHeader(FinancialRecordHeaderType value) {
        this.financialRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad lineItemNum.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineItemNum() {
        return lineItemNum;
    }

    /**
     * Define el valor de la propiedad lineItemNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineItemNum(Integer value) {
        this.lineItemNum = value;
    }

    /**
     * Obtiene el valor de la propiedad subLineItemNum.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubLineItemNum() {
        return subLineItemNum;
    }

    /**
     * Define el valor de la propiedad subLineItemNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubLineItemNum(Integer value) {
        this.subLineItemNum = value;
    }

    /**
     * Obtiene el valor de la propiedad dateOfPurchase.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getDateOfPurchase() {
        return dateOfPurchase;
    }

    /**
     * Define el valor de la propiedad dateOfPurchase.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setDateOfPurchase(LocalDate value) {
        this.dateOfPurchase = value;
    }

    /**
     * Obtiene el valor de la propiedad customerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Define el valor de la propiedad customerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad productCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Define el valor de la propiedad productCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Obtiene el valor de la propiedad productCodeQualifier.
     * 
     * @return
     *     possible object is
     *     {@link ProductCodeQualifierType }
     *     
     */
    public ProductCodeQualifierType getProductCodeQualifier() {
        return productCodeQualifier;
    }

    /**
     * Define el valor de la propiedad productCodeQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCodeQualifierType }
     *     
     */
    public void setProductCodeQualifier(ProductCodeQualifierType value) {
        this.productCodeQualifier = value;
    }

    /**
     * Obtiene el valor de la propiedad itemDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * Define el valor de la propiedad itemDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDescription(String value) {
        this.itemDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad itemQuantity.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getItemQuantity() {
        return itemQuantity;
    }

    /**
     * Define el valor de la propiedad itemQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setItemQuantity(NumExp16Type value) {
        this.itemQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad itemUnitOfMeasurementType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemUnitOfMeasurementType() {
        return itemUnitOfMeasurementType;
    }

    /**
     * Define el valor de la propiedad itemUnitOfMeasurementType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemUnitOfMeasurementType(String value) {
        this.itemUnitOfMeasurementType = value;
    }

    /**
     * Obtiene el valor de la propiedad unitPrice.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getUnitPrice() {
        return unitPrice;
    }

    /**
     * Define el valor de la propiedad unitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setUnitPrice(CurrencyAmountType value) {
        this.unitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad extendedItemAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getExtendedItemAmount() {
        return extendedItemAmount;
    }

    /**
     * Define el valor de la propiedad extendedItemAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setExtendedItemAmount(CurrencyAmountType value) {
        this.extendedItemAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad discountIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountIndicator() {
        return discountIndicator;
    }

    /**
     * Define el valor de la propiedad discountIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountIndicator(String value) {
        this.discountIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad discountAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Define el valor de la propiedad discountAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setDiscountAmount(CurrencyAmountType value) {
        this.discountAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad itemDiscountRate.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getItemDiscountRate() {
        return itemDiscountRate;
    }

    /**
     * Define el valor de la propiedad itemDiscountRate.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setItemDiscountRate(NumExp16Type value) {
        this.itemDiscountRate = value;
    }

    /**
     * Obtiene el valor de la propiedad commodityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * Define el valor de la propiedad commodityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityCode(String value) {
        this.commodityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad totalTaxAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getTotalTaxAmount() {
        return totalTaxAmount;
    }

    /**
     * Define el valor de la propiedad totalTaxAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setTotalTaxAmount(CurrencyAmountType value) {
        this.totalTaxAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad typeOfSupply.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeOfSupply() {
        return typeOfSupply;
    }

    /**
     * Define el valor de la propiedad typeOfSupply.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeOfSupply(String value) {
        this.typeOfSupply = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptIndicator() {
        return taxExemptIndicator;
    }

    /**
     * Define el valor de la propiedad taxExemptIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptIndicator(String value) {
        this.taxExemptIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad uniqueVATInvoiceReferenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueVATInvoiceReferenceNum() {
        return uniqueVATInvoiceReferenceNum;
    }

    /**
     * Define el valor de la propiedad uniqueVATInvoiceReferenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueVATInvoiceReferenceNum(String value) {
        this.uniqueVATInvoiceReferenceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad lineItemTotalAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getLineItemTotalAmount() {
        return lineItemTotalAmount;
    }

    /**
     * Define el valor de la propiedad lineItemTotalAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setLineItemTotalAmount(CurrencyAmountType value) {
        this.lineItemTotalAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad poLineNum.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPOLineNum() {
        return poLineNum;
    }

    /**
     * Define el valor de la propiedad poLineNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPOLineNum(Integer value) {
        this.poLineNum = value;
    }

    /**
     * Obtiene el valor de la propiedad specialCondition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialCondition() {
        return specialCondition;
    }

    /**
     * Define el valor de la propiedad specialCondition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialCondition(String value) {
        this.specialCondition = value;
    }

    /**
     * Obtiene el valor de la propiedad generalText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralText() {
        return generalText;
    }

    /**
     * Define el valor de la propiedad generalText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralText(String value) {
        this.generalText = value;
    }

    /**
     * Obtiene el valor de la propiedad zeroCostToCustomerIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZeroCostToCustomerIndicator() {
        return zeroCostToCustomerIndicator;
    }

    /**
     * Define el valor de la propiedad zeroCostToCustomerIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZeroCostToCustomerIndicator(String value) {
        this.zeroCostToCustomerIndicator = value;
    }

}
