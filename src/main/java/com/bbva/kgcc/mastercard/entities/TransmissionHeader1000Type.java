//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class TransmissionHeader1000Type implements Serializable{


	private static final long serialVersionUID = 1L;

	private TransRecordHeaderType transRecordHeader;
   
    private LocalDate processingStartDate;
    
    private LocalTime processingStartTime;
    
    private LocalDate processingEndDate;
    
    private LocalTime processingEndTime;
    
   
    private String fileReferenceNum;
    
   
    private String cdfVersionNum;
   
    private RunModeType runModeIndicator;
    
    private String processorNum;
   
    private String processorName;
   
    private Long masterCardFileSequenceNum;
    
    private Long masterCardResetNum;
   
    private String inboundSourceFormat;
   
    private String validationFootPrint;
    
    private String schemaVersionNum;

    
    
    public TransmissionHeader1000Type() {
    	// Constructor vacio
    }
    
    /**
     * Obtiene el valor de la propiedad transRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link TransRecordHeaderType }
     *     
     */
    public TransRecordHeaderType getTransRecordHeader() {
        return transRecordHeader;
    }

    /**
     * Define el valor de la propiedad transRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link TransRecordHeaderType }
     *     
     */
    public void setTransRecordHeader(TransRecordHeaderType value) {
        this.transRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad processingStartDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getProcessingStartDate() {
        return processingStartDate;
    }

    /**
     * Define el valor de la propiedad processingStartDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setProcessingStartDate(LocalDate value) {
        this.processingStartDate = value;
    }

    /**
     * Obtiene el valor de la propiedad processingStartTime.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalTime getProcessingStartTime() {
        return processingStartTime;
    }

    /**
     * Define el valor de la propiedad processingStartTime.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setProcessingStartTime(LocalTime value) {
        this.processingStartTime = value;
    }

    /**
     * Obtiene el valor de la propiedad processingEndDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getProcessingEndDate() {
        return processingEndDate;
    }

    /**
     * Define el valor de la propiedad processingEndDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setProcessingEndDate(LocalDate value) {
        this.processingEndDate = value;
    }

    /**
     * Obtiene el valor de la propiedad processingEndTime.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalTime getProcessingEndTime() {
        return processingEndTime;
    }

    /**
     * Define el valor de la propiedad processingEndTime.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setProcessingEndTime(LocalTime value) {
        this.processingEndTime = value;
    }

    /**
     * Obtiene el valor de la propiedad fileReferenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileReferenceNum() {
        return fileReferenceNum;
    }

    /**
     * Define el valor de la propiedad fileReferenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileReferenceNum(String value) {
        this.fileReferenceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cdfVersionNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDFVersionNum() {
        return cdfVersionNum;
    }

    /**
     * Define el valor de la propiedad cdfVersionNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDFVersionNum(String value) {
        this.cdfVersionNum = value;
    }

    /**
     * Obtiene el valor de la propiedad runModeIndicator.
     * 
     * @return
     *     possible object is
     *     {@link RunModeType }
     *     
     */
    public RunModeType getRunModeIndicator() {
        return runModeIndicator;
    }

    /**
     * Define el valor de la propiedad runModeIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link RunModeType }
     *     
     */
    public void setRunModeIndicator(RunModeType value) {
        this.runModeIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad processorNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessorNum() {
        return processorNum;
    }

    /**
     * Define el valor de la propiedad processorNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessorNum(String value) {
        this.processorNum = value;
    }

    /**
     * Obtiene el valor de la propiedad processorName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessorName() {
        return processorName;
    }

    /**
     * Define el valor de la propiedad processorName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessorName(String value) {
        this.processorName = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardFileSequenceNum.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMasterCardFileSequenceNum() {
        return masterCardFileSequenceNum;
    }

    /**
     * Define el valor de la propiedad masterCardFileSequenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMasterCardFileSequenceNum(Long value) {
        this.masterCardFileSequenceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardResetNum.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMasterCardResetNum() {
        return masterCardResetNum;
    }

    /**
     * Define el valor de la propiedad masterCardResetNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMasterCardResetNum(Long value) {
        this.masterCardResetNum = value;
    }

    /**
     * Obtiene el valor de la propiedad inboundSourceFormat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInboundSourceFormat() {
        return inboundSourceFormat;
    }

    /**
     * Define el valor de la propiedad inboundSourceFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInboundSourceFormat(String value) {
        this.inboundSourceFormat = value;
    }

    /**
     * Obtiene el valor de la propiedad validationFootPrint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationFootPrint() {
        return validationFootPrint;
    }

    /**
     * Define el valor de la propiedad validationFootPrint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationFootPrint(String value) {
        this.validationFootPrint = value;
    }

    /**
     * Obtiene el valor de la propiedad schemaVersionNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaVersionNum() {
        return schemaVersionNum;
    }

    /**
     * Define el valor de la propiedad schemaVersionNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaVersionNum(String value) {
        this.schemaVersionNum = value;
    }

}
