//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;



public class FinancialTransactionEntityType  implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("FinancialTransaction_5000")
	private FinancialTransaction5000Type financialTransaction5000;
    
	@XStreamAlias("CardAcceptor_5001")
    private CardAcceptor5001Type cardAcceptor5001;
   
    //private List<LineItemDetailEntityType> lineItemDetailEntity;
    
   
    
    @XStreamAsAttribute
    @XStreamAlias("ProcessorTransactionId")
    private String processorTransactionId;

    
    
	public FinancialTransactionEntityType() {
		super();
	}

	public FinancialTransaction5000Type getFinancialTransaction5000() {
		return financialTransaction5000;
	}

	public void setFinancialTransaction5000(FinancialTransaction5000Type financialTransaction5000) {
		this.financialTransaction5000 = financialTransaction5000;
	}

	public CardAcceptor5001Type getCardAcceptor5001() {
		return cardAcceptor5001;
	}

	public void setCardAcceptor5001(CardAcceptor5001Type cardAcceptor5001) {
		this.cardAcceptor5001 = cardAcceptor5001;
	}

	

	

//	public List<LineItemDetailEntityType> getLineItemDetailEntity() {
//		return lineItemDetailEntity;
//	}
//
//	public void setLineItemDetailEntity(List<LineItemDetailEntityType> lineItemDetailEntity) {
//		this.lineItemDetailEntity = lineItemDetailEntity;
//	}

	

	public String getProcessorTransactionId() {
		return processorTransactionId;
	}

	public void setProcessorTransactionId(String processorTransactionId) {
		this.processorTransactionId = processorTransactionId;
	}

    

}
