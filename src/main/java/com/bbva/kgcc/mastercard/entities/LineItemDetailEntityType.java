//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;

public class LineItemDetailEntityType implements Serializable{

   
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CorporateCardLineItemDetail5010Type corporateCardLineItemDetail5010;


    /**
     * Obtiene el valor de la propiedad corporateCardLineItemDetail5010.
     * 
     * @return
     *     possible object is
     *     {@link CorporateCardLineItemDetail5010Type }
     *     
     */
    public CorporateCardLineItemDetail5010Type getCorporateCardLineItemDetail5010() {
        return corporateCardLineItemDetail5010;
    }

    /**
     * Define el valor de la propiedad corporateCardLineItemDetail5010.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateCardLineItemDetail5010Type }
     *     
     */
    public void setCorporateCardLineItemDetail5010(CorporateCardLineItemDetail5010Type value) {
        this.corporateCardLineItemDetail5010 = value;
    }

    

}
