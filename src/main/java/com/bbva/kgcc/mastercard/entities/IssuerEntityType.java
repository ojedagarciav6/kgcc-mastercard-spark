
package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.bbva.kgcc.utils.Constantes;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


public class IssuerEntityType implements Serializable{

    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("IssuerInformation_3000")
	private IssuerInformation3000Type issuerInformation3000; // Candidato Map Partition (Básico)
	
	
	@XStreamImplicit(itemFieldName="HierarchyAddress_4410")
    private List<HierarchyAddress4410Type> hierarchyAddress4410; // Lista de uno ( Solo tenemos una dirección) - Candidato Map Partition (Básico)

	@XStreamImplicit(itemFieldName="CorporateEntity")
    private List<CorporateEntityType> corporateEntity;
    
    /* Atributos del Bloque*/
    
    @XStreamAsAttribute
    @XStreamAlias("ICANumber")
    private String icaNumber = Constantes.ICANUMBER;
    @XStreamAsAttribute
    @XStreamAlias("IssuerNumber")
    private String issuerNumber = Constantes.ISSUERNUMBER;

    
    public IssuerEntityType() {
    	// Constructor vacío
    }
    
    
    public IssuerInformation3000Type getIssuerInformation3000() {
        return issuerInformation3000;
    }

    
    public void setIssuerInformation3000(IssuerInformation3000Type value) {
        this.issuerInformation3000 = value;
    }

    
    public List<HierarchyAddress4410Type> getHierarchyAddress4410() {
        if (hierarchyAddress4410 == null) {
            hierarchyAddress4410 = new ArrayList<HierarchyAddress4410Type>();
        }
        return this.hierarchyAddress4410;
    }

	public void setHierarchyAddress4410(List<HierarchyAddress4410Type> hierarchyAddress4410) {
		this.hierarchyAddress4410 = hierarchyAddress4410;
	}
    
    
//
//    /**
//     * Gets the value of the mccGroupEntity property.
//     * 
//     * <p>
//     * This accessor method returns a reference to the live list,
//     * not a snapshot. Therefore any modification you make to the
//     * returned list will be present inside the JAXB object.
//     * This is why there is not a <CODE>set</CODE> method for the mccGroupEntity property.
//     * 
//     * <p>
//     * For example, to add a new item, do as follows:
//     * <pre>
//     *    getMCCGroupEntity().add(newItem);
//     * </pre>
//     * 
//     * 
//     * <p>
//     * Objects of the following type(s) are allowed in the list
//     * {@link MCCGroupEntityType }
//     * 
//     * 
//     */
//    public List<MCCGroupEntityType> getMCCGroupEntity() {
//        if (mccGroupEntity == null) {
//            mccGroupEntity = new ArrayList<MCCGroupEntityType>();
//        }
//        return this.mccGroupEntity;
//    }
//
//    /**
//     * Gets the value of the mccGroupAuthLimits4620 property.
//     * 
//     * <p>
//     * This accessor method returns a reference to the live list,
//     * not a snapshot. Therefore any modification you make to the
//     * returned list will be present inside the JAXB object.
//     * This is why there is not a <CODE>set</CODE> method for the mccGroupAuthLimits4620 property.
//     * 
//     * <p>
//     * For example, to add a new item, do as follows:
//     * <pre>
//     *    getMCCGroupAuthLimits4620().add(newItem);
//     * </pre>
//     * 
//     * 
//     * <p>
//     * Objects of the following type(s) are allowed in the list
//     * {@link MCCGroupAuthorizationLimits4620Type }
//     * 
//     * 
//     */
//    public List<MCCGroupAuthorizationLimits4620Type> getMCCGroupAuthLimits4620() {
//        if (mccGroupAuthLimits4620 == null) {
//            mccGroupAuthLimits4620 = new ArrayList<MCCGroupAuthorizationLimits4620Type>();
//        }
//        return this.mccGroupAuthLimits4620;
//    }
//
//    /**
//     * Gets the value of the corporateEntity property.
//     * 
//     * <p>
//     * This accessor method returns a reference to the live list,
//     * not a snapshot. Therefore any modification you make to the
//     * returned list will be present inside the JAXB object.
//     * This is why there is not a <CODE>set</CODE> method for the corporateEntity property.
//     * 
//     * <p>
//     * For example, to add a new item, do as follows:
//     * <pre>
//     *    getCorporateEntity().add(newItem);
//     * </pre>
//     * 
//     * 
//     * <p>
//     * Objects of the following type(s) are allowed in the list
//     * {@link CorporateEntityType }
//     * 
//     * 
//     */
    public List<CorporateEntityType> getCorporateEntity() {
        if (corporateEntity == null) {
            corporateEntity = new ArrayList<CorporateEntityType>();
        }
        return this.corporateEntity;
    }
    
    public void setCorporateEntity(List<CorporateEntityType> corporateEntity) {
		this.corporateEntity = corporateEntity;
	}


	/**
     * Obtiene el valor de la propiedad icaNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICANumber() {
        return icaNumber;
    }

    /**
     * Define el valor de la propiedad icaNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICANumber(String value) {
        this.icaNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad issuerNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerNumber() {
        return issuerNumber;
    }

    /**
     * Define el valor de la propiedad issuerNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerNumber(String value) {
        this.issuerNumber = value;
    }

}
