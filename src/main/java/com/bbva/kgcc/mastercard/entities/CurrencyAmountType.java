//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class CurrencyAmountType implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long value;
   
    @XStreamAsAttribute
    @XStreamAlias("CurrencyCode")
    private String currencyCode;
    
    @XStreamAsAttribute
    @XStreamAlias("CurrencyExponent")
    private short currencyExponent;
    
    @XStreamAsAttribute
    @XStreamAlias("CurrencySign")
    private SignCodeType currencySign;

    
    
    public CurrencyAmountType(long value, String currencyCode, short currencyExponent, SignCodeType currencySign) {
		super();
		this.value = value;
		this.currencyCode = currencyCode;
		this.currencyExponent = currencyExponent;
		this.currencySign = currencySign;
	}
    
    public CurrencyAmountType(long value) {
		super();
		this.value = value;
	}

	/**
     * Obtiene el valor de la propiedad value.
     * 
     */
    public long getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     */
    public void setValue(long value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyExponent.
     * 
     */
    public short getCurrencyExponent() {
        return currencyExponent;
    }

    /**
     * Define el valor de la propiedad currencyExponent.
     * 
     */
    public void setCurrencyExponent(short value) {
        this.currencyExponent = value;
    }

    /**
     * Obtiene el valor de la propiedad currencySign.
     * 
     * @return
     *     possible object is
     *     {@link SignCodeType }
     *     
     */
    public SignCodeType getCurrencySign() {
        return currencySign;
    }

    /**
     * Define el valor de la propiedad currencySign.
     * 
     * @param value
     *     allowed object is
     *     {@link SignCodeType }
     *     
     */
    public void setCurrencySign(SignCodeType value) {
        this.currencySign = value;
    }

}
