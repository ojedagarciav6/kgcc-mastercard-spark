//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class CardAcceptor5001Type implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XStreamAlias("FinancialRecordHeader")
	private FinancialRecordHeaderType financialRecordHeader;

	@XStreamAlias("AcquiringICA")
	private String acquiringICA;

	@XStreamAlias("CardAcceptorId")
    private String cardAcceptorId;
   
	@XStreamAlias("CardAcceptorName")
    private String cardAcceptorName;
    
	@XStreamAlias("CardAcceptorStreetAddress")
    private String cardAcceptorStreetAddress;

	@XStreamAlias("CardAcceptorCity")
    private String cardAcceptorCity;
    
	@XStreamAlias("CardAcceptorStateProvince")
    private String cardAcceptorStateProvince;
    
	@XStreamAlias("CardAcceptorLocationPostalCode")
    private String cardAcceptorLocationPostalCode;

	@XStreamAlias("CardAcceptorCountryCode")
    private String cardAcceptorCountryCode;

	@XStreamAlias("CardAcceptorCustomerServiceTelephoneNum")
    private String cardAcceptorCustomerServiceTelephoneNum;

	@XStreamAlias("CardAcceptorTelephoneNum")
    private String cardAcceptorTelephoneNum;

	@XStreamAlias("CardAcceptorAdditionalContactInformation")
    private String cardAcceptorAdditionalContactInformation;

	@XStreamAlias("SoleProprietorName")
    private String soleProprietorName;

	@XStreamAlias("LegalCorporationName")
    private String legalCorporationName;

	@XStreamAlias("CardAcceptorBusinessCode")
    private String cardAcceptorBusinessCode;

	@XStreamAlias("CardAcceptorTypeIndicator")
    private String cardAcceptorTypeIndicator;

	@XStreamAlias("CardAcceptorType")
    private String cardAcceptorType;
 
	@XStreamAlias("DunNum")
    private String dunNum;

	@XStreamAlias("AustinTetraNum")
    private String austinTetraNum;
    
	@XStreamAlias("CardAcceptorNAICSNum")
	private String cardAcceptorNAICSNum;

    @XStreamAlias("CardAcceptorTaxIdIndicator")
    private String cardAcceptorTaxIdIndicator;

    @XStreamAlias("CardAcceptorTaxId")
    private String cardAcceptorTaxId;

    @XStreamAlias("CardAcceptorVATNum")
    private String cardAcceptorVATNum;
    
    @XStreamAlias("AlternateCardAcceptorDescriptionData")
    private String alternateCardAcceptorDescriptionData;

    @XStreamAlias("EMailAddress")
    private String eMailAddress;

    
    
    public CardAcceptor5001Type() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad financialRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public FinancialRecordHeaderType getFinancialRecordHeader() {
        return financialRecordHeader;
    }

    /**
     * Define el valor de la propiedad financialRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public void setFinancialRecordHeader(FinancialRecordHeaderType value) {
        this.financialRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad acquiringICA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquiringICA() {
        return acquiringICA;
    }

    /**
     * Define el valor de la propiedad acquiringICA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquiringICA(String value) {
        this.acquiringICA = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorId() {
        return cardAcceptorId;
    }

    /**
     * Define el valor de la propiedad cardAcceptorId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorId(String value) {
        this.cardAcceptorId = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorName() {
        return cardAcceptorName;
    }

    /**
     * Define el valor de la propiedad cardAcceptorName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorName(String value) {
        this.cardAcceptorName = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorStreetAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorStreetAddress() {
        return cardAcceptorStreetAddress;
    }

    /**
     * Define el valor de la propiedad cardAcceptorStreetAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorStreetAddress(String value) {
        this.cardAcceptorStreetAddress = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorCity() {
        return cardAcceptorCity;
    }

    /**
     * Define el valor de la propiedad cardAcceptorCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorCity(String value) {
        this.cardAcceptorCity = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorStateProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorStateProvince() {
        return cardAcceptorStateProvince;
    }

    /**
     * Define el valor de la propiedad cardAcceptorStateProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorStateProvince(String value) {
        this.cardAcceptorStateProvince = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorLocationPostalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorLocationPostalCode() {
        return cardAcceptorLocationPostalCode;
    }

    /**
     * Define el valor de la propiedad cardAcceptorLocationPostalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorLocationPostalCode(String value) {
        this.cardAcceptorLocationPostalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorCountryCode() {
        return cardAcceptorCountryCode;
    }

    /**
     * Define el valor de la propiedad cardAcceptorCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorCountryCode(String value) {
        this.cardAcceptorCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorCustomerServiceTelephoneNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorCustomerServiceTelephoneNum() {
        return cardAcceptorCustomerServiceTelephoneNum;
    }

    /**
     * Define el valor de la propiedad cardAcceptorCustomerServiceTelephoneNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorCustomerServiceTelephoneNum(String value) {
        this.cardAcceptorCustomerServiceTelephoneNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorTelephoneNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTelephoneNum() {
        return cardAcceptorTelephoneNum;
    }

    /**
     * Define el valor de la propiedad cardAcceptorTelephoneNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTelephoneNum(String value) {
        this.cardAcceptorTelephoneNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorAdditionalContactInformation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorAdditionalContactInformation() {
        return cardAcceptorAdditionalContactInformation;
    }

    /**
     * Define el valor de la propiedad cardAcceptorAdditionalContactInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorAdditionalContactInformation(String value) {
        this.cardAcceptorAdditionalContactInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad soleProprietorName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoleProprietorName() {
        return soleProprietorName;
    }

    /**
     * Define el valor de la propiedad soleProprietorName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoleProprietorName(String value) {
        this.soleProprietorName = value;
    }

    /**
     * Obtiene el valor de la propiedad legalCorporationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalCorporationName() {
        return legalCorporationName;
    }

    /**
     * Define el valor de la propiedad legalCorporationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalCorporationName(String value) {
        this.legalCorporationName = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorBusinessCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorBusinessCode() {
        return cardAcceptorBusinessCode;
    }

    /**
     * Define el valor de la propiedad cardAcceptorBusinessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorBusinessCode(String value) {
        this.cardAcceptorBusinessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorTypeIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTypeIndicator() {
        return cardAcceptorTypeIndicator;
    }

    /**
     * Define el valor de la propiedad cardAcceptorTypeIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTypeIndicator(String value) {
        this.cardAcceptorTypeIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorType() {
        return cardAcceptorType;
    }

    /**
     * Define el valor de la propiedad cardAcceptorType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorType(String value) {
        this.cardAcceptorType = value;
    }

    /**
     * Obtiene el valor de la propiedad dunNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNNum() {
        return dunNum;
    }

    /**
     * Define el valor de la propiedad dunNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNNum(String value) {
        this.dunNum = value;
    }

    /**
     * Obtiene el valor de la propiedad austinTetraNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAustinTetraNum() {
        return austinTetraNum;
    }

    /**
     * Define el valor de la propiedad austinTetraNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAustinTetraNum(String value) {
        this.austinTetraNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorNAICSNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorNAICSNum() {
        return cardAcceptorNAICSNum;
    }

    /**
     * Define el valor de la propiedad cardAcceptorNAICSNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorNAICSNum(String value) {
        this.cardAcceptorNAICSNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorTaxIdIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTaxIdIndicator() {
        return cardAcceptorTaxIdIndicator;
    }

    /**
     * Define el valor de la propiedad cardAcceptorTaxIdIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTaxIdIndicator(String value) {
        this.cardAcceptorTaxIdIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorTaxId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTaxId() {
        return cardAcceptorTaxId;
    }

    /**
     * Define el valor de la propiedad cardAcceptorTaxId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTaxId(String value) {
        this.cardAcceptorTaxId = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorVATNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorVATNum() {
        return cardAcceptorVATNum;
    }

    /**
     * Define el valor de la propiedad cardAcceptorVATNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorVATNum(String value) {
        this.cardAcceptorVATNum = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateCardAcceptorDescriptionData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateCardAcceptorDescriptionData() {
        return alternateCardAcceptorDescriptionData;
    }

    /**
     * Define el valor de la propiedad alternateCardAcceptorDescriptionData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateCardAcceptorDescriptionData(String value) {
        this.alternateCardAcceptorDescriptionData = value;
    }

    /**
     * Obtiene el valor de la propiedad eMailAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMailAddress() {
        return eMailAddress;
    }

    /**
     * Define el valor de la propiedad eMailAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMailAddress(String value) {
        this.eMailAddress = value;
    }

}
