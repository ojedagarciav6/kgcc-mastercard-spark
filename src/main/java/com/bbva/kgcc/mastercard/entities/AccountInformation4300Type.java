package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;


public class AccountInformation4300Type implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("HierarchyRecordHeader")
	private HierarchyRecordHeaderType hierarchyRecordHeader;
    
	@XStreamAlias("ReportsTo")
    private String reportsTo;
    
	@XStreamAlias("PostedCurrencyCode")
    private String postedCurrencyCode;

	@XStreamAlias("NationalCurrencyCode")
    private String nationalCurrencyCode;

	@XStreamAlias("BillingCurrencyCode")
    private String billingCurrencyCode;

	@XStreamAlias("BillingType")
	private BillingType billingType;

	@XStreamAlias("AcceptanceBrandIdCode")
    private AcceptanceBrandType acceptanceBrandIdCode;
   
	@XStreamAlias("CorporateProduct")
	private String corporateProduct;

	@XStreamAlias("AccountTypeCode")
	private String accountTypeCode;
    
	@XStreamAlias("TransferFromAccount")
	private String transferFromAccount;

	@XStreamAlias("TransferToAccount")
	private String transferToAccount;

	@XStreamAlias("TransferReason")
	private String transferReason;

	@XStreamAlias("TransferDate")
	private LocalDate transferDate;

	@XStreamAlias("EffectiveDate")
	private LocalDate effectiveDate;

	@XStreamAlias("ExpirationDate")
	private LocalDate expirationDate;

	@XStreamAlias("PaymentDueDate")
	private LocalDate paymentDueDate;

	@XStreamAlias("DateOfBirth")
	private LocalDate dateOfBirth;

	@XStreamAlias("AnnualFeeRenewalMonth")
	private Short annualFeeRenewalMonth;

	@XStreamAlias("EmployeeId")
	private String employeeId;

	@XStreamAlias("EinNineDigit")
	private Long einNineDigit;

	@XStreamAlias("NameLocaleCode")
	private String nameLocaleCode;

	@XStreamAlias("NameLine1")
	private String nameLine1;

	@XStreamAlias("NameLine2")
	private String nameLine2;

	@XStreamAlias("AlternateNameLocaleCode")
	private String alternateNameLocaleCode;

	@XStreamAlias("AlternateNameLine1")
	private String alternateNameLine1;

	@XStreamAlias("AlternateNameLine2")
    private String alternateNameLine2;

	@XStreamAlias("InternalAccountingCode")
	private String internalAccountingCode;

	@XStreamAlias("TaxExemptIndicator")
	private String taxExemptIndicator;

	@XStreamAlias("DdaNum")
	private String ddaNum;

	@XStreamAlias("TransitRoutingNum")
    private Long transitRoutingNum;

	@XStreamAlias("TravelersChecksIndicator")
    private String travelersChecksIndicator;

	@XStreamAlias("NumOfCards")
	private Long numOfCards;

	@XStreamAlias("PendingCompanyNum")
	private String pendingCompanyNum;

	@XStreamAlias("EffectiveTransferDate")
	private LocalDate effectiveTransferDate;

	@XStreamAlias("CustomerId")
	private Long customerId;

	@XStreamAlias("AccountIdentifier")
    private String accountIdentifier;

	@XStreamAlias("MasterAccountingCode")
	private String masterAccountingCode;

	@XStreamAlias("ClientDefinedCode")
	private String clientDefinedCode;

	@XStreamAlias("NextBillingCycle")
	private Short nextBillingCycle;

	@XStreamAlias("CrvIndicator")
	private String crvIndicator;

	@XStreamAlias("CreditLimit")
	private NumExp16Type creditLimit;

	@XStreamAlias("FirstName")
	private String firstName;

	@XStreamAlias("LastName")
	private String lastName;

	@XStreamAlias("MiddleName")
	private String middleName;

	@XStreamAlias("SuffixName")
	private String suffixName;


    

    public AccountInformation4300Type() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad hierarchyRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public HierarchyRecordHeaderType getHierarchyRecordHeader() {
        return hierarchyRecordHeader;
    }

    /**
     * Define el valor de la propiedad hierarchyRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public void setHierarchyRecordHeader(HierarchyRecordHeaderType value) {
        this.hierarchyRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad reportsTo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportsTo() {
        return reportsTo;
    }

    /**
     * Define el valor de la propiedad reportsTo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportsTo(String value) {
        this.reportsTo = value;
    }

    /**
     * Obtiene el valor de la propiedad postedCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostedCurrencyCode() {
        return postedCurrencyCode;
    }

    /**
     * Define el valor de la propiedad postedCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostedCurrencyCode(String value) {
        this.postedCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nationalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalCurrencyCode() {
        return nationalCurrencyCode;
    }

    /**
     * Define el valor de la propiedad nationalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalCurrencyCode(String value) {
        this.nationalCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad billingCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCurrencyCode() {
        return billingCurrencyCode;
    }

    /**
     * Define el valor de la propiedad billingCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCurrencyCode(String value) {
        this.billingCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad billingType.
     * 
     * @return
     *     possible object is
     *     {@link BillingType }
     *     
     */
    public BillingType getBillingType() {
        return billingType;
    }

    /**
     * Define el valor de la propiedad billingType.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingType }
     *     
     */
    public void setBillingType(BillingType value) {
        this.billingType = value;
    }

    /**
     * Obtiene el valor de la propiedad acceptanceBrandIdCode.
     * 
     * @return
     *     possible object is
     *     {@link AcceptanceBrandType }
     *     
     */
    public AcceptanceBrandType getAcceptanceBrandIdCode() {
        return acceptanceBrandIdCode;
    }

    /**
     * Define el valor de la propiedad acceptanceBrandIdCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AcceptanceBrandType }
     *     
     */
    public void setAcceptanceBrandIdCode(AcceptanceBrandType value) {
        this.acceptanceBrandIdCode = value;
    }

    /**
     * Obtiene el valor de la propiedad corporateProduct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateProduct() {
        return corporateProduct;
    }

    /**
     * Define el valor de la propiedad corporateProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateProduct(String value) {
        this.corporateProduct = value;
    }

    /**
     * Obtiene el valor de la propiedad accountTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Define el valor de la propiedad accountTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad transferFromAccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferFromAccount() {
        return transferFromAccount;
    }

    /**
     * Define el valor de la propiedad transferFromAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferFromAccount(String value) {
        this.transferFromAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad transferToAccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferToAccount() {
        return transferToAccount;
    }

    /**
     * Define el valor de la propiedad transferToAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferToAccount(String value) {
        this.transferToAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad transferReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferReason() {
        return transferReason;
    }

    /**
     * Define el valor de la propiedad transferReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferReason(String value) {
        this.transferReason = value;
    }

    /**
     * Obtiene el valor de la propiedad transferDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getTransferDate() {
        return transferDate;
    }

    /**
     * Define el valor de la propiedad transferDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setTransferDate(LocalDate value) {
        this.transferDate = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define el valor de la propiedad effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setEffectiveDate(LocalDate value) {
        this.effectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define el valor de la propiedad expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setExpirationDate(LocalDate value) {
        this.expirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentDueDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getPaymentDueDate() {
        return paymentDueDate;
    }

    /**
     * Define el valor de la propiedad paymentDueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setPaymentDueDate(LocalDate value) {
        this.paymentDueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad dateOfBirth.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Define el valor de la propiedad dateOfBirth.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setDateOfBirth(LocalDate value) {
        this.dateOfBirth = value;
    }

    /**
     * Obtiene el valor de la propiedad annualFeeRenewalMonth.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAnnualFeeRenewalMonth() {
        return annualFeeRenewalMonth;
    }

    /**
     * Define el valor de la propiedad annualFeeRenewalMonth.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAnnualFeeRenewalMonth(Short value) {
        this.annualFeeRenewalMonth = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Define el valor de la propiedad employeeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Obtiene el valor de la propiedad einNineDigit.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEINNineDigit() {
        return einNineDigit;
    }

    /**
     * Define el valor de la propiedad einNineDigit.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEINNineDigit(Long value) {
        this.einNineDigit = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLocaleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLocaleCode() {
        return nameLocaleCode;
    }

    /**
     * Define el valor de la propiedad nameLocaleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLocaleCode(String value) {
        this.nameLocaleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLine1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine1() {
        return nameLine1;
    }

    /**
     * Define el valor de la propiedad nameLine1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine1(String value) {
        this.nameLine1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine2() {
        return nameLine2;
    }

    /**
     * Define el valor de la propiedad nameLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine2(String value) {
        this.nameLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLocaleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLocaleCode() {
        return alternateNameLocaleCode;
    }

    /**
     * Define el valor de la propiedad alternateNameLocaleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLocaleCode(String value) {
        this.alternateNameLocaleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLine1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLine1() {
        return alternateNameLine1;
    }

    /**
     * Define el valor de la propiedad alternateNameLine1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLine1(String value) {
        this.alternateNameLine1 = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLine2() {
        return alternateNameLine2;
    }

    /**
     * Define el valor de la propiedad alternateNameLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLine2(String value) {
        this.alternateNameLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad internalAccountingCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalAccountingCode() {
        return internalAccountingCode;
    }

    /**
     * Define el valor de la propiedad internalAccountingCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalAccountingCode(String value) {
        this.internalAccountingCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptIndicator() {
        return taxExemptIndicator;
    }

    /**
     * Define el valor de la propiedad taxExemptIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptIndicator(String value) {
        this.taxExemptIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad ddaNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDDANum() {
        return ddaNum;
    }

    /**
     * Define el valor de la propiedad ddaNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDDANum(String value) {
        this.ddaNum = value;
    }

    /**
     * Obtiene el valor de la propiedad transitRoutingNum.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransitRoutingNum() {
        return transitRoutingNum;
    }

    /**
     * Define el valor de la propiedad transitRoutingNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransitRoutingNum(Long value) {
        this.transitRoutingNum = value;
    }

    /**
     * Obtiene el valor de la propiedad travelersChecksIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelersChecksIndicator() {
        return travelersChecksIndicator;
    }

    /**
     * Define el valor de la propiedad travelersChecksIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelersChecksIndicator(String value) {
        this.travelersChecksIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfCards.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumOfCards() {
        return numOfCards;
    }

    /**
     * Define el valor de la propiedad numOfCards.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumOfCards(Long value) {
        this.numOfCards = value;
    }

    /**
     * Obtiene el valor de la propiedad pendingCompanyNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPendingCompanyNum() {
        return pendingCompanyNum;
    }

    /**
     * Define el valor de la propiedad pendingCompanyNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPendingCompanyNum(String value) {
        this.pendingCompanyNum = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveTransferDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getEffectiveTransferDate() {
        return effectiveTransferDate;
    }

    /**
     * Define el valor de la propiedad effectiveTransferDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setEffectiveTransferDate(LocalDate value) {
        this.effectiveTransferDate = value;
    }

    /**
     * Obtiene el valor de la propiedad customerId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Define el valor de la propiedad customerId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCustomerId(Long value) {
        this.customerId = value;
    }

    /**
     * Obtiene el valor de la propiedad accountIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountIdentifier() {
        return accountIdentifier;
    }

    /**
     * Define el valor de la propiedad accountIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountIdentifier(String value) {
        this.accountIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad masterAccountingCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterAccountingCode() {
        return masterAccountingCode;
    }

    /**
     * Define el valor de la propiedad masterAccountingCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterAccountingCode(String value) {
        this.masterAccountingCode = value;
    }

    /**
     * Obtiene el valor de la propiedad clientDefinedCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientDefinedCode() {
        return clientDefinedCode;
    }

    /**
     * Define el valor de la propiedad clientDefinedCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientDefinedCode(String value) {
        this.clientDefinedCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nextBillingCycle.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNextBillingCycle() {
        return nextBillingCycle;
    }

    /**
     * Define el valor de la propiedad nextBillingCycle.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNextBillingCycle(Short value) {
        this.nextBillingCycle = value;
    }

    /**
     * Obtiene el valor de la propiedad crvIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRVIndicator() {
        return crvIndicator;
    }

    /**
     * Define el valor de la propiedad crvIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRVIndicator(String value) {
        this.crvIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad creditLimit.
     * 
     * @return
     *     possible object is
     *     {@link NumExp16Type }
     *     
     */
    public NumExp16Type getCreditLimit() {
        return creditLimit;
    }

    /**
     * Define el valor de la propiedad creditLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link NumExp16Type }
     *     
     */
    public void setCreditLimit(NumExp16Type value) {
        this.creditLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad firstName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Define el valor de la propiedad firstName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Obtiene el valor de la propiedad lastName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Define el valor de la propiedad lastName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Obtiene el valor de la propiedad middleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Define el valor de la propiedad middleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Obtiene el valor de la propiedad suffixName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuffixName() {
        return suffixName;
    }

    /**
     * Define el valor de la propiedad suffixName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuffixName(String value) {
        this.suffixName = value;
    }

   

}
