//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class Portfolio4420Type implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAlias("HierarchyRecordHeader")
	private HierarchyRecordHeaderType hierarchyRecordHeader;

	@XStreamAlias("PortfolioStatementDate")
	private LocalDate portfolioStatementDate;

	@XStreamAlias("TotalNumOfAccounts")
	private Long totalNumOfAccounts;

	@XStreamAlias("TotalNumTransactions")
	private Long totalNumTransactions;

	@XStreamAlias("AccountsPastDue")
	private Long accountsPastDue;

	@XStreamAlias("AccountsWithDispute")
	private Long accountsWithDispute;

	@XStreamAlias("NumOfCards")
	private Long numOfCards;

	
	private CurrencyAmountType chargeOffAmount;

	
	private CurrencyAmountType pastDueAmount;

	
	private CurrencyAmountType disputeAmount;

	
	private CurrencyAmountType cycleAmountLimit;

	
	private CurrencyAmountType paymentDue;

	
	private CurrencyAmountType totalPaymentsDue;

	
	private CurrencyAmountType outstandingBalance;

	@XStreamAlias("NumOfDebits")
	private Long numOfDebits;

	@XStreamAlias("NumOfCredits")
	private Long numOfCredits;

	
	private CurrencyAmountType amountOfDebits;

	
	private CurrencyAmountType amountOfCredits;

	@XStreamAlias("NumAccountsOverLimit")
	private Long numAccountsOverLimit;


	private CurrencyAmountType amountPastDueThisCycle;


	private CurrencyAmountType amountOverLimit;

	@XStreamAlias("NumOfDaysPastDue")
	private Integer numOfDaysPastDue;

	
	private LocalDate dateOfLastPayment;


	private CurrencyAmountType amountOfLastPayment;

	@XStreamAlias("CurrentBalance")
	private CurrencyAmountType currentBalance;


	private CurrencyAmountType amountPastDue1Cycle;

    private CurrencyAmountType amountPastDue2Cycles;


    private CurrencyAmountType amountPastDue3Cycles;


    private CurrencyAmountType amountPastDue4Cycles;


    private CurrencyAmountType amountPastDue5Cycles;


    private CurrencyAmountType amountPastDue6Cycles;


    private CurrencyAmountType amountPastDue7OrMoreCycles;


    private LocalDate previousStatementDate;


    private CurrencyAmountType previousStatementAmount;
 

    private Integer numberOfTimesPastDue;
   
    @XStreamAlias("AvailableCredit")
    private CurrencyAmountType availableCredit;


    private Long numberOfCashAdvances;


    private CurrencyAmountType amountOfCashAdvances;


    private LocalDate nextStatementDate;

    /**
     * Obtiene el valor de la propiedad hierarchyRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public HierarchyRecordHeaderType getHierarchyRecordHeader() {
        return hierarchyRecordHeader;
    }

    /**
     * Define el valor de la propiedad hierarchyRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public void setHierarchyRecordHeader(HierarchyRecordHeaderType value) {
        this.hierarchyRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad portfolioStatementDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getPortfolioStatementDate() {
        return portfolioStatementDate;
    }

    /**
     * Define el valor de la propiedad portfolioStatementDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setPortfolioStatementDate(LocalDate value) {
        this.portfolioStatementDate = value;
    }

    /**
     * Obtiene el valor de la propiedad totalNumOfAccounts.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTotalNumOfAccounts() {
        return totalNumOfAccounts;
    }

    /**
     * Define el valor de la propiedad totalNumOfAccounts.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTotalNumOfAccounts(Long value) {
        this.totalNumOfAccounts = value;
    }

    /**
     * Obtiene el valor de la propiedad totalNumTransactions.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTotalNumTransactions() {
        return totalNumTransactions;
    }

    /**
     * Define el valor de la propiedad totalNumTransactions.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTotalNumTransactions(Long value) {
        this.totalNumTransactions = value;
    }

    /**
     * Obtiene el valor de la propiedad accountsPastDue.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountsPastDue() {
        return accountsPastDue;
    }

    /**
     * Define el valor de la propiedad accountsPastDue.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountsPastDue(Long value) {
        this.accountsPastDue = value;
    }

    /**
     * Obtiene el valor de la propiedad accountsWithDispute.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAccountsWithDispute() {
        return accountsWithDispute;
    }

    /**
     * Define el valor de la propiedad accountsWithDispute.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccountsWithDispute(Long value) {
        this.accountsWithDispute = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfCards.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumOfCards() {
        return numOfCards;
    }

    /**
     * Define el valor de la propiedad numOfCards.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumOfCards(Long value) {
        this.numOfCards = value;
    }

    /**
     * Obtiene el valor de la propiedad chargeOffAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getChargeOffAmount() {
        return chargeOffAmount;
    }

    /**
     * Define el valor de la propiedad chargeOffAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setChargeOffAmount(CurrencyAmountType value) {
        this.chargeOffAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad pastDueAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getPastDueAmount() {
        return pastDueAmount;
    }

    /**
     * Define el valor de la propiedad pastDueAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setPastDueAmount(CurrencyAmountType value) {
        this.pastDueAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad disputeAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getDisputeAmount() {
        return disputeAmount;
    }

    /**
     * Define el valor de la propiedad disputeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setDisputeAmount(CurrencyAmountType value) {
        this.disputeAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleAmountLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getCycleAmountLimit() {
        return cycleAmountLimit;
    }

    /**
     * Define el valor de la propiedad cycleAmountLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setCycleAmountLimit(CurrencyAmountType value) {
        this.cycleAmountLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentDue.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getPaymentDue() {
        return paymentDue;
    }

    /**
     * Define el valor de la propiedad paymentDue.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setPaymentDue(CurrencyAmountType value) {
        this.paymentDue = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPaymentsDue.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getTotalPaymentsDue() {
        return totalPaymentsDue;
    }

    /**
     * Define el valor de la propiedad totalPaymentsDue.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setTotalPaymentsDue(CurrencyAmountType value) {
        this.totalPaymentsDue = value;
    }

    /**
     * Obtiene el valor de la propiedad outstandingBalance.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getOutstandingBalance() {
        return outstandingBalance;
    }

    /**
     * Define el valor de la propiedad outstandingBalance.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setOutstandingBalance(CurrencyAmountType value) {
        this.outstandingBalance = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfDebits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumOfDebits() {
        return numOfDebits;
    }

    /**
     * Define el valor de la propiedad numOfDebits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumOfDebits(Long value) {
        this.numOfDebits = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfCredits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumOfCredits() {
        return numOfCredits;
    }

    /**
     * Define el valor de la propiedad numOfCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumOfCredits(Long value) {
        this.numOfCredits = value;
    }

    /**
     * Obtiene el valor de la propiedad amountOfDebits.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountOfDebits() {
        return amountOfDebits;
    }

    /**
     * Define el valor de la propiedad amountOfDebits.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountOfDebits(CurrencyAmountType value) {
        this.amountOfDebits = value;
    }

    /**
     * Obtiene el valor de la propiedad amountOfCredits.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountOfCredits() {
        return amountOfCredits;
    }

    /**
     * Define el valor de la propiedad amountOfCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountOfCredits(CurrencyAmountType value) {
        this.amountOfCredits = value;
    }

    /**
     * Obtiene el valor de la propiedad numAccountsOverLimit.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumAccountsOverLimit() {
        return numAccountsOverLimit;
    }

    /**
     * Define el valor de la propiedad numAccountsOverLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumAccountsOverLimit(Long value) {
        this.numAccountsOverLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDueThisCycle.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDueThisCycle() {
        return amountPastDueThisCycle;
    }

    /**
     * Define el valor de la propiedad amountPastDueThisCycle.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDueThisCycle(CurrencyAmountType value) {
        this.amountPastDueThisCycle = value;
    }

    /**
     * Obtiene el valor de la propiedad amountOverLimit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountOverLimit() {
        return amountOverLimit;
    }

    /**
     * Define el valor de la propiedad amountOverLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountOverLimit(CurrencyAmountType value) {
        this.amountOverLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfDaysPastDue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumOfDaysPastDue() {
        return numOfDaysPastDue;
    }

    /**
     * Define el valor de la propiedad numOfDaysPastDue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumOfDaysPastDue(Integer value) {
        this.numOfDaysPastDue = value;
    }

    /**
     * Obtiene el valor de la propiedad dateOfLastPayment.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getDateOfLastPayment() {
        return dateOfLastPayment;
    }

    /**
     * Define el valor de la propiedad dateOfLastPayment.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setDateOfLastPayment(LocalDate value) {
        this.dateOfLastPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad amountOfLastPayment.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountOfLastPayment() {
        return amountOfLastPayment;
    }

    /**
     * Define el valor de la propiedad amountOfLastPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountOfLastPayment(CurrencyAmountType value) {
        this.amountOfLastPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad currentBalance.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getCurrentBalance() {
        return currentBalance;
    }

    /**
     * Define el valor de la propiedad currentBalance.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setCurrentBalance(CurrencyAmountType value) {
        this.currentBalance = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue1Cycle.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue1Cycle() {
        return amountPastDue1Cycle;
    }

    /**
     * Define el valor de la propiedad amountPastDue1Cycle.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue1Cycle(CurrencyAmountType value) {
        this.amountPastDue1Cycle = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue2Cycles.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue2Cycles() {
        return amountPastDue2Cycles;
    }

    /**
     * Define el valor de la propiedad amountPastDue2Cycles.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue2Cycles(CurrencyAmountType value) {
        this.amountPastDue2Cycles = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue3Cycles.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue3Cycles() {
        return amountPastDue3Cycles;
    }

    /**
     * Define el valor de la propiedad amountPastDue3Cycles.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue3Cycles(CurrencyAmountType value) {
        this.amountPastDue3Cycles = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue4Cycles.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue4Cycles() {
        return amountPastDue4Cycles;
    }

    /**
     * Define el valor de la propiedad amountPastDue4Cycles.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue4Cycles(CurrencyAmountType value) {
        this.amountPastDue4Cycles = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue5Cycles.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue5Cycles() {
        return amountPastDue5Cycles;
    }

    /**
     * Define el valor de la propiedad amountPastDue5Cycles.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue5Cycles(CurrencyAmountType value) {
        this.amountPastDue5Cycles = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue6Cycles.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue6Cycles() {
        return amountPastDue6Cycles;
    }

    /**
     * Define el valor de la propiedad amountPastDue6Cycles.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue6Cycles(CurrencyAmountType value) {
        this.amountPastDue6Cycles = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPastDue7OrMoreCycles.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountPastDue7OrMoreCycles() {
        return amountPastDue7OrMoreCycles;
    }

    /**
     * Define el valor de la propiedad amountPastDue7OrMoreCycles.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountPastDue7OrMoreCycles(CurrencyAmountType value) {
        this.amountPastDue7OrMoreCycles = value;
    }

    /**
     * Obtiene el valor de la propiedad previousStatementDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getPreviousStatementDate() {
        return previousStatementDate;
    }

    /**
     * Define el valor de la propiedad previousStatementDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setPreviousStatementDate(LocalDate value) {
        this.previousStatementDate = value;
    }

    /**
     * Obtiene el valor de la propiedad previousStatementAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getPreviousStatementAmount() {
        return previousStatementAmount;
    }

    /**
     * Define el valor de la propiedad previousStatementAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setPreviousStatementAmount(CurrencyAmountType value) {
        this.previousStatementAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfTimesPastDue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfTimesPastDue() {
        return numberOfTimesPastDue;
    }

    /**
     * Define el valor de la propiedad numberOfTimesPastDue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfTimesPastDue(Integer value) {
        this.numberOfTimesPastDue = value;
    }

    /**
     * Obtiene el valor de la propiedad availableCredit.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAvailableCredit() {
        return availableCredit;
    }

    /**
     * Define el valor de la propiedad availableCredit.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAvailableCredit(CurrencyAmountType value) {
        this.availableCredit = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfCashAdvances.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfCashAdvances() {
        return numberOfCashAdvances;
    }

    /**
     * Define el valor de la propiedad numberOfCashAdvances.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfCashAdvances(Long value) {
        this.numberOfCashAdvances = value;
    }

    /**
     * Obtiene el valor de la propiedad amountOfCashAdvances.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountOfCashAdvances() {
        return amountOfCashAdvances;
    }

    /**
     * Define el valor de la propiedad amountOfCashAdvances.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountOfCashAdvances(CurrencyAmountType value) {
        this.amountOfCashAdvances = value;
    }

    /**
     * Obtiene el valor de la propiedad nextStatementDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getNextStatementDate() {
        return nextStatementDate;
    }

    /**
     * Define el valor de la propiedad nextStatementDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setNextStatementDate(LocalDate value) {
        this.nextStatementDate = value;
    }

}
