//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("TransmissionTrailer_9999")
public class TransmissionTrailer9999Type implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TransRecordHeaderType transRecordHeader;
    
    private Long recordCount;
    
    private Long numOfCredits;
    
    private BigDecimal totalAmountOfCredits;
    
    private Long numOfDebits;
    
    private BigDecimal totalAmountOfDebits;
    
    private LocalDate earliestPostingDate;
   
    private LocalDate earliestTxnDate;
   
    private LocalDate latestPostingDate;
    
    private LocalDate latestTxnDate;
    
    private Long numberOfTxnDebits;
    
    private Long numberOfTxnCredits;
    
    private Long numberOfAdjDebits;
    
    private Long numberOfAdjCredits;
    
    
    public TransmissionTrailer9999Type() {
    	//Constructor vacio
    }

    /**
     * Obtiene el valor de la propiedad transRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link TransRecordHeaderType }
     *     
     */
    public TransRecordHeaderType getTransRecordHeader() {
        return transRecordHeader;
    }

    /**
     * Define el valor de la propiedad transRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link TransRecordHeaderType }
     *     
     */
    public void setTransRecordHeader(TransRecordHeaderType value) {
        this.transRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad recordCount.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecordCount() {
        return recordCount;
    }

    /**
     * Define el valor de la propiedad recordCount.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecordCount(Long value) {
        this.recordCount = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfCredits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumOfCredits() {
        return numOfCredits;
    }

    /**
     * Define el valor de la propiedad numOfCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumOfCredits(Long value) {
        this.numOfCredits = value;
    }

    /**
     * Obtiene el valor de la propiedad totalAmountOfCredits.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmountOfCredits() {
        return totalAmountOfCredits;
    }

    /**
     * Define el valor de la propiedad totalAmountOfCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmountOfCredits(BigDecimal value) {
        this.totalAmountOfCredits = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfDebits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumOfDebits() {
        return numOfDebits;
    }

    /**
     * Define el valor de la propiedad numOfDebits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumOfDebits(Long value) {
        this.numOfDebits = value;
    }

    /**
     * Obtiene el valor de la propiedad totalAmountOfDebits.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmountOfDebits() {
        return totalAmountOfDebits;
    }

    /**
     * Define el valor de la propiedad totalAmountOfDebits.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmountOfDebits(BigDecimal value) {
        this.totalAmountOfDebits = value;
    }

    /**
     * Obtiene el valor de la propiedad earliestPostingDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public LocalDate getEarliestPostingDate() {
        return earliestPostingDate;
    }

    /**
     * Define el valor de la propiedad earliestPostingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEarliestPostingDate(LocalDate value) {
        this.earliestPostingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad earliestTxnDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public LocalDate getEarliestTxnDate() {
        return earliestTxnDate;
    }

    /**
     * Define el valor de la propiedad earliestTxnDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEarliestTxnDate(LocalDate value) {
        this.earliestTxnDate = value;
    }

    /**
     * Obtiene el valor de la propiedad latestPostingDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public LocalDate getLatestPostingDate() {
        return latestPostingDate;
    }

    /**
     * Define el valor de la propiedad latestPostingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLatestPostingDate(LocalDate value) {
        this.latestPostingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad latestTxnDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public LocalDate getLatestTxnDate() {
        return latestTxnDate;
    }

    /**
     * Define el valor de la propiedad latestTxnDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLatestTxnDate(LocalDate value) {
        this.latestTxnDate = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfTxnDebits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfTxnDebits() {
        return numberOfTxnDebits;
    }

    /**
     * Define el valor de la propiedad numberOfTxnDebits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfTxnDebits(Long value) {
        this.numberOfTxnDebits = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfTxnCredits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfTxnCredits() {
        return numberOfTxnCredits;
    }

    /**
     * Define el valor de la propiedad numberOfTxnCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfTxnCredits(Long value) {
        this.numberOfTxnCredits = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfAdjDebits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfAdjDebits() {
        return numberOfAdjDebits;
    }

    /**
     * Define el valor de la propiedad numberOfAdjDebits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfAdjDebits(Long value) {
        this.numberOfAdjDebits = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfAdjCredits.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfAdjCredits() {
        return numberOfAdjCredits;
    }

    /**
     * Define el valor de la propiedad numberOfAdjCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfAdjCredits(Long value) {
        this.numberOfAdjCredits = value;
    }

}
