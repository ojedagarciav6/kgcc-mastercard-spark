//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;



public class AccountEntityType  implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XStreamAlias("AccountInformation_4300")
	private AccountInformation4300Type accountInformation4300; // Hay que ver la informacion por lo que difiera del bloque superior
    
	@XStreamAlias("HierarchyAddress_4410")
    private List<HierarchyAddress4410Type> hierarchyAddress4410; // Hay que ver la informacion por lo que difiera del bloque superior
    
	@XStreamAlias("Portfolio_4420")
    private Portfolio4420Type portfolio4420; // Hay que ver la informacion por lo que difiera del bloque superior
    
	@XStreamAlias("AuthorizationLimits_4430")
	private AuthorizationLimits4430Type authorizationLimits4430; //Hay que ver la informacion por lo que difiera del bloque superior - A veces en el superior no aparece pero si aquí

       
    @XStreamImplicit(itemFieldName="FinancialTransactionEntity")
    private List<FinancialTransactionEntityType> financialTransactionEntity;

    @XStreamAsAttribute
    @XStreamAlias("AccountNumber")
    private String accountNumber = "5555555555555551";
    
    

    public AccountEntityType() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad accountInformation4300.
     * 
     * @return
     *     possible object is
     *     {@link AccountInformation4300Type }
     *     
     */
    public AccountInformation4300Type getAccountInformation4300() {
        return accountInformation4300;
    }

    /**
     * Define el valor de la propiedad accountInformation4300.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountInformation4300Type }
     *     
     */
    public void setAccountInformation4300(AccountInformation4300Type value) {
        this.accountInformation4300 = value;
    }

    /**
     * Gets the value of the hierarchyAddress4410 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hierarchyAddress4410 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHierarchyAddress4410().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HierarchyAddress4410Type }
     * 
     * 
     */
    public List<HierarchyAddress4410Type> getHierarchyAddress4410() {
        if (hierarchyAddress4410 == null) {
            hierarchyAddress4410 = new ArrayList<HierarchyAddress4410Type>();
        }
        return this.hierarchyAddress4410;
    }
    
    

    public void setHierarchyAddress4410(List<HierarchyAddress4410Type> hierarchyAddress4410) {
		this.hierarchyAddress4410 = hierarchyAddress4410;
	}

	/**
     * Obtiene el valor de la propiedad portfolio4420.
     * 
     * @return
     *     possible object is
     *     {@link Portfolio4420Type }
     *     
     */
    public Portfolio4420Type getPortfolio4420() {
        return portfolio4420;
    }

    /**
     * Define el valor de la propiedad portfolio4420.
     * 
     * @param value
     *     allowed object is
     *     {@link Portfolio4420Type }
     *     
     */
    public void setPortfolio4420(Portfolio4420Type value) {
        this.portfolio4420 = value;
    }

    /**
     * Obtiene el valor de la propiedad authorizationLimits4430.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationLimits4430Type }
     *     
     */
    public AuthorizationLimits4430Type getAuthorizationLimits4430() {
        return authorizationLimits4430;
    }

    /**
     * Define el valor de la propiedad authorizationLimits4430.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationLimits4430Type }
     *     
     */
    public void setAuthorizationLimits4430(AuthorizationLimits4430Type value) {
        this.authorizationLimits4430 = value;
    }
    
    

    /**
     * Obtiene el valor de la propiedad dailyPortfolio4450.
     * 
     * @return
     *     possible object is
     *     {@link DailyPortfolio4450Type }
     *     
     */


    public List<FinancialTransactionEntityType> getFinancialTransactionEntity() {
		return financialTransactionEntity;
	}

	public void setFinancialTransactionEntity(List<FinancialTransactionEntityType> financialTransactionEntity) {
		this.financialTransactionEntity = financialTransactionEntity;
	}

	/**
     * Obtiene el valor de la propiedad accountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Define el valor de la propiedad accountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

}
