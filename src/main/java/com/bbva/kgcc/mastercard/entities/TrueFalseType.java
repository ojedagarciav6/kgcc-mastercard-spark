
package com.bbva.kgcc.mastercard.entities;

public enum TrueFalseType {

    Y,
    N;

    public String value() {
        return name();
    }

    public static TrueFalseType fromValue(String v) {
        return valueOf(v);
    }

}
