package com.bbva.kgcc.mastercard.entities;

public class MasterCardFileEncoder {
	
	private Long sequence;
	private String codFileId;
	private String codCarga;
	private Long codCompId;
	private Integer codPeriod;
	private Integer codCardType;
	private Long startDate;
	private Long endDate;
	private Integer codPeriodComplet;
	private String optionalField1;
	private String optionalField2;
	private String optionalField3;
	private String optionalField4;
	private String cardHolder;
	private String prueba;
	private String nombre;
	private String apellidos;
	private String direccion;
	private String ciudad;
	
	
	public MasterCardFileEncoder() {
		//Constructor vacio
	}


	public String getCodCarga() {
		return codCarga;
	}


	public void setCodCarga(String codCarga) {
		this.codCarga = codCarga;
	}


	public Long getCodCompId() {
		return codCompId;
	}


	public void setCodCompId(Long codCompId) {
		this.codCompId = codCompId;
	}


	public Integer getCodPeriod() {
		return codPeriod;
	}


	public void setCodPeriod(Integer codPeriod) {
		this.codPeriod = codPeriod;
	}


	public Integer getCodCardType() {
		return codCardType;
	}


	public void setCodCardType(Integer codCardType) {
		this.codCardType = codCardType;
	}


	public Long getStartDate() {
		return startDate;
	}


	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}


	public Long getEndDate() {
		return endDate;
	}


	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}


	public Integer getCodPeriodComplet() {
		return codPeriodComplet;
	}


	public void setCodPeriodComplet(Integer codPeriodComplet) {
		this.codPeriodComplet = codPeriodComplet;
	}


	public String getOptionalField1() {
		return optionalField1;
	}


	public void setOptionalField1(String optionalField1) {
		this.optionalField1 = optionalField1;
	}


	public String getOptionalField2() {
		return optionalField2;
	}


	public void setOptionalField2(String optionalField2) {
		this.optionalField2 = optionalField2;
	}


	public String getOptionalField3() {
		return optionalField3;
	}


	public void setOptionalField3(String optionalField3) {
		this.optionalField3 = optionalField3;
	}


	public String getOptionalField4() {
		return optionalField4;
	}


	public void setOptionalField4(String optionalField4) {
		this.optionalField4 = optionalField4;
	}


	public String getCardHolder() {
		return cardHolder;
	}


	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}


	public String getPrueba() {
		return prueba;
	}


	public void setPrueba(String prueba) {
		this.prueba = prueba;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public Long getSequence() {
		return sequence;
	}


	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}


	public String getCodFileId() {
		return codFileId;
	}


	public void setCodFileId(String codFileId) {
		this.codFileId = codFileId;
	}
	
	

}
