//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


public class NumExp16Type {

    
    private String value;
    
    // Aquí hay que poner atributo para el XML
    @XStreamAsAttribute
    @XStreamAlias("Exponent")
    private String exponent = "0";

    
    public NumExp16Type(final String value) {
    	super();
    	
    	this.value = value;
    }
    
    /**
     * Obtiene el valor de la propiedad value.
     * 
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad exponent.
     * 
     */
    public String getExponent() {
        return exponent;
    }

    /**
     * Define el valor de la propiedad exponent.
     * 
     */
    public void setExponent(String value) {
        this.exponent = value;
    }

}
