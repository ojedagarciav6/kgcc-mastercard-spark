//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//

package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("CDFTransmissionFile")
public class CDFTransmissionFileEntityType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAsAttribute
	final String xmlns = "http://www.w3.org/2001/XMLSchema-instance";

	@XStreamAsAttribute
	@XStreamAlias("xmlns:xsi")
	final String xlink = "http://www.w3.org/2001/XMLSchema-instance";

	@XStreamAsAttribute
	@XStreamAlias("xsi:noNamespaceSchemaLocation")
	final String xlink2 = "CDFTransmissionFile.xsd";

	@XStreamAlias("TransmissionHeader_1000")
	private TransmissionHeader1000Type transmissionHeader1000;

	@XStreamAlias("IssuerEntity")
	@XStreamImplicit(itemFieldName = "IssuerEntity")
	private List<IssuerEntityType> issuerEntity;

	@XStreamAlias("TransmissionTrailer_9999")
	private TransmissionTrailer9999Type transmissionTrailer9999;

	public CDFTransmissionFileEntityType() {
		super();
	}

	public TransmissionHeader1000Type getTransmissionHeader1000() {
		return transmissionHeader1000;
	}

	public void setTransmissionHeader1000(TransmissionHeader1000Type value) {
		this.transmissionHeader1000 = value;
	}

	public List<IssuerEntityType> getIssuerEntity() {
		if (issuerEntity == null) {
			issuerEntity = new ArrayList<IssuerEntityType>();
		}
		return this.issuerEntity;
	}

	public void setIssuerEntity(List<IssuerEntityType> issuerEntity) {
		this.issuerEntity = issuerEntity;
	}

	public TransmissionTrailer9999Type getTransmissionTrailer9999() {
		return transmissionTrailer9999;
	}

	public void setTransmissionTrailer9999(TransmissionTrailer9999Type value) {
		this.transmissionTrailer9999 = value;
	}

	public String getXmlns() {
		return xmlns;
	}

	public String getXlink() {
		return xlink;
	}

	public String getXlink2() {
		return xlink2;
	}

}
