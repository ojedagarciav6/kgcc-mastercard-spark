package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class IssuerInformation3000Type implements Serializable{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HierarchyRecordHeaderType hierarchyRecordHeader;
    
	@XStreamAlias("NameLocaleCode")
	private String nameLocaleCode;
    
    @XStreamAlias("NameLine1")
    private String nameLine1;
    
    @XStreamAlias("NameLine2")
    private String nameLine2;
    
    @XStreamAlias("AlternateNameLocaleCode")
    private String alternateNameLocaleCode;
   
    @XStreamAlias("AlternateNameLine1")
    private String alternateNameLine1;
    
    @XStreamAlias("AlternateNameLine2")
    private String alternateNameLine2;
    
    @XStreamAlias("PostedCurrencyCode")
    private String postedCurrencyCode;
    
    private String vatCertificationCode;
   
    
    private String vatCertificationLevel;
    
    private LocalDate vatCertificationDate;
    
    @XStreamAlias("UseSmartDataNextGeneration")
    private TrueFalseType useSmartDataNextGeneration;

    public IssuerInformation3000Type() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad hierarchyRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public HierarchyRecordHeaderType getHierarchyRecordHeader() {
        return hierarchyRecordHeader;
    }

    /**
     * Define el valor de la propiedad hierarchyRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link HierarchyRecordHeaderType }
     *     
     */
    public void setHierarchyRecordHeader(HierarchyRecordHeaderType value) {
        this.hierarchyRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLocaleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLocaleCode() {
        return nameLocaleCode;
    }

    /**
     * Define el valor de la propiedad nameLocaleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLocaleCode(String value) {
        this.nameLocaleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLine1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine1() {
        return nameLine1;
    }

    /**
     * Define el valor de la propiedad nameLine1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine1(String value) {
        this.nameLine1 = value;
    }

    /**
     * Obtiene el valor de la propiedad nameLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameLine2() {
        return nameLine2;
    }

    /**
     * Define el valor de la propiedad nameLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameLine2(String value) {
        this.nameLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLocaleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLocaleCode() {
        return alternateNameLocaleCode;
    }

    /**
     * Define el valor de la propiedad alternateNameLocaleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLocaleCode(String value) {
        this.alternateNameLocaleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLine1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLine1() {
        return alternateNameLine1;
    }

    /**
     * Define el valor de la propiedad alternateNameLine1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLine1(String value) {
        this.alternateNameLine1 = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateNameLine2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateNameLine2() {
        return alternateNameLine2;
    }

    /**
     * Define el valor de la propiedad alternateNameLine2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateNameLine2(String value) {
        this.alternateNameLine2 = value;
    }

    /**
     * Obtiene el valor de la propiedad postedCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostedCurrencyCode() {
        return postedCurrencyCode;
    }

    /**
     * Define el valor de la propiedad postedCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostedCurrencyCode(String value) {
        this.postedCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad vatCertificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATCertificationCode() {
        return vatCertificationCode;
    }

    /**
     * Define el valor de la propiedad vatCertificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATCertificationCode(String value) {
        this.vatCertificationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad vatCertificationLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATCertificationLevel() {
        return vatCertificationLevel;
    }

    /**
     * Define el valor de la propiedad vatCertificationLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATCertificationLevel(String value) {
        this.vatCertificationLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad vatCertificationDate.
     * 
     * @return
     *     possible object is
     *     
     */
    public LocalDate getVATCertificationDate() {
        return vatCertificationDate;
    }

    /**
     * Define el valor de la propiedad vatCertificationDate.
     * 
     * @param value
     *     allowed object is
     *     
     */
    public void setVATCertificationDate(LocalDate value) {
        this.vatCertificationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad useSmartDataNextGeneration.
     * 
     * @return
     *     possible object is
     *     {@link TrueFalseType }
     *     
     */
    public TrueFalseType getUseSmartDataNextGeneration() {
        return useSmartDataNextGeneration;
    }

    /**
     * Define el valor de la propiedad useSmartDataNextGeneration.
     * 
     * @param value
     *     allowed object is
     *     {@link TrueFalseType }
     *     
     */
    public void setUseSmartDataNextGeneration(TrueFalseType value) {
        this.useSmartDataNextGeneration = value;
    }

}
