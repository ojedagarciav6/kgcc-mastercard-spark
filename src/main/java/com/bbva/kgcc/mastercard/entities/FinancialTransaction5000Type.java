//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.06.17 a las 05:20:24 PM CEST 
//


package com.bbva.kgcc.mastercard.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;



public class FinancialTransaction5000Type implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XStreamAlias("FinancialRecordHeader")
	private FinancialRecordHeaderType financialRecordHeader;
    
	@XStreamAlias("ProcessorTransactionId")
    private String processorTransactionId;
    

    private String reversalFlag;
    

    private String masterCardEstimatedTaxIndicator;
	
	
    private String masterCardFinancialTransactionId;
	
	@XStreamAlias("AcquirerReferenceData")
    private String acquirerReferenceData;
	
	@XStreamAlias("CardHolderTransactionType")
    private String cardHolderTransactionType;

	@XStreamAlias("PostingDate")
    private LocalDate postingDate;

	@XStreamAlias("TransactionDate")
    private LocalDate transactionDate;

	@XStreamAlias("ProcessingDate")
    private LocalDate processingDate;

	@XStreamAlias("BillingDate")
    private LocalDate billingDate;
 
	@XStreamAlias("ApprovalCode")
    private String approvalCode;

	@XStreamAlias("BanknetReferenceNum")
    private String banknetReferenceNum;
  
	@XStreamAlias("CardAcceptorTerminalId")
    private String cardAcceptorTerminalId;
 
	@XStreamAlias("DebitOrCreditIndicator")
    private SignCodeType debitOrCreditIndicator;
    
	@XStreamAlias("AmountInOriginalCurrency")
    private CurrencyAmountType amountInOriginalCurrency;
 
	@XStreamAlias("OriginalCurrencyCode")
    private String originalCurrencyCode;

	@XStreamAlias("AmountInPostedCurrency")
    private CurrencyAmountType amountInPostedCurrency;

	@XStreamAlias("PostedCurrencyCode")
    private String postedCurrencyCode;

	@XStreamAlias("PostedCurrencyConversionDate")
    private String postedCurrencyConversionDate;

	@XStreamAlias("PostedConversionRate")
    private BigDecimal postedConversionRate;

	@XStreamAlias("AmountInNationalCurrency")
    private CurrencyAmountType amountInNationalCurrency;

	@XStreamAlias("NationalCurrencyCode")
    private String nationalCurrencyCode;

	@XStreamAlias("NationalCurrencyConversionDate")
    private String nationalCurrencyConversionDate;

	@XStreamAlias("NationalConversionRate")
    private BigDecimal nationalConversionRate;

	@XStreamAlias("AmountInBillingCurrency")
    private CurrencyAmountType amountInBillingCurrency;

	@XStreamAlias("BillingCurrencyCode")
    private String billingCurrencyCode;

	@XStreamAlias("BillingCurrencyConversionDate")
    private String billingCurrencyConversionDate;

	@XStreamAlias("BillingConversionRate")
    private BigDecimal billingConversionRate;

	@XStreamAlias("CustomerCode")
    private String customerCode;

	@XStreamAlias("TotalTaxAmount")
    private CurrencyAmountType totalTaxAmount;

	@XStreamAlias("TotalTaxCollectedIndicator")
    private String totalTaxCollectedIndicator;

	@XStreamAlias("CorporationVATNum")
    private String corporationVATNum;

	@XStreamAlias("CardAcceptorReferenceNum")
    private String cardAcceptorReferenceNum;


    private CurrencyAmountType freightAmount;


    private CurrencyAmountType dutyAmount;


    private String destinationPostalCode;


    private String destinationStateProvinceCode;


    private String destinationCountryCode;

	@XStreamAlias("ShipFromPostalCode")
    private String shipFromPostalCode;

	@XStreamAlias("OrderDate")
    private LocalDate orderDate;

	@XStreamAlias("CustomerVATNum")
    private String customerVATNum;

	@XStreamAlias("UniqueInvoiceNum")
    private String uniqueInvoiceNum;

	@XStreamAlias("CommodityCode")
	private String commodityCode;

	@XStreamAlias("AuthorizedContactName")
    private String authorizedContactName;

	@XStreamAlias("AuthorizedContactName")
    private String authorizedContactPhone;

	@XStreamAlias("TaxExemptIndicator")
    private String taxExemptIndicator;

	@XStreamAlias("TransactionDiscountAmount")
    private CurrencyAmountType transactionDiscountAmount;

	@XStreamAlias("TransactionDiscountType")
    private String transactionDiscountType;


	private String memoFlag;


    private String cardAcceptorAdditionalNameInformation;

	@XStreamAlias("InternetTransactionIndicator")
    private String internetTransactionIndicator;

	@XStreamAlias("MailOrderTelephoneOrderIndicator")
	private String mailOrderTelephoneOrderIndicator;


	private String electricCommerceIndicator;


    private String disputeTrackingNum;

	@XStreamAlias("IssuerTransactionCode")
	private Integer issuerTransactionCode;

	@XStreamAlias("TransactionCategory")
    private Integer transactionCategory;

	@XStreamAlias("TransferFlag")
    private String transferFlag;

	@XStreamAlias("TranslateFlag")
	private String translateFlag;

	@XStreamAlias("TransactionCodeQualifier")
	private String transactionCodeQualifier;

	@XStreamAlias("VatEligibleInd")
	private String vatEligibleInd;

	@XStreamAlias("CustomIdentifier")
	private String customIdentifier;

	@XStreamAlias("CustomIdentifierType")
	private String customIdentifierType;

	@XStreamAlias("DataSource")
	private Integer dataSource;

	@XStreamAlias("VatSuppressionIndicator")
	private String vatSuppressionIndicator;

	@XStreamAlias("MccrAmount")
	private CurrencyAmountType mccrAmount;

	@XStreamAlias("IccrAmount")
    private CurrencyAmountType iccrAmount;

	@XStreamAlias("IccrSourceIndicator")
	private String iccrSourceIndicator;

	@XStreamAlias("OriginalProcessorTransactionID")
	private String originalProcessorTransactionID;

	@XStreamAlias("MatchSwitch")
	private String matchSwitch;

	@XStreamAlias("MatchDate")
	private LocalDate matchDate;

	@XStreamAlias("MatchLevelCode")
	private String matchLevelCode;

	@XStreamAlias("MatchTypeCode")
    private String matchTypeCode;

	@XStreamAlias("ManualMatchSwitch")
    private String manualMatchSwitch;

	@XStreamAlias("PostEnrichmentSwitch")
    private String postEnrichmentSwitch;

	@XStreamAlias("TransactionCategoryIndicator")
    private String transactionCategoryIndicator;

	@XStreamAlias("EstimatedVATRate")
    private BigDecimal estimatedVATRate;

	@XStreamAlias("EstimatedVATReclaimRate")
    private BigDecimal estimatedVATReclaimRate;

	@XStreamAlias("ExpenseCategoryCode")
    private String expenseCategoryCode;

	@XStreamAlias("EmployeeId")
	private String employeeId;

	@XStreamAlias("MerchantLocationId")
    private Long merchantLocationId;

	@XStreamAlias("ProgramIdentifier")
	private String programIdentifier;

	@XStreamAlias("TransactionTime")
	private LocalDate transactionTime;

    
    
    public FinancialTransaction5000Type() {
		super();
	}

	/**
     * Obtiene el valor de la propiedad financialRecordHeader.
     * 
     * @return
     *     possible object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public FinancialRecordHeaderType getFinancialRecordHeader() {
        return financialRecordHeader;
    }

    /**
     * Define el valor de la propiedad financialRecordHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialRecordHeaderType }
     *     
     */
    public void setFinancialRecordHeader(FinancialRecordHeaderType value) {
        this.financialRecordHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad processorTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessorTransactionId() {
        return processorTransactionId;
    }

    /**
     * Define el valor de la propiedad processorTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessorTransactionId(String value) {
        this.processorTransactionId = value;
    }

    /**
     * Obtiene el valor de la propiedad reversalFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReversalFlag() {
        return reversalFlag;
    }

    /**
     * Define el valor de la propiedad reversalFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReversalFlag(String value) {
        this.reversalFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardEstimatedTaxIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterCardEstimatedTaxIndicator() {
        return masterCardEstimatedTaxIndicator;
    }

    /**
     * Define el valor de la propiedad masterCardEstimatedTaxIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterCardEstimatedTaxIndicator(String value) {
        this.masterCardEstimatedTaxIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad masterCardFinancialTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterCardFinancialTransactionId() {
        return masterCardFinancialTransactionId;
    }

    /**
     * Define el valor de la propiedad masterCardFinancialTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterCardFinancialTransactionId(String value) {
        this.masterCardFinancialTransactionId = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateAccount.
     * 
     * @return
     *     possible object is
     *     {@link AlternateAccountType }
     *     
     */
  

    /**
     * Obtiene el valor de la propiedad acquirerReferenceData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquirerReferenceData() {
        return acquirerReferenceData;
    }

    /**
     * Define el valor de la propiedad acquirerReferenceData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquirerReferenceData(String value) {
        this.acquirerReferenceData = value;
    }

    /**
     * Obtiene el valor de la propiedad cardHolderTransactionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardHolderTransactionType() {
        return cardHolderTransactionType;
    }

    /**
     * Define el valor de la propiedad cardHolderTransactionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardHolderTransactionType(String value) {
        this.cardHolderTransactionType = value;
    }

    /**
     * Obtiene el valor de la propiedad postingDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getPostingDate() {
        return postingDate;
    }

    /**
     * Define el valor de la propiedad postingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setPostingDate(LocalDate value) {
        this.postingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    /**
     * Define el valor de la propiedad transactionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setTransactionDate(LocalDate value) {
        this.transactionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad processingDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getProcessingDate() {
        return processingDate;
    }

    /**
     * Define el valor de la propiedad processingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setProcessingDate(LocalDate value) {
        this.processingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad billingDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getBillingDate() {
        return billingDate;
    }

    /**
     * Define el valor de la propiedad billingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setBillingDate(LocalDate value) {
        this.billingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad approvalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * Define el valor de la propiedad approvalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalCode(String value) {
        this.approvalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad banknetReferenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanknetReferenceNum() {
        return banknetReferenceNum;
    }

    /**
     * Define el valor de la propiedad banknetReferenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanknetReferenceNum(String value) {
        this.banknetReferenceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorTerminalId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }

    /**
     * Define el valor de la propiedad cardAcceptorTerminalId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTerminalId(String value) {
        this.cardAcceptorTerminalId = value;
    }

    /**
     * Obtiene el valor de la propiedad debitOrCreditIndicator.
     * 
     * @return
     *     possible object is
     *     {@link SignCodeType }
     *     
     */
    public SignCodeType getDebitOrCreditIndicator() {
        return debitOrCreditIndicator;
    }

    /**
     * Define el valor de la propiedad debitOrCreditIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link SignCodeType }
     *     
     */
    public void setDebitOrCreditIndicator(SignCodeType value) {
        this.debitOrCreditIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad amountInOriginalCurrency.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountInOriginalCurrency() {
        return amountInOriginalCurrency;
    }

    /**
     * Define el valor de la propiedad amountInOriginalCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountInOriginalCurrency(CurrencyAmountType value) {
        this.amountInOriginalCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad originalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalCurrencyCode() {
        return originalCurrencyCode;
    }

    /**
     * Define el valor de la propiedad originalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalCurrencyCode(String value) {
        this.originalCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad amountInPostedCurrency.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountInPostedCurrency() {
        return amountInPostedCurrency;
    }

    /**
     * Define el valor de la propiedad amountInPostedCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountInPostedCurrency(CurrencyAmountType value) {
        this.amountInPostedCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad postedCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostedCurrencyCode() {
        return postedCurrencyCode;
    }

    /**
     * Define el valor de la propiedad postedCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostedCurrencyCode(String value) {
        this.postedCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad postedCurrencyConversionDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostedCurrencyConversionDate() {
        return postedCurrencyConversionDate;
    }

    /**
     * Define el valor de la propiedad postedCurrencyConversionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostedCurrencyConversionDate(String value) {
        this.postedCurrencyConversionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad postedConversionRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPostedConversionRate() {
        return postedConversionRate;
    }

    /**
     * Define el valor de la propiedad postedConversionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPostedConversionRate(BigDecimal value) {
        this.postedConversionRate = value;
    }

    /**
     * Obtiene el valor de la propiedad amountInNationalCurrency.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountInNationalCurrency() {
        return amountInNationalCurrency;
    }

    /**
     * Define el valor de la propiedad amountInNationalCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountInNationalCurrency(CurrencyAmountType value) {
        this.amountInNationalCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad nationalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalCurrencyCode() {
        return nationalCurrencyCode;
    }

    /**
     * Define el valor de la propiedad nationalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalCurrencyCode(String value) {
        this.nationalCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nationalCurrencyConversionDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalCurrencyConversionDate() {
        return nationalCurrencyConversionDate;
    }

    /**
     * Define el valor de la propiedad nationalCurrencyConversionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalCurrencyConversionDate(String value) {
        this.nationalCurrencyConversionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad nationalConversionRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNationalConversionRate() {
        return nationalConversionRate;
    }

    /**
     * Define el valor de la propiedad nationalConversionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNationalConversionRate(BigDecimal value) {
        this.nationalConversionRate = value;
    }

    /**
     * Obtiene el valor de la propiedad amountInBillingCurrency.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getAmountInBillingCurrency() {
        return amountInBillingCurrency;
    }

    /**
     * Define el valor de la propiedad amountInBillingCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setAmountInBillingCurrency(CurrencyAmountType value) {
        this.amountInBillingCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad billingCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCurrencyCode() {
        return billingCurrencyCode;
    }

    /**
     * Define el valor de la propiedad billingCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCurrencyCode(String value) {
        this.billingCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad billingCurrencyConversionDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCurrencyConversionDate() {
        return billingCurrencyConversionDate;
    }

    /**
     * Define el valor de la propiedad billingCurrencyConversionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCurrencyConversionDate(String value) {
        this.billingCurrencyConversionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad billingConversionRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillingConversionRate() {
        return billingConversionRate;
    }

    /**
     * Define el valor de la propiedad billingConversionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillingConversionRate(BigDecimal value) {
        this.billingConversionRate = value;
    }

    /**
     * Obtiene el valor de la propiedad customerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Define el valor de la propiedad customerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad totalTaxAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getTotalTaxAmount() {
        return totalTaxAmount;
    }

    /**
     * Define el valor de la propiedad totalTaxAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setTotalTaxAmount(CurrencyAmountType value) {
        this.totalTaxAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad totalTaxCollectedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalTaxCollectedIndicator() {
        return totalTaxCollectedIndicator;
    }

    /**
     * Define el valor de la propiedad totalTaxCollectedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalTaxCollectedIndicator(String value) {
        this.totalTaxCollectedIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad corporationVATNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporationVATNum() {
        return corporationVATNum;
    }

    /**
     * Define el valor de la propiedad corporationVATNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporationVATNum(String value) {
        this.corporationVATNum = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorReferenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorReferenceNum() {
        return cardAcceptorReferenceNum;
    }

    /**
     * Define el valor de la propiedad cardAcceptorReferenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorReferenceNum(String value) {
        this.cardAcceptorReferenceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad freightAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getFreightAmount() {
        return freightAmount;
    }

    /**
     * Define el valor de la propiedad freightAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setFreightAmount(CurrencyAmountType value) {
        this.freightAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad dutyAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getDutyAmount() {
        return dutyAmount;
    }

    /**
     * Define el valor de la propiedad dutyAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setDutyAmount(CurrencyAmountType value) {
        this.dutyAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationPostalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationPostalCode() {
        return destinationPostalCode;
    }

    /**
     * Define el valor de la propiedad destinationPostalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationPostalCode(String value) {
        this.destinationPostalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationStateProvinceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationStateProvinceCode() {
        return destinationStateProvinceCode;
    }

    /**
     * Define el valor de la propiedad destinationStateProvinceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationStateProvinceCode(String value) {
        this.destinationStateProvinceCode = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountryCode() {
        return destinationCountryCode;
    }

    /**
     * Define el valor de la propiedad destinationCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountryCode(String value) {
        this.destinationCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad shipFromPostalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipFromPostalCode() {
        return shipFromPostalCode;
    }

    /**
     * Define el valor de la propiedad shipFromPostalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipFromPostalCode(String value) {
        this.shipFromPostalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad orderDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getOrderDate() {
        return orderDate;
    }

    /**
     * Define el valor de la propiedad orderDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setOrderDate(LocalDate value) {
        this.orderDate = value;
    }

    /**
     * Obtiene el valor de la propiedad customerVATNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerVATNum() {
        return customerVATNum;
    }

    /**
     * Define el valor de la propiedad customerVATNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerVATNum(String value) {
        this.customerVATNum = value;
    }

    /**
     * Obtiene el valor de la propiedad uniqueInvoiceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueInvoiceNum() {
        return uniqueInvoiceNum;
    }

    /**
     * Define el valor de la propiedad uniqueInvoiceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueInvoiceNum(String value) {
        this.uniqueInvoiceNum = value;
    }

    /**
     * Obtiene el valor de la propiedad commodityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * Define el valor de la propiedad commodityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityCode(String value) {
        this.commodityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad authorizedContactName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedContactName() {
        return authorizedContactName;
    }

    /**
     * Define el valor de la propiedad authorizedContactName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedContactName(String value) {
        this.authorizedContactName = value;
    }

    /**
     * Obtiene el valor de la propiedad authorizedContactPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedContactPhone() {
        return authorizedContactPhone;
    }

    /**
     * Define el valor de la propiedad authorizedContactPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedContactPhone(String value) {
        this.authorizedContactPhone = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxExemptIndicator() {
        return taxExemptIndicator;
    }

    /**
     * Define el valor de la propiedad taxExemptIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxExemptIndicator(String value) {
        this.taxExemptIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionDiscountAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getTransactionDiscountAmount() {
        return transactionDiscountAmount;
    }

    /**
     * Define el valor de la propiedad transactionDiscountAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setTransactionDiscountAmount(CurrencyAmountType value) {
        this.transactionDiscountAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionDiscountType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDiscountType() {
        return transactionDiscountType;
    }

    /**
     * Define el valor de la propiedad transactionDiscountType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDiscountType(String value) {
        this.transactionDiscountType = value;
    }

    /**
     * Obtiene el valor de la propiedad memoFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemoFlag() {
        return memoFlag;
    }

    /**
     * Define el valor de la propiedad memoFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemoFlag(String value) {
        this.memoFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorAdditionalNameInformation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorAdditionalNameInformation() {
        return cardAcceptorAdditionalNameInformation;
    }

    /**
     * Define el valor de la propiedad cardAcceptorAdditionalNameInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorAdditionalNameInformation(String value) {
        this.cardAcceptorAdditionalNameInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad internetTransactionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternetTransactionIndicator() {
        return internetTransactionIndicator;
    }

    /**
     * Define el valor de la propiedad internetTransactionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternetTransactionIndicator(String value) {
        this.internetTransactionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad mailOrderTelephoneOrderIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailOrderTelephoneOrderIndicator() {
        return mailOrderTelephoneOrderIndicator;
    }

    /**
     * Define el valor de la propiedad mailOrderTelephoneOrderIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailOrderTelephoneOrderIndicator(String value) {
        this.mailOrderTelephoneOrderIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad electricCommerceIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElectricCommerceIndicator() {
        return electricCommerceIndicator;
    }

    /**
     * Define el valor de la propiedad electricCommerceIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElectricCommerceIndicator(String value) {
        this.electricCommerceIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad disputeTrackingNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisputeTrackingNum() {
        return disputeTrackingNum;
    }

    /**
     * Define el valor de la propiedad disputeTrackingNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisputeTrackingNum(String value) {
        this.disputeTrackingNum = value;
    }

    /**
     * Obtiene el valor de la propiedad issuerTransactionCode.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIssuerTransactionCode() {
        return issuerTransactionCode;
    }

    /**
     * Define el valor de la propiedad issuerTransactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIssuerTransactionCode(Integer value) {
        this.issuerTransactionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionCategory.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionCategory() {
        return transactionCategory;
    }

    /**
     * Define el valor de la propiedad transactionCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionCategory(Integer value) {
        this.transactionCategory = value;
    }

    /**
     * Obtiene el valor de la propiedad transferFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferFlag() {
        return transferFlag;
    }

    /**
     * Define el valor de la propiedad transferFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferFlag(String value) {
        this.transferFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad translateFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranslateFlag() {
        return translateFlag;
    }

    /**
     * Define el valor de la propiedad translateFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranslateFlag(String value) {
        this.translateFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionCodeQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCodeQualifier() {
        return transactionCodeQualifier;
    }

    /**
     * Define el valor de la propiedad transactionCodeQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCodeQualifier(String value) {
        this.transactionCodeQualifier = value;
    }

    /**
     * Obtiene el valor de la propiedad vatEligibleInd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATEligibleInd() {
        return vatEligibleInd;
    }

    /**
     * Define el valor de la propiedad vatEligibleInd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATEligibleInd(String value) {
        this.vatEligibleInd = value;
    }

    /**
     * Obtiene el valor de la propiedad customIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomIdentifier() {
        return customIdentifier;
    }

    /**
     * Define el valor de la propiedad customIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomIdentifier(String value) {
        this.customIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad customIdentifierType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomIdentifierType() {
        return customIdentifierType;
    }

    /**
     * Define el valor de la propiedad customIdentifierType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomIdentifierType(String value) {
        this.customIdentifierType = value;
    }

    /**
     * Obtiene el valor de la propiedad dataSource.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDataSource() {
        return dataSource;
    }

    /**
     * Define el valor de la propiedad dataSource.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDataSource(Integer value) {
        this.dataSource = value;
    }

    /**
     * Obtiene el valor de la propiedad vatSuppressionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATSuppressionIndicator() {
        return vatSuppressionIndicator;
    }

    /**
     * Define el valor de la propiedad vatSuppressionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATSuppressionIndicator(String value) {
        this.vatSuppressionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad mccrAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getMCCRAmount() {
        return mccrAmount;
    }

    /**
     * Define el valor de la propiedad mccrAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setMCCRAmount(CurrencyAmountType value) {
        this.mccrAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad iccrAmount.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountType }
     *     
     */
    public CurrencyAmountType getICCRAmount() {
        return iccrAmount;
    }

    /**
     * Define el valor de la propiedad iccrAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountType }
     *     
     */
    public void setICCRAmount(CurrencyAmountType value) {
        this.iccrAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad iccrSourceIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICCRSourceIndicator() {
        return iccrSourceIndicator;
    }

    /**
     * Define el valor de la propiedad iccrSourceIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICCRSourceIndicator(String value) {
        this.iccrSourceIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad originalProcessorTransactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalProcessorTransactionID() {
        return originalProcessorTransactionID;
    }

    /**
     * Define el valor de la propiedad originalProcessorTransactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalProcessorTransactionID(String value) {
        this.originalProcessorTransactionID = value;
    }

    /**
     * Obtiene el valor de la propiedad matchSwitch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchSwitch() {
        return matchSwitch;
    }

    /**
     * Define el valor de la propiedad matchSwitch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchSwitch(String value) {
        this.matchSwitch = value;
    }

    /**
     * Obtiene el valor de la propiedad matchDate.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getMatchDate() {
        return matchDate;
    }

    /**
     * Define el valor de la propiedad matchDate.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setMatchDate(LocalDate value) {
        this.matchDate = value;
    }

    /**
     * Obtiene el valor de la propiedad matchLevelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchLevelCode() {
        return matchLevelCode;
    }

    /**
     * Define el valor de la propiedad matchLevelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchLevelCode(String value) {
        this.matchLevelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad matchTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchTypeCode() {
        return matchTypeCode;
    }

    /**
     * Define el valor de la propiedad matchTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchTypeCode(String value) {
        this.matchTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad manualMatchSwitch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualMatchSwitch() {
        return manualMatchSwitch;
    }

    /**
     * Define el valor de la propiedad manualMatchSwitch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualMatchSwitch(String value) {
        this.manualMatchSwitch = value;
    }

    /**
     * Obtiene el valor de la propiedad postEnrichmentSwitch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostEnrichmentSwitch() {
        return postEnrichmentSwitch;
    }

    /**
     * Define el valor de la propiedad postEnrichmentSwitch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostEnrichmentSwitch(String value) {
        this.postEnrichmentSwitch = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionCategoryIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCategoryIndicator() {
        return transactionCategoryIndicator;
    }

    /**
     * Define el valor de la propiedad transactionCategoryIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCategoryIndicator(String value) {
        this.transactionCategoryIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad estimatedVATRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedVATRate() {
        return estimatedVATRate;
    }

    /**
     * Define el valor de la propiedad estimatedVATRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedVATRate(BigDecimal value) {
        this.estimatedVATRate = value;
    }

    /**
     * Obtiene el valor de la propiedad estimatedVATReclaimRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedVATReclaimRate() {
        return estimatedVATReclaimRate;
    }

    /**
     * Define el valor de la propiedad estimatedVATReclaimRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedVATReclaimRate(BigDecimal value) {
        this.estimatedVATReclaimRate = value;
    }

    /**
     * Obtiene el valor de la propiedad expenseCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpenseCategoryCode() {
        return expenseCategoryCode;
    }

    /**
     * Define el valor de la propiedad expenseCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpenseCategoryCode(String value) {
        this.expenseCategoryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Define el valor de la propiedad employeeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Obtiene el valor de la propiedad merchantLocationId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMerchantLocationId() {
        return merchantLocationId;
    }

    /**
     * Define el valor de la propiedad merchantLocationId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMerchantLocationId(Long value) {
        this.merchantLocationId = value;
    }

    /**
     * Obtiene el valor de la propiedad programIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramIdentifier() {
        return programIdentifier;
    }

    /**
     * Define el valor de la propiedad programIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramIdentifier(String value) {
        this.programIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionTime.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getTransactionTime() {
        return transactionTime;
    }

    /**
     * Define el valor de la propiedad transactionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setTransactionTime(LocalDate value) {
        this.transactionTime = value;
    }

}
