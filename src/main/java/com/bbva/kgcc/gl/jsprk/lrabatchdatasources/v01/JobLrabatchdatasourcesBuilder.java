package com.bbva.kgcc.gl.jsprk.lrabatchdatasources.v01;

import com.bbva.kgcc.utils.Constantes;
import com.bbva.lrba.builder.annotation.Builder;
import com.bbva.lrba.builder.spark.RegisterSparkBuilder;
import com.bbva.lrba.builder.spark.domain.SourcesList;
import com.bbva.lrba.builder.spark.domain.TargetsList;
import com.bbva.lrba.spark.domain.datasource.Source;
import com.bbva.lrba.spark.domain.datatarget.Target;
import com.bbva.lrba.spark.domain.transform.TransformConfig;

@Builder
public class JobLrabatchdatasourcesBuilder extends RegisterSparkBuilder {

    @Override
    public SourcesList registerSources() {
        //EXAMPLE WITH A LOCAL SOURCE FILE
        return SourcesList.builder()
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_CARDHOLDER)
                        .physicalName(Constantes.INPUT_VCF_CARDHOLDER)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasCardholder")
                        .header(true)
                        .delimiter("\t")
                        .build())
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_PERIOD)
                        .physicalName(Constantes.INPUT_VCF_PERIOD)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasPeriod")
                        .header(true)
                        .delimiter("\t")
                        .build())
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_ACCOUNTBALANCE)
                        .physicalName(Constantes.INPUT_VCF_ACCOUNTBALANCE)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasAccountBalance")
                        .header(true)
                        .delimiter("\t")
                        .build())
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_CARDACCOUNT)
                        .physicalName(Constantes.INPUT_VCF_CARDACCOUNT)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasCardAccount")
                        .header(true)
                        .delimiter("\t")
                        .build())
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_CARDTRANSACTION)
                        .physicalName(Constantes.INPUT_VCF_CARDTRANSACTION)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasCardTransaction")
                        .header(true)
                        .delimiter("\t")
                        .build())
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_COMPANY)
                        .physicalName(Constantes.INPUT_VCF_COMPANY)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasCompany")
                        .header(true)
                        .delimiter("\t")
                        .build())
        		.add(Source.File.Csv.builder()
                        .alias(Constantes.INPUT_ORGANIZATION)
                        .physicalName(Constantes.INPUT_VCF_ORGANIZATION)
                        .serviceName("local.logicalDataStore.batch")
                        .sql("SELECT * FROM aliasOrganization")
                        .header(true)
                        .delimiter("\t")
                        .build())
                .build();
    }

    @Override
    public TransformConfig registerTransform() {
        //IF YOU WANT TRANSFORM CLASS
        //return TransformConfig.TransformClass.builder().transform(new Transformer()).build();
        //IF YOU WANT SQL TRANSFORM
    	return TransformConfig.TransformClass.builder().transform(new Transformer()).build();//IF YOU DO NOT WANT TRANSFORM
        //IF YOU DO NOT WANT TRANSFORM
        //return null;
    }

    @Override
    public TargetsList registerTargets() {
        //EXAMPLE WITH A LOCAL TARGET FILE
        return TargetsList.builder()
                .add(Target.File.Csv.builder()
                        .alias(Constantes.OUTPUT_TARGET)
                        .physicalName("output/output.csv")
                        .serviceName("local.logicalDataStore.batch")
                        .header(false)
                        .delimiter("\t")
                        .build())
                .build();
    }

}