package com.bbva.kgcc.gl.jsprk.lrabatchdatasources.v01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import com.bbva.kgcc.mastercard.converters.CurrencyAmountTypeConverter;
import com.bbva.kgcc.mastercard.converters.LocalDateConverter;
import com.bbva.kgcc.mastercard.entities.AccountEntityType;
import com.bbva.kgcc.mastercard.entities.AccountInformation4300Type;
import com.bbva.kgcc.mastercard.entities.CDFTransmissionFileEntityType;
import com.bbva.kgcc.mastercard.entities.CorporateEntityType;
import com.bbva.kgcc.mastercard.entities.FinancialTransactionEntityType;
import com.bbva.kgcc.mastercard.entities.IssuerEntityType;
import com.bbva.kgcc.mastercard.entities.IssuerInformation3000Type;
import com.bbva.kgcc.processor.mastercard.entities.HeaderProcessor;
import com.bbva.kgcc.processor.mastercard.entities.IssuerEntityProcessor;
import com.bbva.kgcc.processor.mastercard.entities.TrailerProcessor;
import com.bbva.kgcc.processor.mastercard.entities.TransactionProcessor;
import com.bbva.kgcc.utils.Constantes;
import com.bbva.kgcc.utils.TransformUtils;
import com.bbva.kgcc.visa.entities.VisaMergeEntity;
import com.bbva.lrba.spark.transformers.Transform;
import com.thoughtworks.xstream.XStream;

import scala.collection.JavaConverters;
import scala.collection.Seq;

public class Transformer implements Transform {


	@Override
	public Map<String, Dataset<Row>> transform(Map<String, Dataset<Row>> datasetsFromRead) {

		/******** DATASET JOIN ***********/
		Map<String, Dataset<Row>> datasetsToWrite = new HashMap<>();
		List<String> claves = new ArrayList<String>();
		claves.add("codCarga");
		claves.add("codCompId");

		// Creamos un tablon con todos los campos de todas las tablas del modelo (JOIN)
		// con sus selects
		Dataset<Row> allTables = TransformUtils.getGlobalDatasetRow(datasetsFromRead);

		//Funcion para el casteo de columnas

		allTables = TransformUtils.columnsDataTypesCast(allTables);

		/** PRUEBAS FILTER POR COMPANY **/

		Dataset<Row> companies = allTables.select("codCompId").distinct();
		Iterator<Row> companiesIterator = companies.toLocalIterator();

		/* Objetos globales CDF File */

		CDFTransmissionFileEntityType cdfFile = new CDFTransmissionFileEntityType();
		//Dataset<VisaMergeEntity> rowVisaMDS = selectedColumns.as(Encoders.bean(VisaMergeEntity.class));
		//VisaMergeEntity rowVisaM = rowVisaMDS.first();
		VisaMergeEntity rowVisaM = new VisaMergeEntity();
		rowVisaM.setSequence(0L);
		cdfFile.setTransmissionHeader1000(HeaderProcessor.getTransmissionHeader1000(rowVisaM));
		
		List<IssuerEntityType> listIssuerEntity = new ArrayList<IssuerEntityType>();

		while (companiesIterator.hasNext()) {
			Row company = companiesIterator.next();

			List<AccountEntityType> accountEntityList = new ArrayList<AccountEntityType>();
			List<CorporateEntityType> listCorporateEntity = new ArrayList<CorporateEntityType>();
			CorporateEntityType corporateEntity = new CorporateEntityType();

			Dataset<Row> cardAccountsByCompany = allTables.filter(allTables.col("codCompId").equalTo(company.get(0)));
			Dataset<Row> accounts = cardAccountsByCompany.select("accountNum").distinct();
			Iterator<Row> accountsIterator = accounts.toLocalIterator();

			while (accountsIterator.hasNext()) {
				Row account = accountsIterator.next();

				// Añadir entidad FinancialTransactionEntity
				List<FinancialTransactionEntityType> financialTransactionList = new ArrayList<FinancialTransactionEntityType>();

				AccountEntityType accountEntity = new AccountEntityType();

				Dataset<Row> cardAccounts = cardAccountsByCompany
						.filter(allTables.col("accountNum").equalTo(account.get(0)));

				Iterator<Row> cardAccountsIterator = cardAccounts.toLocalIterator();

				while(cardAccountsIterator.hasNext()){
					Row row = cardAccountsIterator.next();					
					FinancialTransactionEntityType financialTransaction = TransactionProcessor.getFinancialTransactionMastercard(row,rowVisaM);
					financialTransactionList.add(financialTransaction);
				}
												
				//Función para setear el accountInformation4300
				AccountInformation4300Type accInf4300 = IssuerEntityProcessor.accountInf4300(cardAccounts.first(), rowVisaM);

				// Seteamos los valores del account Entity
				accountEntity.setAccountNumber(account.get(0).toString());
				accountEntity.setAccountInformation4300(accInf4300);
				accountEntity.setFinancialTransactionEntity(financialTransactionList);
				accountEntityList.add(accountEntity);
			}
			// map de corporate
			corporateEntity.setCorporationNumber(company.get(0).toString());
			corporateEntity.setAccountEntity(accountEntityList);
			listCorporateEntity.add(corporateEntity);

			// map issuer
			IssuerEntityType issuerEntity = IssuerEntityProcessor.getIssuerEntityRow(cardAccountsByCompany.first(), rowVisaM);
			issuerEntity.setCorporateEntity(listCorporateEntity);
			listIssuerEntity.add(issuerEntity);
		}

		cdfFile.setIssuerEntity(listIssuerEntity);
		cdfFile.setTransmissionTrailer9999(TrailerProcessor.getTransmissionTrailer_9999(rowVisaM));


		Dataset<String> parMapped = allTables.mapPartitions((MapPartitionsFunction<Row, String>) it -> {

			List<String> listResponses = new ArrayList<String>();

			XStream xstream = new XStream();
			
			//
			xstream.processAnnotations(CDFTransmissionFileEntityType.class);
			xstream.registerConverter(new CurrencyAmountTypeConverter());
			xstream.registerConverter(new LocalDateConverter());

			String dataXml = xstream.toXML(cdfFile);
			String cabecera = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			listResponses.add(cabecera);
			listResponses.add(dataXml);

			return listResponses.iterator();
		}, Encoders.STRING());

		//parMapped.show();


		parMapped.write().format("text").mode(SaveMode.Overwrite).save(
				"C:\\Users\\usuario\\eclipse-workspace\\KGCC-MasterCard-RepVictor\\kgcc-mastercard-spark\\local-execution\\files\\output\\cdfFile.xml");

		datasetsToWrite.put(Constantes.OUTPUT_TARGET, parMapped.toDF());

		return datasetsToWrite;

	}

	public Seq<String> convertListToSeq(List<String> inputList) {
		return JavaConverters.asScalaIteratorConverter(inputList.iterator()).asScala().toSeq();
	}

}