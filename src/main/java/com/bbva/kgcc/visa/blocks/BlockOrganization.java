package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.Organization;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockOrganization extends Block {
	private List<Organization> organizations;

	public BlockOrganization(final VCFHeader header, final VCFHeader trailer,
			final List<Organization> organizations2) {
		super(header, trailer);
		this.organizations = organizations2;
	}

	/**
	 * @return the organizations
	 */
	public List<Organization> getOrganizations() {
		return organizations;
	}

	/**
	 * @param organizations the organizations to set
	 */
	public void setOrganizations(final List<Organization> organizations) {
		this.organizations = organizations;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockOrganization [organizations=");
		builder.append(organizations);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
