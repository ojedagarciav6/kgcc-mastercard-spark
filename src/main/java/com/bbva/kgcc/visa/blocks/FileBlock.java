package com.bbva.kgcc.visa.blocks;

import java.util.List;

public class FileBlock {
	List<CompanySequenceBlock> companiesSequencesBlocks;

	public FileBlock(final List<CompanySequenceBlock> companiesSequencesBlocks) {
		super();
		this.companiesSequencesBlocks = companiesSequencesBlocks;
	}
	
	

	public FileBlock() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the companiesSequencesBlocks
	 */
	public List<CompanySequenceBlock> getCompaniesSequencesBlocks() {
		return companiesSequencesBlocks;
	}



	/**
	 * @param companiesSequencesBlocks the companiesSequencesBlocks to set
	 */
	public void setCompaniesSequencesBlocks(
			final List<CompanySequenceBlock> companiesSequencesBlocks) {
		this.companiesSequencesBlocks = companiesSequencesBlocks;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileBlock [companiesSequencesBlocks=");
		builder.append(companiesSequencesBlocks);
		builder.append("]");
		return builder.toString();
	}
	
	
}
