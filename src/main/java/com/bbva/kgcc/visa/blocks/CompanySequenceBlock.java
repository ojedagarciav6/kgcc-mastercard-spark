package com.bbva.kgcc.visa.blocks;

import com.bbva.kgcc.visa.entities.VCFHeader;

public class CompanySequenceBlock extends Block{

	private BlockCompany blockCompany;
	private BlockOrganization blockOrganization;
	private BlockCardHolder blockCardHolder;
	private BlockCardAccount blockCardAccount;
	private BlockCardTransaction blockCardTransaction;
	private BlockPeriod blockPeriod;
	private BlockAccountBalance blockAccountBalance;
	
	
	public CompanySequenceBlock(final VCFHeader header, final VCFHeader trailer,
			final BlockCompany blockCompany, final BlockOrganization blockOrganization,
			final BlockCardHolder blockCardHolder, final BlockCardAccount blockCardAccount,
			final BlockCardTransaction blockCardTransaction, final BlockPeriod blockPeriod,
			final BlockAccountBalance blockAccountBalance) {
		super(header, trailer);
		this.blockCompany = blockCompany;
		this.blockOrganization = blockOrganization;
		this.blockCardHolder = blockCardHolder;
		this.blockCardAccount = blockCardAccount;
		this.blockCardTransaction = blockCardTransaction;
		this.blockPeriod = blockPeriod;
		this.blockAccountBalance = blockAccountBalance;
	}


	


	public CompanySequenceBlock() {
		// TODO Auto-generated constructor stub
	}





	/**
	 * @return the blockCompany
	 */
	public BlockCompany getBlockCompany() {
		return blockCompany;
	}


	/**
	 * @param blockCompany the blockCompany to set
	 */
	public void setBlockCompany(final BlockCompany blockCompany) {
		this.blockCompany = blockCompany;
	}


	/**
	 * @return the blockOrganization
	 */
	public BlockOrganization getBlockOrganization() {
		return blockOrganization;
	}


	/**
	 * @param blockOrganization the blockOrganization to set
	 */
	public void setBlockOrganization(final BlockOrganization blockOrganization) {
		this.blockOrganization = blockOrganization;
	}


	/**
	 * @return the blockCardHolder
	 */
	public BlockCardHolder getBlockCardHolder() {
		return blockCardHolder;
	}


	/**
	 * @param blockCardHolder the blockCardHolder to set
	 */
	public void setBlockCardHolder(final BlockCardHolder blockCardHolder) {
		this.blockCardHolder = blockCardHolder;
	}


	/**
	 * @return the blockCardAccount
	 */
	public BlockCardAccount getBlockCardAccount() {
		return blockCardAccount;
	}


	/**
	 * @param blockCardAccount the blockCardAccount to set
	 */
	public void setBlockCardAccount(final BlockCardAccount blockCardAccount) {
		this.blockCardAccount = blockCardAccount;
	}


	/**
	 * @return the blockCardTransaction
	 */
	public BlockCardTransaction getBlockCardTransaction() {
		return blockCardTransaction;
	}


	/**
	 * @param blockCardTransaction the blockCardTransaction to set
	 */
	public void setBlockCardTransaction(final BlockCardTransaction blockCardTransaction) {
		this.blockCardTransaction = blockCardTransaction;
	}


	/**
	 * @return the blockPeriod
	 */
	public BlockPeriod getBlockPeriod() {
		return blockPeriod;
	}


	/**
	 * @param blockPeriod the blockPeriod to set
	 */
	public void setBlockPeriod(final BlockPeriod blockPeriod) {
		this.blockPeriod = blockPeriod;
	}


	/**
	 * @return the blockAccountBalance
	 */
	public BlockAccountBalance getBlockAccountBalance() {
		return blockAccountBalance;
	}


	/**
	 * @param blockAccountBalance the blockAccountBalance to set
	 */
	public void setBlockAccountBalance(final BlockAccountBalance blockAccountBalance) {
		this.blockAccountBalance = blockAccountBalance;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompanySequenceBlock [blockCompany=");
		builder.append(blockCompany);
		builder.append(", blockOrganization=");
		builder.append(blockOrganization);
		builder.append(", blockCardHolder=");
		builder.append(blockCardHolder);
		builder.append(", blockCardAccount=");
		builder.append(blockCardAccount);
		builder.append(", blockCardTransaction=");
		builder.append(blockCardTransaction);
		builder.append(", blockPeriod=");
		builder.append(blockPeriod);
		builder.append(", blockAccountBalance=");
		builder.append(blockAccountBalance);
		builder.append(", =");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
