package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.AccountBalance;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockAccountBalance extends Block {
	private List<AccountBalance> accountBalances;

	public BlockAccountBalance(final VCFHeader header, final VCFHeader trailer,
			final List<AccountBalance> accountBalance) {
		super(header, trailer);
		this.accountBalances = accountBalance;
	}

	/**
	 * @return the accountBalances
	 */
	public List<AccountBalance> getAccountBalances() {
		return accountBalances;
	}

	/**
	 * @param accountBalances
	 *            the accountBalances to set
	 */
	public void setAccountBalances(final List<AccountBalance> accountBalances) {
		this.accountBalances = accountBalances;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockAccountBalance [accountBalances=");
		builder.append(accountBalances);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}



	
}
