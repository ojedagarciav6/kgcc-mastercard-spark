package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.CardHolder;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockCardHolder extends Block {
	private List<CardHolder> cardHolders;

	public BlockCardHolder(final VCFHeader header, final VCFHeader trailer,
			final List<CardHolder> cardHolders2) {
		super(header, trailer);
		this.cardHolders = cardHolders2;
	}

	/**
	 * @return the cardHolders
	 */
	public List<CardHolder> getCardHolders() {
		return cardHolders;
	}

	/**
	 * @param cardHolders the cardHolders to set
	 */
	public void setCardHolders(final List<CardHolder> cardHolders) {
		this.cardHolders = cardHolders;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockCardHolder [cardHolders=");
		builder.append(cardHolders);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}


	
}
