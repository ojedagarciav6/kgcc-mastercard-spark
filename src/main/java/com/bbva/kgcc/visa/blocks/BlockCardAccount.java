package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.CardAccount;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockCardAccount extends Block{
	private List<CardAccount> cardAccounts;

	public BlockCardAccount(final VCFHeader header, final VCFHeader trailer,
			final List<CardAccount> cardAccounts2) {
		super(header, trailer);
		this.cardAccounts = cardAccounts2;
	}

	/**
	 * @return the cardAccounts
	 */
	public List<CardAccount> getCardAccounts() {
		return cardAccounts;
	}

	/**
	 * @param cardAccounts the cardAccounts to set
	 */
	public void setCardAccounts(final List<CardAccount> cardAccounts) {
		this.cardAccounts = cardAccounts;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockCardAccount [cardAccounts=");
		builder.append(cardAccounts);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
