package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.Period;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockPeriod extends Block {
	private List<Period> periods;

	public BlockPeriod(final VCFHeader header, final VCFHeader trailer, final List<Period> periods2) {
		super(header, trailer);
		this.periods = periods2;
	}

	/**
	 * @return the periods
	 */
	public List<Period> getPeriods() {
		return periods;
	}

	/**
	 * @param periods the periods to set
	 */
	public void setPeriods(final List<Period> periods) {
		this.periods = periods;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockPeriod [periods=");
		builder.append(periods);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
	
}
