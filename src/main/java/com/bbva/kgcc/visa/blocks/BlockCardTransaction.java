package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.CardTransaction;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockCardTransaction extends Block{
	private List<CardTransaction> cardTransactions;

	public BlockCardTransaction(final VCFHeader header, final VCFHeader trailer,
			final List<CardTransaction> cardTransactions2) {
		super(header, trailer);
		this.cardTransactions = cardTransactions2;
	}

	/**
	 * @return the cardTransactions
	 */
	public List<CardTransaction> getCardTransactions() {
		return cardTransactions;
	}

	/**
	 * @param cardTransactions the cardTransactions to set
	 */
	public void setCardTransactions(final List<CardTransaction> cardTransactions) {
		this.cardTransactions = cardTransactions;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockCardTransaction [cardTransactions=");
		builder.append(cardTransactions);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
	
}
