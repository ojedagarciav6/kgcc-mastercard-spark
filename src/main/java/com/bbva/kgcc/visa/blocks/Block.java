package com.bbva.kgcc.visa.blocks;

import com.bbva.kgcc.visa.entities.VCFHeader;

public class Block {
	private VCFHeader header;
	private VCFHeader trailer;

	public Block(final VCFHeader header, final VCFHeader trailer) {
		super();
		this.header = header;
		this.trailer = trailer;
	}

	public Block() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the header
	 */
	public VCFHeader getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            the header to set
	 */
	public void setHeader(final VCFHeader header) {
		this.header = header;
	}

	/**
	 * @return the trailer
	 */
	public VCFHeader getTrailer() {
		return trailer;
	}

	/**
	 * @param trailer
	 *            the trailer to set
	 */
	public void setTrailer(final VCFHeader trailer) {
		this.trailer = trailer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Block [header=");
		builder.append(header);
		builder.append(", trailer=");
		builder.append(trailer);
		builder.append("]");
		return builder.toString();
	}

}
