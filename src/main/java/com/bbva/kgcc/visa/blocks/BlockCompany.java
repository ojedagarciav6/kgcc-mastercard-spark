package com.bbva.kgcc.visa.blocks;

import java.util.List;

import com.bbva.kgcc.visa.entities.Company;
import com.bbva.kgcc.visa.entities.VCFHeader;

public class BlockCompany extends Block{
	private List<Company> companies;

	public BlockCompany(final VCFHeader header, final VCFHeader trailer,
			final List<Company> companies2) {
		super(header, trailer);
		this.companies = companies2;
	}

	/**
	 * @return the companies
	 */
	public List<Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(final List<Company> companies) {
		this.companies = companies;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlockCompany [companies=");
		builder.append(companies);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
