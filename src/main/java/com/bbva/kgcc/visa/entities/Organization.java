package com.bbva.kgcc.visa.entities;

/**
 * The Class Organization.
 */
public class Organization {

	/** Attribute CODE_LOAD_TRANS. */
	private String codCarga;

	/** Attribute COD_COMP_ID. */
	private Long codCompId;

	/** Attribute DES_COMPANY_NAME. */
	private String hierarchyNode;

	/** Attribute DES_PARNT_HIERRCHY. */
	private String parntHierrchy;

	/** Attribute QNU_EFFECTIVE_DATE. */
	private Long effectiveDate;

	/** Attribute DES_DESCRIPTION. */
	private String description;

	/** Attribute DES_MNAGER_LAST_NA. */
	private String managerLastNA;

	/** Attribute DES_MNAGER_NAME. */
	private String managerName;

	/** Attribute DES_MANAGER_TITLE. */
	private String managerTitle;

	/** Attribute QNU_MNAGER_TRINING. */
	private Long managerTrining;

	/** Attribute DES_MAN_PHONE_NUMB. */
	private String managerPhoneNumber;

	/** Attribute DES_TASK_ORDER_NUM. */
	private String taskOrderNum;

	/** Attribute DES_CONTACT_NAME. */
	private String contactName;

	/** Attribute DES_CONTACT_L_NAME. */
	private String contactLastName;

	/** Attribute DES_ADDRESS_LINE_1. */
	private String addressLine1;

	/** Attribute DES_ADDRESS_LINE_2. */
	private String addressLine2;

	/** Attribute DES_ADDRESS_LINE_3. */
	private String addressLine3;

	/** Attribute DES_CONTACT_CITY. */
	private String contactCity;

	/** Attribute DES_CONTACT_STATE. */
	private String contactState;

	/** Attribute DES_CONTACT_COUNTR. */
	private String codContactCountry;

	/** Attribute COD_POSTAL. */
	private String codPostal;

	/** Attribute DES_PHONE_NUMBER. */
	private String contactPhoneNumber;

	/** Attribute DES_CONTACT_FAX. */
	private String contactFax;

	/** Attribute DES_CONTACT_EMAIL. */
	private String contactEmail;

	/** Attribute DES_HIERRCHY_NODE2. */
	private String hierrchyNode2;

	/** Attribute DES_HIERRCHY_NODE3. */
	private String hierrchyNode3;

	/** Attribute COD_TREE_ID. */
	private String treeId;

	/** Attribute DES_COST_CENTER. */
	private String costCenter;

	/** Attribute DES_GL_SUB_ACCOUNT. */
	private String glSubAccount;

	/** Attribute OPTINAL_FIELD1. */
	private String optionalField1;

	/** Attribute OPTINAL_FIELD2. */
	private String optionalField2;

	/** Attribute OPTINAL_FIELD3. */
	private String optionalField3;

	/** Attribute OPTINAL_FIELD4. */
	private String optionalField4;

	public Organization(final String codCarga, final Long codCompId, final String hierarchyNode,
			final String parntHierrchy, final Long effectiveDate, final String description,
			final String managerLastNA, final String managerName, final String managerTitle,
			final Long managerTrining, final String managerPhoneNumber,
			final String taskOrderNum, final String contactName, final String contactLastName,
			final String addressLine1, final String addressLine2, final String addressLine3,
			final String contactCity, final String contactState, final String codContactCountry,
			final String codPostal, final String contactPhoneNumber, final String contactFax,
			final String contactEmail, final String hierrchyNode2, final String hierrchyNode3,
			final String treeId, final String costCenter, final String glSubAccount,
			final String optionalField1, final String optionalField2,
			final String optionalField3, final String optionalField4) {
		super();
		this.codCarga = codCarga;
		this.codCompId = codCompId;
		this.hierarchyNode = hierarchyNode;
		this.parntHierrchy = parntHierrchy;
		this.effectiveDate = effectiveDate;
		this.description = description;
		this.managerLastNA = managerLastNA;
		this.managerName = managerName;
		this.managerTitle = managerTitle;
		this.managerTrining = managerTrining;
		this.managerPhoneNumber = managerPhoneNumber;
		this.taskOrderNum = taskOrderNum;
		this.contactName = contactName;
		this.contactLastName = contactLastName;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.contactCity = contactCity;
		this.contactState = contactState;
		this.codContactCountry = codContactCountry;
		this.codPostal = codPostal;
		this.contactPhoneNumber = contactPhoneNumber;
		this.contactFax = contactFax;
		this.contactEmail = contactEmail;
		this.hierrchyNode2 = hierrchyNode2;
		this.hierrchyNode3 = hierrchyNode3;
		this.treeId = treeId;
		this.costCenter = costCenter;
		this.glSubAccount = glSubAccount;
		this.optionalField1 = optionalField1;
		this.optionalField2 = optionalField2;
		this.optionalField3 = optionalField3;
		this.optionalField4 = optionalField4;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the codCompId
	 */
	public Long getCodCompId() {
		return codCompId;
	}

	/**
	 * @param codCompId the codCompId to set
	 */
	public void setCodCompId(final Long codCompId) {
		this.codCompId = codCompId;
	}

	/**
	 * @return the hierarchyNode
	 */
	public String getHierarchyNode() {
		return hierarchyNode;
	}

	/**
	 * @param hierarchyNode the hierarchyNode to set
	 */
	public void setHierarchyNode(final String hierarchyNode) {
		this.hierarchyNode = hierarchyNode;
	}

	/**
	 * @return the parntHierrchy
	 */
	public String getParntHierrchy() {
		return parntHierrchy;
	}

	/**
	 * @param parntHierrchy the parntHierrchy to set
	 */
	public void setParntHierrchy(final String parntHierrchy) {
		this.parntHierrchy = parntHierrchy;
	}

	/**
	 * @return the effectiveDate
	 */
	public Long getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(final Long effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * @return the managerLastNA
	 */
	public String getManagerLastNA() {
		return managerLastNA;
	}

	/**
	 * @param managerLastNA the managerLastNA to set
	 */
	public void setManagerLastNA(final String managerLastNA) {
		this.managerLastNA = managerLastNA;
	}

	/**
	 * @return the managerName
	 */
	public String getManagerName() {
		return managerName;
	}

	/**
	 * @param managerName the managerName to set
	 */
	public void setManagerName(final String managerName) {
		this.managerName = managerName;
	}

	/**
	 * @return the managerTitle
	 */
	public String getManagerTitle() {
		return managerTitle;
	}

	/**
	 * @param managerTitle the managerTitle to set
	 */
	public void setManagerTitle(final String managerTitle) {
		this.managerTitle = managerTitle;
	}

	/**
	 * @return the managerTrining
	 */
	public Long getManagerTrining() {
		return managerTrining;
	}

	/**
	 * @param managerTrining the managerTrining to set
	 */
	public void setManagerTrining(final Long managerTrining) {
		this.managerTrining = managerTrining;
	}

	/**
	 * @return the managerPhoneNumber
	 */
	public String getManagerPhoneNumber() {
		return managerPhoneNumber;
	}

	/**
	 * @param managerPhoneNumber the managerPhoneNumber to set
	 */
	public void setManagerPhoneNumber(final String managerPhoneNumber) {
		this.managerPhoneNumber = managerPhoneNumber;
	}

	/**
	 * @return the taskOrderNum
	 */
	public String getTaskOrderNum() {
		return taskOrderNum;
	}

	/**
	 * @param taskOrderNum the taskOrderNum to set
	 */
	public void setTaskOrderNum(final String taskOrderNum) {
		this.taskOrderNum = taskOrderNum;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(final String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return contactLastName;
	}

	/**
	 * @param contactLastName the contactLastName to set
	 */
	public void setContactLastName(final String contactLastName) {
		this.contactLastName = contactLastName;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the addressLine3
	 */
	public String getAddressLine3() {
		return addressLine3;
	}

	/**
	 * @param addressLine3 the addressLine3 to set
	 */
	public void setAddressLine3(final String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	/**
	 * @return the contactCity
	 */
	public String getContactCity() {
		return contactCity;
	}

	/**
	 * @param contactCity the contactCity to set
	 */
	public void setContactCity(final String contactCity) {
		this.contactCity = contactCity;
	}

	/**
	 * @return the contactState
	 */
	public String getContactState() {
		return contactState;
	}

	/**
	 * @param contactState the contactState to set
	 */
	public void setContactState(final String contactState) {
		this.contactState = contactState;
	}

	/**
	 * @return the codContactCountry
	 */
	public String getCodContactCountry() {
		return codContactCountry;
	}

	/**
	 * @param codContactCountry the codContactCountry to set
	 */
	public void setCodContactCountry(final String codContactCountry) {
		this.codContactCountry = codContactCountry;
	}

	/**
	 * @return the codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}

	/**
	 * @param codPostal the codPostal to set
	 */
	public void setCodPostal(final String codPostal) {
		this.codPostal = codPostal;
	}

	/**
	 * @return the contactPhoneNumber
	 */
	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	/**
	 * @param contactPhoneNumber the contactPhoneNumber to set
	 */
	public void setContactPhoneNumber(final String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	/**
	 * @return the contactFax
	 */
	public String getContactFax() {
		return contactFax;
	}

	/**
	 * @param contactFax the contactFax to set
	 */
	public void setContactFax(final String contactFax) {
		this.contactFax = contactFax;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(final String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * @return the hierrchyNode2
	 */
	public String getHierrchyNode2() {
		return hierrchyNode2;
	}

	/**
	 * @param hierrchyNode2 the hierrchyNode2 to set
	 */
	public void setHierrchyNode2(final String hierrchyNode2) {
		this.hierrchyNode2 = hierrchyNode2;
	}

	/**
	 * @return the hierrchyNode3
	 */
	public String getHierrchyNode3() {
		return hierrchyNode3;
	}

	/**
	 * @param hierrchyNode3 the hierrchyNode3 to set
	 */
	public void setHierrchyNode3(final String hierrchyNode3) {
		this.hierrchyNode3 = hierrchyNode3;
	}

	/**
	 * @return the treeId
	 */
	public String getTreeId() {
		return treeId;
	}

	/**
	 * @param treeId the treeId to set
	 */
	public void setTreeId(final String treeId) {
		this.treeId = treeId;
	}

	/**
	 * @return the costCenter
	 */
	public String getCostCenter() {
		return costCenter;
	}

	/**
	 * @param costCenter the costCenter to set
	 */
	public void setCostCenter(final String costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * @return the glSubAccount
	 */
	public String getGlSubAccount() {
		return glSubAccount;
	}

	/**
	 * @param glSubAccount the glSubAccount to set
	 */
	public void setGlSubAccount(final String glSubAccount) {
		this.glSubAccount = glSubAccount;
	}

	/**
	 * @return the optionalField1
	 */
	public String getOptionalField1() {
		return optionalField1;
	}

	/**
	 * @param optionalField1 the optionalField1 to set
	 */
	public void setOptionalField1(final String optionalField1) {
		this.optionalField1 = optionalField1;
	}

	/**
	 * @return the optionalField2
	 */
	public String getOptionalField2() {
		return optionalField2;
	}

	/**
	 * @param optionalField2 the optionalField2 to set
	 */
	public void setOptionalField2(final String optionalField2) {
		this.optionalField2 = optionalField2;
	}

	/**
	 * @return the optionalField3
	 */
	public String getOptionalField3() {
		return optionalField3;
	}

	/**
	 * @param optionalField3 the optionalField3 to set
	 */
	public void setOptionalField3(final String optionalField3) {
		this.optionalField3 = optionalField3;
	}

	/**
	 * @return the optionalField4
	 */
	public String getOptionalField4() {
		return optionalField4;
	}

	/**
	 * @param optionalField4 the optionalField4 to set
	 */
	public void setOptionalField4(final String optionalField4) {
		this.optionalField4 = optionalField4;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Organization [codCarga=");
		builder.append(codCarga);
		builder.append(", codCompId=");
		builder.append(codCompId);
		builder.append(", hierarchyNode=");
		builder.append(hierarchyNode);
		builder.append(", parntHierrchy=");
		builder.append(parntHierrchy);
		builder.append(", effectiveDate=");
		builder.append(effectiveDate);
		builder.append(", description=");
		builder.append(description);
		builder.append(", managerLastNA=");
		builder.append(managerLastNA);
		builder.append(", managerName=");
		builder.append(managerName);
		builder.append(", managerTitle=");
		builder.append(managerTitle);
		builder.append(", managerTrining=");
		builder.append(managerTrining);
		builder.append(", managerPhoneNumber=");
		builder.append(managerPhoneNumber);
		builder.append(", taskOrderNum=");
		builder.append(taskOrderNum);
		builder.append(", contactName=");
		builder.append(contactName);
		builder.append(", contactLastName=");
		builder.append(contactLastName);
		builder.append(", addressLine1=");
		builder.append(addressLine1);
		builder.append(", addressLine2=");
		builder.append(addressLine2);
		builder.append(", addressLine3=");
		builder.append(addressLine3);
		builder.append(", contactCity=");
		builder.append(contactCity);
		builder.append(", contactState=");
		builder.append(contactState);
		builder.append(", codContactCountry=");
		builder.append(codContactCountry);
		builder.append(", codPostal=");
		builder.append(codPostal);
		builder.append(", contactPhoneNumber=");
		builder.append(contactPhoneNumber);
		builder.append(", contactFax=");
		builder.append(contactFax);
		builder.append(", contactEmail=");
		builder.append(contactEmail);
		builder.append(", hierrchyNode2=");
		builder.append(hierrchyNode2);
		builder.append(", hierrchyNode3=");
		builder.append(hierrchyNode3);
		builder.append(", treeId=");
		builder.append(treeId);
		builder.append(", costCenter=");
		builder.append(costCenter);
		builder.append(", glSubAccount=");
		builder.append(glSubAccount);
		builder.append(", optionalField1=");
		builder.append(optionalField1);
		builder.append(", optionalField2=");
		builder.append(optionalField2);
		builder.append(", optionalField3=");
		builder.append(optionalField3);
		builder.append(", optionalField4=");
		builder.append(optionalField4);
		builder.append("]");
		return builder.toString();
	}

	
}
