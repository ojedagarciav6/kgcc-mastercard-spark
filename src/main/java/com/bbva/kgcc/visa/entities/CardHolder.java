package com.bbva.kgcc.visa.entities;

/**
 * The Class CardHolder.
 */
public class CardHolder {

	/** Attribute COD_LOAD_TR. */
	private String codCarga;

	/** Attribute COD_COMP_ID. */
	private Long codCompId;

	/** Attribute COD_CARDHOLD. */
	private String idCardHolder;

	/** Attribute COD_HIER_NOD. */
	private String hierarchyNode;

	/** Attribute DES_FIRST_NM. */
	private String firstName;

	/** Attribute DES_SECON_NM. */
	private String lastName;

	/** Attribute DES_CHL_ADL1. */
	private String addressLine1;

	/** Attribute DES_CHL_ADL2. */
	private String addressLine2;

	/** Attribute DES_CITY_C. */
	private String city;

	/** Attribute COD_STATE_C. */
	private String state;

	/** Attribute COD_ISO_CTRY. */
	private String codISOCountry;

	/** Attribute COD_CHL_POST. */
	private String codPostal;

	/** Attribute DES_CHL_ADL3. */
	private String addressLine3;

	/** Attribute DES_MAIL_STP. */
	private String mailStop;

	/** Attribute COD_PHONE_NM. */
	private String phoneNumber;

	/** Attribute COD_FAX_NUM. */
	private String faxNumber;

	/** Attribute COD_CH_OTH. */
	private String codCardHolderOTH;

	/** Attribute QNU_TRAIN_DT. */
	private Long trainingDate;

	/** Attribute DES_MAIL_ADD. */
	private String emailAddress;

	/** Attribute DES_AUTHORI1. */
	private String authorizduser1;

	/** Attribute DES_AUTHORI2. */
	private String authorizduser2;

	/** Attribute DES_AUTHORI3. */
	private String authorizduser3;

	/** Attribute COD_EMPLOYEE. */
	private String codEmployee;

	/** Attribute COD_HOME_PHN. */
	private String homePhone;

	/** Attribute DES_MIDDL_NM. */
	private String middleName;

	/** Attribute COD_VISA_BUY. */
	private String visaBuyer;

	/** Attribute COD_VEHICLE. */
	private String codVehicle;

	/** Attribute DES_MISC_FD1. */
	private String miscField1;

	/** Attribute DES_MS_F1_DS. */
	private String miscField1DS;

	/** Attribute DES_MISC_FD2. */
	private String miscField2;

	/** Attribute DES_MS_F2_DS. */
	private String miscField2DS;

	/** Attribute DES_OPT1. */
	private String optionalField1;

	/** Attribute DES_OPT2. */
	private String optionalField2;

	/** Attribute DES_OPT3. */
	private String optionalField3;

	/** Attribute DES_OPT4. */
	private String optionalField4;

		
	public CardHolder() {
		super();
	}

	public CardHolder(final String codCarga, final Long codCompId, final String idCardHolder,
			final String hierarchyNode, final String firstName, final String lastName,
			final String addressLine1, final String addressLine2, final String city,
			final String state, final String codISOCountry, final String codPostal,
			final String addressLine3, final String mailStop, final String phoneNumber,
			final String faxNumber, final String codCardHolderOTH, final Long trainingDate,
			final String emailAddress, final String authorizduser1, final String authorizduser2,
			final String authorizduser3, final String codEmployee, final String homePhone,
			final String middleName, final String visaBuyer, final String codVehicle,
			final String miscField1, final String miscField1DS, final String miscField2,
			final String miscField2DS, final String optionalField1, final String optionalField2,
			final String optionalField3, final String optionalField4) {
		super();
		this.codCarga = codCarga;
		this.codCompId = codCompId;
		this.idCardHolder = idCardHolder;
		this.hierarchyNode = hierarchyNode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.codISOCountry = codISOCountry;
		this.codPostal = codPostal;
		this.addressLine3 = addressLine3;
		this.mailStop = mailStop;
		this.phoneNumber = phoneNumber;
		this.faxNumber = faxNumber;
		this.codCardHolderOTH = codCardHolderOTH;
		this.trainingDate = trainingDate;
		this.emailAddress = emailAddress;
		this.authorizduser1 = authorizduser1;
		this.authorizduser2 = authorizduser2;
		this.authorizduser3 = authorizduser3;
		this.codEmployee = codEmployee;
		this.homePhone = homePhone;
		this.middleName = middleName;
		this.visaBuyer = visaBuyer;
		this.codVehicle = codVehicle;
		this.miscField1 = miscField1;
		this.miscField1DS = miscField1DS;
		this.miscField2 = miscField2;
		this.miscField2DS = miscField2DS;
		this.optionalField1 = optionalField1;
		this.optionalField2 = optionalField2;
		this.optionalField3 = optionalField3;
		this.optionalField4 = optionalField4;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the codCompId
	 */
	public Long getCodCompId() {
		return codCompId;
	}

	/**
	 * @param codCompId the codCompId to set
	 */
	public void setCodCompId(final Long codCompId) {
		this.codCompId = codCompId;
	}

	/**
	 * @return the idCardHolder
	 */
	public String getIdCardHolder() {
		return idCardHolder;
	}

	/**
	 * @param idCardHolder the idCardHolder to set
	 */
	public void setIdCardHolder(final String idCardHolder) {
		this.idCardHolder = idCardHolder;
	}

	/**
	 * @return the hierarchyNode
	 */
	public String getHierarchyNode() {
		return hierarchyNode;
	}

	/**
	 * @param hierarchyNode the hierarchyNode to set
	 */
	public void setHierarchyNode(final String hierarchyNode) {
		this.hierarchyNode = hierarchyNode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(final String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(final String state) {
		this.state = state;
	}

	/**
	 * @return the codISOCountry
	 */
	public String getCodISOCountry() {
		return codISOCountry;
	}

	/**
	 * @param codISOCountry the codISOCountry to set
	 */
	public void setCodISOCountry(final String codISOCountry) {
		this.codISOCountry = codISOCountry;
	}

	/**
	 * @return the codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}

	/**
	 * @param codPostal the codPostal to set
	 */
	public void setCodPostal(final String codPostal) {
		this.codPostal = codPostal;
	}

	/**
	 * @return the addressLine3
	 */
	public String getAddressLine3() {
		return addressLine3;
	}

	/**
	 * @param addressLine3 the addressLine3 to set
	 */
	public void setAddressLine3(final String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	/**
	 * @return the mailStop
	 */
	public String getMailStop() {
		return mailStop;
	}

	/**
	 * @param mailStop the mailStop to set
	 */
	public void setMailStop(final String mailStop) {
		this.mailStop = mailStop;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber the faxNumber to set
	 */
	public void setFaxNumber(final String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the codCardHolderOTH
	 */
	public String getCodCardHolderOTH() {
		return codCardHolderOTH;
	}

	/**
	 * @param codCardHolderOTH the codCardHolderOTH to set
	 */
	public void setCodCardHolderOTH(final String codCardHolderOTH) {
		this.codCardHolderOTH = codCardHolderOTH;
	}

	/**
	 * @return the trainingDate
	 */
	public Long getTrainingDate() {
		return trainingDate;
	}

	/**
	 * @param trainingDate the trainingDate to set
	 */
	public void setTrainingDate(final Long trainingDate) {
		this.trainingDate = trainingDate;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(final String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the authorizduser1
	 */
	public String getAuthorizduser1() {
		return authorizduser1;
	}

	/**
	 * @param authorizduser1 the authorizduser1 to set
	 */
	public void setAuthorizduser1(final String authorizduser1) {
		this.authorizduser1 = authorizduser1;
	}

	/**
	 * @return the authorizduser2
	 */
	public String getAuthorizduser2() {
		return authorizduser2;
	}

	/**
	 * @param authorizduser2 the authorizduser2 to set
	 */
	public void setAuthorizduser2(final String authorizduser2) {
		this.authorizduser2 = authorizduser2;
	}

	/**
	 * @return the authorizduser3
	 */
	public String getAuthorizduser3() {
		return authorizduser3;
	}

	/**
	 * @param authorizduser3 the authorizduser3 to set
	 */
	public void setAuthorizduser3(final String authorizduser3) {
		this.authorizduser3 = authorizduser3;
	}

	/**
	 * @return the codEmployee
	 */
	public String getCodEmployee() {
		return codEmployee;
	}

	/**
	 * @param codEmployee the codEmployee to set
	 */
	public void setCodEmployee(final String codEmployee) {
		this.codEmployee = codEmployee;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone the homePhone to set
	 */
	public void setHomePhone(final String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the visaBuyer
	 */
	public String getVisaBuyer() {
		return visaBuyer;
	}

	/**
	 * @param visaBuyer the visaBuyer to set
	 */
	public void setVisaBuyer(final String visaBuyer) {
		this.visaBuyer = visaBuyer;
	}

	/**
	 * @return the codVehicle
	 */
	public String getCodVehicle() {
		return codVehicle;
	}

	/**
	 * @param codVehicle the codVehicle to set
	 */
	public void setCodVehicle(final String codVehicle) {
		this.codVehicle = codVehicle;
	}

	/**
	 * @return the miscField1
	 */
	public String getMiscField1() {
		return miscField1;
	}

	/**
	 * @param miscField1 the miscField1 to set
	 */
	public void setMiscField1(final String miscField1) {
		this.miscField1 = miscField1;
	}

	/**
	 * @return the miscField1DS
	 */
	public String getMiscField1DS() {
		return miscField1DS;
	}

	/**
	 * @param miscField1DS the miscField1DS to set
	 */
	public void setMiscField1DS(final String miscField1DS) {
		this.miscField1DS = miscField1DS;
	}

	/**
	 * @return the miscField2
	 */
	public String getMiscField2() {
		return miscField2;
	}

	/**
	 * @param miscField2 the miscField2 to set
	 */
	public void setMiscField2(final String miscField2) {
		this.miscField2 = miscField2;
	}

	/**
	 * @return the miscField2DS
	 */
	public String getMiscField2DS() {
		return miscField2DS;
	}

	/**
	 * @param miscField2DS the miscField2DS to set
	 */
	public void setMiscField2DS(final String miscField2DS) {
		this.miscField2DS = miscField2DS;
	}

	/**
	 * @return the optionalField1
	 */
	public String getOptionalField1() {
		return optionalField1;
	}

	/**
	 * @param optionalField1 the optionalField1 to set
	 */
	public void setOptionalField1(final String optionalField1) {
		this.optionalField1 = optionalField1;
	}

	/**
	 * @return the optionalField2
	 */
	public String getOptionalField2() {
		return optionalField2;
	}

	/**
	 * @param optionalField2 the optionalField2 to set
	 */
	public void setOptionalField2(final String optionalField2) {
		this.optionalField2 = optionalField2;
	}

	/**
	 * @return the optionalField3
	 */
	public String getOptionalField3() {
		return optionalField3;
	}

	/**
	 * @param optionalField3 the optionalField3 to set
	 */
	public void setOptionalField3(final String optionalField3) {
		this.optionalField3 = optionalField3;
	}

	/**
	 * @return the optionalField4
	 */
	public String getOptionalField4() {
		return optionalField4;
	}

	/**
	 * @param optionalField4 the optionalField4 to set
	 */
	public void setOptionalField4(final String optionalField4) {
		this.optionalField4 = optionalField4;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CardHolder [codCarga=");
		builder.append(codCarga);
		builder.append(", codCompId=");
		builder.append(codCompId);
		builder.append(", idCardHolder=");
		builder.append(idCardHolder);
		builder.append(", hierarchyNode=");
		builder.append(hierarchyNode);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", addressLine1=");
		builder.append(addressLine1);
		builder.append(", addressLine2=");
		builder.append(addressLine2);
		builder.append(", city=");
		builder.append(city);
		builder.append(", state=");
		builder.append(state);
		builder.append(", codISOCountry=");
		builder.append(codISOCountry);
		builder.append(", codPostal=");
		builder.append(codPostal);
		builder.append(", addressLine3=");
		builder.append(addressLine3);
		builder.append(", mailStop=");
		builder.append(mailStop);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append(", faxNumber=");
		builder.append(faxNumber);
		builder.append(", codCardHolderOTH=");
		builder.append(codCardHolderOTH);
		builder.append(", trainingDate=");
		builder.append(trainingDate);
		builder.append(", emailAddress=");
		builder.append(emailAddress);
		builder.append(", authorizduser1=");
		builder.append(authorizduser1);
		builder.append(", authorizduser2=");
		builder.append(authorizduser2);
		builder.append(", authorizduser3=");
		builder.append(authorizduser3);
		builder.append(", codEmployee=");
		builder.append(codEmployee);
		builder.append(", homePhone=");
		builder.append(homePhone);
		builder.append(", middleName=");
		builder.append(middleName);
		builder.append(", visaBuyer=");
		builder.append(visaBuyer);
		builder.append(", codVehicle=");
		builder.append(codVehicle);
		builder.append(", miscField1=");
		builder.append(miscField1);
		builder.append(", miscField1DS=");
		builder.append(miscField1DS);
		builder.append(", miscField2=");
		builder.append(miscField2);
		builder.append(", miscField2DS=");
		builder.append(miscField2DS);
		builder.append(", optionalField1=");
		builder.append(optionalField1);
		builder.append(", optionalField2=");
		builder.append(optionalField2);
		builder.append(", optionalField3=");
		builder.append(optionalField3);
		builder.append(", optionalField4=");
		builder.append(optionalField4);
		builder.append("]");
		return builder.toString();
	}


	
}
