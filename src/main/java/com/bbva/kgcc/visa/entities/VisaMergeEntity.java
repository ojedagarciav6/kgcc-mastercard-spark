package com.bbva.kgcc.visa.entities;

import java.sql.Timestamp;

public class VisaMergeEntity {
	
	
	private Long codCompId;
	private Long postingDate;
	private String codTransaction;
	private Long codSeqFich;
	private Integer codCardTp;
	private Integer codAcquiringBin;
	private String codCardAcceptor;
	private String supplierName;
	private String supplierCity;
	private String supplierState;
	private String codIsoCountryCTR;
	private String codSupplrPostal;
	private Long sourceAmount;
	private Long billingAmount;
	private Integer codSourceCurrency;
	private Long merchantCat;
	private String codTransType;
	private Integer codBillingCurrency;
	private Long authDate;
	private Long taxAmount;
	private Timestamp fechaFirstIn;
	private String codCarHolder;
	private String accountNum;
	private Long effectiveDate;
	private String statusCode;
	private String reasonStatus;
	private String cardType;
	private String codAccountType;
	private Integer statementType;
	private String accountExpired;
	private String idCardHolder;
	private String firstName;
	private Integer codPeriod;
	private Long startDate;
	private Long endDate;
	private Integer codPeriodComplet;
	private String companyName;
	private String addressLine1;
	private String addressLine2;
	private String cicty;
	private String state;
	private String codIsoCountry;
	private String codPostal;
	private String email;
	private String hierarchyNode;
	private String parntHierrchy;
	private Long sequence;
	private String codFileId;
	
	
	
	public VisaMergeEntity() {
		super();
	}



	public VisaMergeEntity(Long codCompId, Long postingDate, String codTransaction, Long codSeqFich, Integer codCardTp,
			Integer codAcquiringBin, String codCardAcceptor, String supplierName, String supplierCity,
			String supplierState, String codIsoCountryCTR, String codSupplrPostal, Long sourceAmount,
			Long billingAmount, Integer codSourceCurrency, Long merchantCat, String codTransType,
			Integer codBillingCurrency, Long authDate, Long taxAmount, Timestamp fechaFirstIn, String codCarHolder,
			String accountNum, Long effectiveDate, String statusCode, String reasonStatus, String cardType,
			String codAccountType, Integer statementType, String accountExpired, String idCardHolder, String firstName,
			Integer codPeriod, Long startDate, Long endDate, Integer codPeriodComplet, String companyName,
			String addressLine1, String addressLine2, String cicty, String state, String codIsoCountry,
			String codPostal, String email, String hierarchyNode, String parntHierrchy) {
		super();
		this.codCompId = codCompId;
		this.postingDate = postingDate;
		this.codTransaction = codTransaction;
		this.codSeqFich = codSeqFich;
		this.codCardTp = codCardTp;
		this.codAcquiringBin = codAcquiringBin;
		this.codCardAcceptor = codCardAcceptor;
		this.supplierName = supplierName;
		this.supplierCity = supplierCity;
		this.supplierState = supplierState;
		this.codIsoCountryCTR = codIsoCountryCTR;
		this.codSupplrPostal = codSupplrPostal;
		this.sourceAmount = sourceAmount;
		this.billingAmount = billingAmount;
		this.codSourceCurrency = codSourceCurrency;
		this.merchantCat = merchantCat;
		this.codTransType = codTransType;
		this.codBillingCurrency = codBillingCurrency;
		this.authDate = authDate;
		this.taxAmount = taxAmount;
		this.fechaFirstIn = fechaFirstIn;
		this.codCarHolder = codCarHolder;
		this.accountNum = accountNum;
		this.effectiveDate = effectiveDate;
		this.statusCode = statusCode;
		this.reasonStatus = reasonStatus;
		this.cardType = cardType;
		this.codAccountType = codAccountType;
		this.statementType = statementType;
		this.accountExpired = accountExpired;
		this.idCardHolder = idCardHolder;
		this.firstName = firstName;
		this.codPeriod = codPeriod;
		this.startDate = startDate;
		this.endDate = endDate;
		this.codPeriodComplet = codPeriodComplet;
		this.companyName = companyName;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.cicty = cicty;
		this.state = state;
		this.codIsoCountry = codIsoCountry;
		this.codPostal = codPostal;
		this.email = email;
		this.hierarchyNode = hierarchyNode;
		this.parntHierrchy = parntHierrchy;
	}



	public Long getCodCompId() {
		return codCompId;
	}



	public void setCodCompId(Long codCompId) {
		this.codCompId = codCompId;
	}



	public Long getPostingDate() {
		return postingDate;
	}



	public void setPostingDate(Long postingDate) {
		this.postingDate = postingDate;
	}



	public String getCodTransaction() {
		return codTransaction;
	}



	public void setCodTransaction(String codTransaction) {
		this.codTransaction = codTransaction;
	}



	public Long getCodSeqFich() {
		return codSeqFich;
	}



	public void setCodSeqFich(Long codSeqFich) {
		this.codSeqFich = codSeqFich;
	}



	public Integer getCodCardTp() {
		return codCardTp;
	}



	public void setCodCardTp(Integer codCardTp) {
		this.codCardTp = codCardTp;
	}



	public Integer getCodAcquiringBin() {
		return codAcquiringBin;
	}



	public void setCodAcquiringBin(Integer codAcquiringBin) {
		this.codAcquiringBin = codAcquiringBin;
	}



	public String getCodCardAcceptor() {
		return codCardAcceptor;
	}



	public void setCodCardAcceptor(String codCardAcceptor) {
		this.codCardAcceptor = codCardAcceptor;
	}



	public String getSupplierName() {
		return supplierName;
	}



	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}



	public String getSupplierCity() {
		return supplierCity;
	}



	public void setSupplierCity(String supplierCity) {
		this.supplierCity = supplierCity;
	}



	public String getSupplierState() {
		return supplierState;
	}



	public void setSupplierState(String supplierState) {
		this.supplierState = supplierState;
	}



	public String getCodIsoCountryCTR() {
		return codIsoCountryCTR;
	}



	public void setCodIsoCountryCTR(String codIsoCountryCTR) {
		this.codIsoCountryCTR = codIsoCountryCTR;
	}



	public String getCodSupplrPostal() {
		return codSupplrPostal;
	}



	public void setCodSupplrPostal(String codSupplrPostal) {
		this.codSupplrPostal = codSupplrPostal;
	}



	public Long getSourceAmount() {
		return sourceAmount;
	}



	public void setSourceAmount(Long sourceAmount) {
		this.sourceAmount = sourceAmount;
	}



	public Long getBillingAmount() {
		return billingAmount;
	}



	public void setBillingAmount(Long billingAmount) {
		this.billingAmount = billingAmount;
	}



	public Integer getCodSourceCurrency() {
		return codSourceCurrency;
	}



	public void setCodSourceCurrency(Integer codSourceCurrency) {
		this.codSourceCurrency = codSourceCurrency;
	}



	public Long getMerchantCat() {
		return merchantCat;
	}



	public void setMerchantCat(Long merchantCat) {
		this.merchantCat = merchantCat;
	}



	public String getCodTransType() {
		return codTransType;
	}



	public void setCodTransType(String codTransType) {
		this.codTransType = codTransType;
	}



	public Integer getCodBillingCurrency() {
		return codBillingCurrency;
	}



	public void setCodBillingCurrency(Integer codBillingCurrency) {
		this.codBillingCurrency = codBillingCurrency;
	}



	public Long getAuthDate() {
		return authDate;
	}



	public void setAuthDate(Long authDate) {
		this.authDate = authDate;
	}



	public Long getTaxAmount() {
		return taxAmount;
	}



	public void setTaxAmount(Long taxAmount) {
		this.taxAmount = taxAmount;
	}



	public Timestamp getFechaFirstIn() {
		return fechaFirstIn;
	}



	public void setFechaFirstIn(Timestamp fechaFirstIn) {
		this.fechaFirstIn = fechaFirstIn;
	}



	public String getCodCarHolder() {
		return codCarHolder;
	}



	public void setCodCarHolder(String codCarHolder) {
		this.codCarHolder = codCarHolder;
	}



	public String getAccountNum() {
		return accountNum;
	}



	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}



	public Long getEffectiveDate() {
		return effectiveDate;
	}



	public void setEffectiveDate(Long effectiveDate) {
		this.effectiveDate = effectiveDate;
	}



	public String getStatusCode() {
		return statusCode;
	}



	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}



	public String getReasonStatus() {
		return reasonStatus;
	}



	public void setReasonStatus(String reasonStatus) {
		this.reasonStatus = reasonStatus;
	}



	public String getCardType() {
		return cardType;
	}



	public void setCardType(String cardType) {
		this.cardType = cardType;
	}



	public String getCodAccountType() {
		return codAccountType;
	}



	public void setCodAccountType(String codAccountType) {
		this.codAccountType = codAccountType;
	}



	public Integer getStatementType() {
		return statementType;
	}



	public void setStatementType(Integer statementType) {
		this.statementType = statementType;
	}



	public String getAccountExpired() {
		return accountExpired;
	}



	public void setAccountExpired(String accountExpired) {
		this.accountExpired = accountExpired;
	}



	public String getIdCardHolder() {
		return idCardHolder;
	}



	public void setIdCardHolder(String idCardHolder) {
		this.idCardHolder = idCardHolder;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public Integer getCodPeriod() {
		return codPeriod;
	}



	public void setCodPeriod(Integer codPeriod) {
		this.codPeriod = codPeriod;
	}



	public Long getStartDate() {
		return startDate;
	}



	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}



	public Long getEndDate() {
		return endDate;
	}



	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}



	public Integer getCodPeriodComplet() {
		return codPeriodComplet;
	}



	public void setCodPeriodComplet(Integer codPeriodComplet) {
		this.codPeriodComplet = codPeriodComplet;
	}



	public String getCompanyName() {
		return companyName;
	}



	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}



	public String getAddressLine1() {
		return addressLine1;
	}



	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}



	public String getAddressLine2() {
		return addressLine2;
	}



	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}



	public String getCicty() {
		return cicty;
	}



	public void setCicty(String cicty) {
		this.cicty = cicty;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getCodIsoCountry() {
		return codIsoCountry;
	}



	public void setCodIsoCountry(String codIsoCountry) {
		this.codIsoCountry = codIsoCountry;
	}



	public String getCodPostal() {
		return codPostal;
	}



	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getHierarchyNode() {
		return hierarchyNode;
	}



	public void setHierarchyNode(String hierarchyNode) {
		this.hierarchyNode = hierarchyNode;
	}



	public String getParntHierrchy() {
		return parntHierrchy;
	}



	public void setParntHierrchy(String parntHierrchy) {
		this.parntHierrchy = parntHierrchy;
	}
	
	public Long getSequence() {
		return sequence;
	}

	public Long nextSequence() {
		this.sequence = this.sequence + 10L;
		return this.sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getCodFileId() {
		return codFileId;
	}

	public void setCodFileId(String codFileId) {
		this.codFileId = codFileId;
	}
	

}
