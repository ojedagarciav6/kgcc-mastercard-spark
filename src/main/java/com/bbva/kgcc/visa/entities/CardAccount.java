package com.bbva.kgcc.visa.entities;

/**
 * The Class CardAccount.
 */
public class CardAccount {

	/** Attribute COD_LOAD_TR. */
	private String codCarga;

	/** Attribute COD_CARDHOLD. */
	private String codCarHolder;

	/** Attribute COD_ACC_NUM. */
	private String accountNum;

	/** Attribute COD_COMP_ID. */
	private Long codCompId;

	/** Attribute COD_HIER_NOD. */
	private String hierarchyNode;

	/** Attribute QNU_EFFEC_DT. */
	private Long effectiveDate;

	/** Attribute QNU_ACC_OPEN. */
	private Long accountOpen;

	/** Attribute QNU_ACC_CLOS. */
	private Long accountClose;

	/** Attribute QNU_CARD_EXP. */
	private Long accountExpired;

	/** Attribute COD_CARD_TP. */
	private Integer cardType;

	/** Attribute IMP_SPEN_LIM. */
	private Long spendingLimit;

	/** Attribute QNU_STAT_TP. */
	private Integer statementType;

	/** Attribute QNU_LAST_REV. */
	private Long lastRevision;

	/** Attribute IMP_TRN_LIM. */
	private Long transLimit;

	/** Attribute COD_CORP_PAY. */
	private Integer corporatePay;

	/** Attribute DES_BLL_ACC. */
	private String billingAccount;

	/** Attribute DES_COST_CNT. */
	private String costCenter;

	/** Attribute DES_SUB_ACCO. */
	private String subAccount;

	/** Attribute QNU_DAY_LIM. */
	private Long dailyLimit;

	/** Attribute QNU_CYC_LIM. */
	private Long cycleLimit;

	/** Attribute IMP_CASH_LIM. */
	private Long cashLimit;

	/** Attribute COD_STA_COD. */
	private Integer statusCode;

	/** Attribute COD_REAS_STA. */
	private Integer reasonStatus;

	/** Attribute QNU_STA_DT. */
	private Long statusDate;

	/** Attribute COD_PR_FOU_I. */
	private Integer codPrFoundedInd;

	/** Attribute COD_CT_PA_PR. */
	private Integer codCityPairProg;

	/** Attribute DES_TASK_ORD. */
	private String taskOrder;

	/** Attribute COD_FLE_SERV. */
	private Integer codFleetService;

	/** Attribute COD_CRED_RAT. */
	private String codCreditRating;

	/** Attribute QRA_RAT_DT. */
	private Long ratingDate;

	/** Attribute COD_ANN_FEE. */
	private Integer codAnnualFee;

	/** Attribute COD_FEE_MON. */
	private Integer codMonthFee;

	/** Attribute COD_CARD_VER. */
	private Integer codCardVerif;

	/** Attribute XTI_CHCK. */
	private Integer checkIndicator;

	/** Attribute COD_ACC_TYPE. */
	private Integer codAccountType;

	/** Attribute QNU_LST_STOL. */
	private Long LostStolen;

	/** Attribute QNU_CHAR_OFF. */
	private Long chargeOff;

	/** Attribute IMP_CH_OF_AM. */
	private Long chargeOffAMN;

	/** Attribute COD_TRN_ACC. */
	private String transferAcc;

	/** Attribute COD_PHONE_TP. */
	private String phoneType;

	/** Attribute DES_EMBOSSL1. */
	private String embossline1;

	/** Attribute DES_EMBOSSL2. */
	private String embossline2;

	/** Attribute QNU_L_CRD_LI. */
	private Long lCreditLimit;

	/** Attribute QNU_MAINT_DT. */
	private Long maintDate;

	/** Attribute DES_OPT_FD1. */
	private String optionalField1;

	/** Attribute DES_OPT_FD2. */
	private String optionalField2;

	/** Attribute DES_OPT_FD3. */
	private String optionalField3;

	/** Attribute DES_OPT_FD4. */
	private String optionalField4;

	public CardAccount(final String codCarga, final String codCarHolder, final String accountNum,
			final Long codCompId, final String hierarchyNode, final Long effectiveDate,
			final Long accountOpen, final Long accountClose, final Long accountExpired,
			final Integer cardType, final Long spendingLimit, final Integer statementType,
			final Long lastRevision, final Long transLimit, final Integer corporatePay,
			final String billingAccount, final String costCenter, final String subAccount,
			final Long dailyLimit, final Long cycleLimit, final Long cashLimit,
			final Integer statusCode, final Integer reasonStatus, final Long statusDate,
			final Integer codPrFoundedInd, final Integer codCityPairProg, final String taskOrder,
			final Integer codFleetService, final String codCreditRating, final Long ratingDate,
			final Integer codAnnualFee, final Integer codMonthFee, final Integer codCardVerif,
			final Integer checkIndicator, final Integer codAccountType, final Long lostStolen,
			final Long chargeOff, final Long chargeOffAMN, final String transferAcc,
			final String phoneType, final String embossline1, final String embossline2,
			final Long lCreditLimit, final Long maintDate, final String optionalField1,
			final String optionalField2, final String optionalField3, final String optionalField4) {
		super();
		this.codCarga = codCarga;
		this.codCarHolder = codCarHolder;
		this.accountNum = accountNum;
		this.codCompId = codCompId;
		this.hierarchyNode = hierarchyNode;
		this.effectiveDate = effectiveDate;
		this.accountOpen = accountOpen;
		this.accountClose = accountClose;
		this.accountExpired = accountExpired;
		this.cardType = cardType;
		this.spendingLimit = spendingLimit;
		this.statementType = statementType;
		this.lastRevision = lastRevision;
		this.transLimit = transLimit;
		this.corporatePay = corporatePay;
		this.billingAccount = billingAccount;
		this.costCenter = costCenter;
		this.subAccount = subAccount;
		this.dailyLimit = dailyLimit;
		this.cycleLimit = cycleLimit;
		this.cashLimit = cashLimit;
		this.statusCode = statusCode;
		this.reasonStatus = reasonStatus;
		this.statusDate = statusDate;
		this.codPrFoundedInd = codPrFoundedInd;
		this.codCityPairProg = codCityPairProg;
		this.taskOrder = taskOrder;
		this.codFleetService = codFleetService;
		this.codCreditRating = codCreditRating;
		this.ratingDate = ratingDate;
		this.codAnnualFee = codAnnualFee;
		this.codMonthFee = codMonthFee;
		this.codCardVerif = codCardVerif;
		this.checkIndicator = checkIndicator;
		this.codAccountType = codAccountType;
		LostStolen = lostStolen;
		this.chargeOff = chargeOff;
		this.chargeOffAMN = chargeOffAMN;
		this.transferAcc = transferAcc;
		this.phoneType = phoneType;
		this.embossline1 = embossline1;
		this.embossline2 = embossline2;
		this.lCreditLimit = lCreditLimit;
		this.maintDate = maintDate;
		this.optionalField1 = optionalField1;
		this.optionalField2 = optionalField2;
		this.optionalField3 = optionalField3;
		this.optionalField4 = optionalField4;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the codCarHolder
	 */
	public String getCodCarHolder() {
		return codCarHolder;
	}

	/**
	 * @param codCarHolder the codCarHolder to set
	 */
	public void setCodCarHolder(final String codCarHolder) {
		this.codCarHolder = codCarHolder;
	}

	/**
	 * @return the accountNum
	 */
	public String getAccountNum() {
		return accountNum;
	}

	/**
	 * @param accountNum the accountNum to set
	 */
	public void setAccountNum(final String accountNum) {
		this.accountNum = accountNum;
	}

	/**
	 * @return the codCompId
	 */
	public Long getCodCompId() {
		return codCompId;
	}

	/**
	 * @param codCompId the codCompId to set
	 */
	public void setCodCompId(final Long codCompId) {
		this.codCompId = codCompId;
	}

	/**
	 * @return the hierarchyNode
	 */
	public String getHierarchyNode() {
		return hierarchyNode;
	}

	/**
	 * @param hierarchyNode the hierarchyNode to set
	 */
	public void setHierarchyNode(final String hierarchyNode) {
		this.hierarchyNode = hierarchyNode;
	}

	/**
	 * @return the effectiveDate
	 */
	public Long getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(final Long effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the accountOpen
	 */
	public Long getAccountOpen() {
		return accountOpen;
	}

	/**
	 * @param accountOpen the accountOpen to set
	 */
	public void setAccountOpen(final Long accountOpen) {
		this.accountOpen = accountOpen;
	}

	/**
	 * @return the accountClose
	 */
	public Long getAccountClose() {
		return accountClose;
	}

	/**
	 * @param accountClose the accountClose to set
	 */
	public void setAccountClose(final Long accountClose) {
		this.accountClose = accountClose;
	}

	/**
	 * @return the accountExpired
	 */
	public Long getAccountExpired() {
		return accountExpired;
	}

	/**
	 * @param accountExpired the accountExpired to set
	 */
	public void setAccountExpired(final Long accountExpired) {
		this.accountExpired = accountExpired;
	}

	/**
	 * @return the cardType
	 */
	public Integer getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(final Integer cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the spendingLimit
	 */
	public Long getSpendingLimit() {
		return spendingLimit;
	}

	/**
	 * @param spendingLimit the spendingLimit to set
	 */
	public void setSpendingLimit(final Long spendingLimit) {
		this.spendingLimit = spendingLimit;
	}

	/**
	 * @return the statementType
	 */
	public Integer getStatementType() {
		return statementType;
	}

	/**
	 * @param statementType the statementType to set
	 */
	public void setStatementType(final Integer statementType) {
		this.statementType = statementType;
	}

	/**
	 * @return the lastRevision
	 */
	public Long getLastRevision() {
		return lastRevision;
	}

	/**
	 * @param lastRevision the lastRevision to set
	 */
	public void setLastRevision(final Long lastRevision) {
		this.lastRevision = lastRevision;
	}

	/**
	 * @return the transLimit
	 */
	public Long getTransLimit() {
		return transLimit;
	}

	/**
	 * @param transLimit the transLimit to set
	 */
	public void setTransLimit(final Long transLimit) {
		this.transLimit = transLimit;
	}

	/**
	 * @return the corporatePay
	 */
	public Integer getCorporatePay() {
		return corporatePay;
	}

	/**
	 * @param corporatePay the corporatePay to set
	 */
	public void setCorporatePay(final Integer corporatePay) {
		this.corporatePay = corporatePay;
	}

	/**
	 * @return the billingAccount
	 */
	public String getBillingAccount() {
		return billingAccount;
	}

	/**
	 * @param billingAccount the billingAccount to set
	 */
	public void setBillingAccount(final String billingAccount) {
		this.billingAccount = billingAccount;
	}

	/**
	 * @return the costCenter
	 */
	public String getCostCenter() {
		return costCenter;
	}

	/**
	 * @param costCenter the costCenter to set
	 */
	public void setCostCenter(final String costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * @return the subAccount
	 */
	public String getSubAccount() {
		return subAccount;
	}

	/**
	 * @param subAccount the subAccount to set
	 */
	public void setSubAccount(final String subAccount) {
		this.subAccount = subAccount;
	}

	/**
	 * @return the dailyLimit
	 */
	public Long getDailyLimit() {
		return dailyLimit;
	}

	/**
	 * @param dailyLimit the dailyLimit to set
	 */
	public void setDailyLimit(final Long dailyLimit) {
		this.dailyLimit = dailyLimit;
	}

	/**
	 * @return the cycleLimit
	 */
	public Long getCycleLimit() {
		return cycleLimit;
	}

	/**
	 * @param cycleLimit the cycleLimit to set
	 */
	public void setCycleLimit(final Long cycleLimit) {
		this.cycleLimit = cycleLimit;
	}

	/**
	 * @return the cashLimit
	 */
	public Long getCashLimit() {
		return cashLimit;
	}

	/**
	 * @param cashLimit the cashLimit to set
	 */
	public void setCashLimit(final Long cashLimit) {
		this.cashLimit = cashLimit;
	}

	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(final Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the reasonStatus
	 */
	public Integer getReasonStatus() {
		return reasonStatus;
	}

	/**
	 * @param reasonStatus the reasonStatus to set
	 */
	public void setReasonStatus(final Integer reasonStatus) {
		this.reasonStatus = reasonStatus;
	}

	/**
	 * @return the statusDate
	 */
	public Long getStatusDate() {
		return statusDate;
	}

	/**
	 * @param statusDate the statusDate to set
	 */
	public void setStatusDate(final Long statusDate) {
		this.statusDate = statusDate;
	}

	/**
	 * @return the codPrFoundedInd
	 */
	public Integer getCodPrFoundedInd() {
		return codPrFoundedInd;
	}

	/**
	 * @param codPrFoundedInd the codPrFoundedInd to set
	 */
	public void setCodPrFoundedInd(final Integer codPrFoundedInd) {
		this.codPrFoundedInd = codPrFoundedInd;
	}

	/**
	 * @return the codCityPairProg
	 */
	public Integer getCodCityPairProg() {
		return codCityPairProg;
	}

	/**
	 * @param codCityPairProg the codCityPairProg to set
	 */
	public void setCodCityPairProg(final Integer codCityPairProg) {
		this.codCityPairProg = codCityPairProg;
	}

	/**
	 * @return the taskOrder
	 */
	public String getTaskOrder() {
		return taskOrder;
	}

	/**
	 * @param taskOrder the taskOrder to set
	 */
	public void setTaskOrder(final String taskOrder) {
		this.taskOrder = taskOrder;
	}

	/**
	 * @return the codFleetService
	 */
	public Integer getCodFleetService() {
		return codFleetService;
	}

	/**
	 * @param codFleetService the codFleetService to set
	 */
	public void setCodFleetService(final Integer codFleetService) {
		this.codFleetService = codFleetService;
	}

	/**
	 * @return the codCreditRating
	 */
	public String getCodCreditRating() {
		return codCreditRating;
	}

	/**
	 * @param codCreditRating the codCreditRating to set
	 */
	public void setCodCreditRating(final String codCreditRating) {
		this.codCreditRating = codCreditRating;
	}

	/**
	 * @return the ratingDate
	 */
	public Long getRatingDate() {
		return ratingDate;
	}

	/**
	 * @param ratingDate the ratingDate to set
	 */
	public void setRatingDate(final Long ratingDate) {
		this.ratingDate = ratingDate;
	}

	/**
	 * @return the codAnnualFee
	 */
	public Integer getCodAnnualFee() {
		return codAnnualFee;
	}

	/**
	 * @param codAnnualFee the codAnnualFee to set
	 */
	public void setCodAnnualFee(final Integer codAnnualFee) {
		this.codAnnualFee = codAnnualFee;
	}

	/**
	 * @return the codMonthFee
	 */
	public Integer getCodMonthFee() {
		return codMonthFee;
	}

	/**
	 * @param codMonthFee the codMonthFee to set
	 */
	public void setCodMonthFee(final Integer codMonthFee) {
		this.codMonthFee = codMonthFee;
	}

	/**
	 * @return the codCardVerif
	 */
	public Integer getCodCardVerif() {
		return codCardVerif;
	}

	/**
	 * @param codCardVerif the codCardVerif to set
	 */
	public void setCodCardVerif(final Integer codCardVerif) {
		this.codCardVerif = codCardVerif;
	}

	/**
	 * @return the checkIndicator
	 */
	public Integer getCheckIndicator() {
		return checkIndicator;
	}

	/**
	 * @param checkIndicator the checkIndicator to set
	 */
	public void setCheckIndicator(final Integer checkIndicator) {
		this.checkIndicator = checkIndicator;
	}

	/**
	 * @return the codAccountType
	 */
	public Integer getCodAccountType() {
		return codAccountType;
	}

	/**
	 * @param codAccountType the codAccountType to set
	 */
	public void setCodAccountType(final Integer codAccountType) {
		this.codAccountType = codAccountType;
	}

	/**
	 * @return the lostStolen
	 */
	public Long getLostStolen() {
		return LostStolen;
	}

	/**
	 * @param lostStolen the lostStolen to set
	 */
	public void setLostStolen(final Long lostStolen) {
		LostStolen = lostStolen;
	}

	/**
	 * @return the chargeOff
	 */
	public Long getChargeOff() {
		return chargeOff;
	}

	/**
	 * @param chargeOff the chargeOff to set
	 */
	public void setChargeOff(final Long chargeOff) {
		this.chargeOff = chargeOff;
	}

	/**
	 * @return the chargeOffAMN
	 */
	public Long getChargeOffAMN() {
		return chargeOffAMN;
	}

	/**
	 * @param chargeOffAMN the chargeOffAMN to set
	 */
	public void setChargeOffAMN(final Long chargeOffAMN) {
		this.chargeOffAMN = chargeOffAMN;
	}

	/**
	 * @return the transferAcc
	 */
	public String getTransferAcc() {
		return transferAcc;
	}

	/**
	 * @param transferAcc the transferAcc to set
	 */
	public void setTransferAcc(final String transferAcc) {
		this.transferAcc = transferAcc;
	}

	/**
	 * @return the phoneType
	 */
	public String getPhoneType() {
		return phoneType;
	}

	/**
	 * @param phoneType the phoneType to set
	 */
	public void setPhoneType(final String phoneType) {
		this.phoneType = phoneType;
	}

	/**
	 * @return the embossline1
	 */
	public String getEmbossline1() {
		return embossline1;
	}

	/**
	 * @param embossline1 the embossline1 to set
	 */
	public void setEmbossline1(final String embossline1) {
		this.embossline1 = embossline1;
	}

	/**
	 * @return the embossline2
	 */
	public String getEmbossline2() {
		return embossline2;
	}

	/**
	 * @param embossline2 the embossline2 to set
	 */
	public void setEmbossline2(final String embossline2) {
		this.embossline2 = embossline2;
	}

	/**
	 * @return the lCreditLimit
	 */
	public Long getlCreditLimit() {
		return lCreditLimit;
	}

	/**
	 * @param lCreditLimit the lCreditLimit to set
	 */
	public void setlCreditLimit(final Long lCreditLimit) {
		this.lCreditLimit = lCreditLimit;
	}

	/**
	 * @return the maintDate
	 */
	public Long getMaintDate() {
		return maintDate;
	}

	/**
	 * @param maintDate the maintDate to set
	 */
	public void setMaintDate(final Long maintDate) {
		this.maintDate = maintDate;
	}

	/**
	 * @return the optionalField1
	 */
	public String getOptionalField1() {
		return optionalField1;
	}

	/**
	 * @param optionalField1 the optionalField1 to set
	 */
	public void setOptionalField1(final String optionalField1) {
		this.optionalField1 = optionalField1;
	}

	/**
	 * @return the optionalField2
	 */
	public String getOptionalField2() {
		return optionalField2;
	}

	/**
	 * @param optionalField2 the optionalField2 to set
	 */
	public void setOptionalField2(final String optionalField2) {
		this.optionalField2 = optionalField2;
	}

	/**
	 * @return the optionalField3
	 */
	public String getOptionalField3() {
		return optionalField3;
	}

	/**
	 * @param optionalField3 the optionalField3 to set
	 */
	public void setOptionalField3(final String optionalField3) {
		this.optionalField3 = optionalField3;
	}

	/**
	 * @return the optionalField4
	 */
	public String getOptionalField4() {
		return optionalField4;
	}

	/**
	 * @param optionalField4 the optionalField4 to set
	 */
	public void setOptionalField4(final String optionalField4) {
		this.optionalField4 = optionalField4;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CardAccount [codCarga=");
		builder.append(codCarga);
		builder.append(", codCarHolder=");
		builder.append(codCarHolder);
		builder.append(", accountNum=");
		builder.append(accountNum);
		builder.append(", codCompId=");
		builder.append(codCompId);
		builder.append(", hierarchyNode=");
		builder.append(hierarchyNode);
		builder.append(", effectiveDate=");
		builder.append(effectiveDate);
		builder.append(", accountOpen=");
		builder.append(accountOpen);
		builder.append(", accountClose=");
		builder.append(accountClose);
		builder.append(", accountExpired=");
		builder.append(accountExpired);
		builder.append(", cardType=");
		builder.append(cardType);
		builder.append(", spendingLimit=");
		builder.append(spendingLimit);
		builder.append(", statementType=");
		builder.append(statementType);
		builder.append(", lastRevision=");
		builder.append(lastRevision);
		builder.append(", transLimit=");
		builder.append(transLimit);
		builder.append(", corporatePay=");
		builder.append(corporatePay);
		builder.append(", billingAccount=");
		builder.append(billingAccount);
		builder.append(", costCenter=");
		builder.append(costCenter);
		builder.append(", subAccount=");
		builder.append(subAccount);
		builder.append(", dailyLimit=");
		builder.append(dailyLimit);
		builder.append(", cycleLimit=");
		builder.append(cycleLimit);
		builder.append(", cashLimit=");
		builder.append(cashLimit);
		builder.append(", statusCode=");
		builder.append(statusCode);
		builder.append(", reasonStatus=");
		builder.append(reasonStatus);
		builder.append(", statusDate=");
		builder.append(statusDate);
		builder.append(", codPrFoundedInd=");
		builder.append(codPrFoundedInd);
		builder.append(", codCityPairProg=");
		builder.append(codCityPairProg);
		builder.append(", taskOrder=");
		builder.append(taskOrder);
		builder.append(", codFleetService=");
		builder.append(codFleetService);
		builder.append(", codCreditRating=");
		builder.append(codCreditRating);
		builder.append(", ratingDate=");
		builder.append(ratingDate);
		builder.append(", codAnnualFee=");
		builder.append(codAnnualFee);
		builder.append(", codMonthFee=");
		builder.append(codMonthFee);
		builder.append(", codCardVerif=");
		builder.append(codCardVerif);
		builder.append(", checkIndicator=");
		builder.append(checkIndicator);
		builder.append(", codAccountType=");
		builder.append(codAccountType);
		builder.append(", LostStolen=");
		builder.append(LostStolen);
		builder.append(", chargeOff=");
		builder.append(chargeOff);
		builder.append(", chargeOffAMN=");
		builder.append(chargeOffAMN);
		builder.append(", transferAcc=");
		builder.append(transferAcc);
		builder.append(", phoneType=");
		builder.append(phoneType);
		builder.append(", embossline1=");
		builder.append(embossline1);
		builder.append(", embossline2=");
		builder.append(embossline2);
		builder.append(", lCreditLimit=");
		builder.append(lCreditLimit);
		builder.append(", maintDate=");
		builder.append(maintDate);
		builder.append(", optionalField1=");
		builder.append(optionalField1);
		builder.append(", optionalField2=");
		builder.append(optionalField2);
		builder.append(", optionalField3=");
		builder.append(optionalField3);
		builder.append(", optionalField4=");
		builder.append(optionalField4);
		builder.append("]");
		return builder.toString();
	}

	
}
