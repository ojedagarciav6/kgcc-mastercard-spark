package com.bbva.kgcc.visa.entities;

/**
 * The Class AccountBalance.
 */
public class AccountBalance {

	/** Attribute COD_CARD_TP. */
	private Integer codCardTp;

	/** Attribute COD_LOAD_TR. */
	private String codCarga;

	/** Attribute COD_ACC_NUM. */
	private String codAccountNum;

	/** Attribute COD_COMP_ID // BORRAR. */
	private Long codCompId;

	/** Attribute QNU_CLOS_DAT. */
	private Long closingDate;

	/** Attribute COD_PERIOD_N. */
	private Long period;

	/** Attribute IMP_PREV_BAL. */
	private Long prevBalance;

	/** Attribute IMP_CURR_BAL. */
	private Long currBalance;

	/** Attribute QNU_CRED_LIM. */
	private Long creditLimit;

	/** Attribute IMP_AMT_DUE. */
	private Long amountDue;

	/** Attribute QNU_PAS_DUE. */
	private Long pastDueCount;

	/** Attribute IMP_PST_DUE. */
	private Long pastDueAmount;

	/** Attribute IMP_DSPT_AMT. */
	private Long disputedAmount;

	/** Attribute COD_BILL_CUR. */
	private Long billingCurrency;

	/** Attribute IMP_AMT_PAST. */
	private Long amountPastD;

	/** Attribute IMP_AMT_CYC2. */
	private Long amountCycle2;

	/** Attribute IMP_AMT_CYC3. */
	private Long amountCycle3;

	/** Attribute IMP_AMT_CYC4. */
	private Long amountCycle4;

	/** Attribute IMP_AMT_CYC5. */
	private Long amountCycle5;

	/** Attribute IMP_AMT_CYC6. */
	private Long amountCycle6;

	/** Attribute IMP_AMT_CYCP. */
	private Long amountCycleP;

	/** Attribute QNU_BLL_CYC1. */
	private Long billingCycle1;

	/** Attribute QNU_BLL_CYC2. */
	private Long billingCycle2;

	/** Attribute QNU_BLL_CYC3. */
	private Long billingCycle3;

	/** Attribute QNU_BLL_CYC4. */
	private Long billingCycle4;

	/** Attribute QNU_BLL_CYC5. */
	private Long billingCycle5;

	/** Attribute QNU_BLL_CYC6. */
	private Long billingCycle6;

	/** Attribute QNU_BLL_CYCP. */
	private Long billingCycleP;

	/** Attribute QNU_BLL_CYCC. */
	private Long billingCycleC;

	/** Attribute IMP_LST_PAYA. */
	private Long lasPaymentAmount;

	/** Attribute QNU_LST_PAYD. */
	private Long lastPaymentDate;

	/** Attribute QNU_PAY_DUE. */
	private Long paymentDueDate;

	/** Attribute IMP_HIGH_BAL. */
	private Long higBalance;

	/** Attribute DES_OPT1. */
	private String optional1;

	/** Attribute DES_OPT2. */
	private String optional2;

	/** Attribute DES_OPT3. */
	private String optional3;

	/** Attribute DES_OPT4. */
	private String optional4;

	/** Attribute QRA_ANN_PERC. */
	private Long anualPerc;

	/** Attribute IMP_PURC_BAL. */
	private Long purchaseBalance;

	/** Attribute QRA_RATE_BAL. */
	private Long rateBalance;

	/** Attribute IMP_BAL_TRNS. */
	private Long balanceTransfer;

	/** Attribute QRA_RAT_CASH. */
	private Long rateCashAdv;

	/** Attribute IMP_CSH_ADV. */
	private Long cashAdvSubj;

	/** Attribute QNU_TOT_REWA. */
	private Long rewardsPoints;

	/** Attribute QNU_REWAR_PT. */
	private Long totalRewards;

	/** Attribute QNU_BONUS_PT. */
	private Long bonusPoints;

	/** Attribute IMP_TOT_CHRG. */
	private Long totalCharged;

	public AccountBalance(final Integer codCardTp, final String codCarga,
			final String codAccountNum, final Long codCompId, final Long closingDate,
			final Long period, final Long prevBalance, final Long currBalance, final Long creditLimit,
			final Long amountDue, final Long pastDueCount, final Long pastDueAmount,
			final Long disputedAmount, final Long billingCurrency, final Long amountPastD,
			final Long amountCycle2, final Long amountCycle3, final Long amountCycle4,
			final Long amountCycle5, final Long amountCycle6, final Long amountCycleP,
			final Long billingCycle1, final Long billingCycle2, final Long billingCycle3,
			final Long billingCycle4, final Long billingCycle5, final Long billingCycle6,
			final Long billingCycleP, final Long billingCycleC, final Long lasPaymentAmount,
			final Long lastPaymentDate, final Long paymentDueDate, final Long higBalance,
			final String optional1, final String optional2, final String optional3,
			final String optional4, final Long anualPerc, final Long purchaseBalance,
			final Long rateBalance, final Long balanceTransfer, final Long rateCashAdv,
			final Long cashAdvSubj, final Long rewardsPoints, final Long totalRewards,
			final Long bonusPoints, final Long totalCharged) {
		super();
		this.codCardTp = codCardTp;
		this.codCarga = codCarga;
		this.codAccountNum = codAccountNum;
		this.codCompId = codCompId;
		this.closingDate = closingDate;
		this.period = period;
		this.prevBalance = prevBalance;
		this.currBalance = currBalance;
		this.creditLimit = creditLimit;
		this.amountDue = amountDue;
		this.pastDueCount = pastDueCount;
		this.pastDueAmount = pastDueAmount;
		this.disputedAmount = disputedAmount;
		this.billingCurrency = billingCurrency;
		this.amountPastD = amountPastD;
		this.amountCycle2 = amountCycle2;
		this.amountCycle3 = amountCycle3;
		this.amountCycle4 = amountCycle4;
		this.amountCycle5 = amountCycle5;
		this.amountCycle6 = amountCycle6;
		this.amountCycleP = amountCycleP;
		this.billingCycle1 = billingCycle1;
		this.billingCycle2 = billingCycle2;
		this.billingCycle3 = billingCycle3;
		this.billingCycle4 = billingCycle4;
		this.billingCycle5 = billingCycle5;
		this.billingCycle6 = billingCycle6;
		this.billingCycleP = billingCycleP;
		this.billingCycleC = billingCycleC;
		this.lasPaymentAmount = lasPaymentAmount;
		this.lastPaymentDate = lastPaymentDate;
		this.paymentDueDate = paymentDueDate;
		this.higBalance = higBalance;
		this.optional1 = optional1;
		this.optional2 = optional2;
		this.optional3 = optional3;
		this.optional4 = optional4;
		this.anualPerc = anualPerc;
		this.purchaseBalance = purchaseBalance;
		this.rateBalance = rateBalance;
		this.balanceTransfer = balanceTransfer;
		this.rateCashAdv = rateCashAdv;
		this.cashAdvSubj = cashAdvSubj;
		this.rewardsPoints = rewardsPoints;
		this.totalRewards = totalRewards;
		this.bonusPoints = bonusPoints;
		this.totalCharged = totalCharged;
	}

	/**
	 * @return the codCardTp
	 */
	public Integer getCodCardTp() {
		return codCardTp;
	}

	/**
	 * @param codCardTp the codCardTp to set
	 */
	public void setCodCardTp(final Integer codCardTp) {
		this.codCardTp = codCardTp;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the codAccountNum
	 */
	public String getCodAccountNum() {
		return codAccountNum;
	}

	/**
	 * @param codAccountNum the codAccountNum to set
	 */
	public void setCodAccountNum(final String codAccountNum) {
		this.codAccountNum = codAccountNum;
	}

	/**
	 * @return the codCompId
	 */
	public Long getCodCompId() {
		return codCompId;
	}

	/**
	 * @param codCompId the codCompId to set
	 */
	public void setCodCompId(final Long codCompId) {
		this.codCompId = codCompId;
	}

	/**
	 * @return the closingDate
	 */
	public Long getClosingDate() {
		return closingDate;
	}

	/**
	 * @param closingDate the closingDate to set
	 */
	public void setClosingDate(final Long closingDate) {
		this.closingDate = closingDate;
	}

	/**
	 * @return the period
	 */
	public Long getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(final Long period) {
		this.period = period;
	}

	/**
	 * @return the prevBalance
	 */
	public Long getPrevBalance() {
		return prevBalance;
	}

	/**
	 * @param prevBalance the prevBalance to set
	 */
	public void setPrevBalance(final Long prevBalance) {
		this.prevBalance = prevBalance;
	}

	/**
	 * @return the currBalance
	 */
	public Long getCurrBalance() {
		return currBalance;
	}

	/**
	 * @param currBalance the currBalance to set
	 */
	public void setCurrBalance(final Long currBalance) {
		this.currBalance = currBalance;
	}

	/**
	 * @return the creditLimit
	 */
	public Long getCreditLimit() {
		return creditLimit;
	}

	/**
	 * @param creditLimit the creditLimit to set
	 */
	public void setCreditLimit(final Long creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * @return the amountDue
	 */
	public Long getAmountDue() {
		return amountDue;
	}

	/**
	 * @param amountDue the amountDue to set
	 */
	public void setAmountDue(final Long amountDue) {
		this.amountDue = amountDue;
	}

	/**
	 * @return the pastDueCount
	 */
	public Long getPastDueCount() {
		return pastDueCount;
	}

	/**
	 * @param pastDueCount the pastDueCount to set
	 */
	public void setPastDueCount(final Long pastDueCount) {
		this.pastDueCount = pastDueCount;
	}

	/**
	 * @return the pastDueAmount
	 */
	public Long getPastDueAmount() {
		return pastDueAmount;
	}

	/**
	 * @param pastDueAmount the pastDueAmount to set
	 */
	public void setPastDueAmount(final Long pastDueAmount) {
		this.pastDueAmount = pastDueAmount;
	}

	/**
	 * @return the disputedAmount
	 */
	public Long getDisputedAmount() {
		return disputedAmount;
	}

	/**
	 * @param disputedAmount the disputedAmount to set
	 */
	public void setDisputedAmount(final Long disputedAmount) {
		this.disputedAmount = disputedAmount;
	}

	/**
	 * @return the billingCurrency
	 */
	public Long getBillingCurrency() {
		return billingCurrency;
	}

	/**
	 * @param billingCurrency the billingCurrency to set
	 */
	public void setBillingCurrency(final Long billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	/**
	 * @return the amountPastD
	 */
	public Long getAmountPastD() {
		return amountPastD;
	}

	/**
	 * @param amountPastD the amountPastD to set
	 */
	public void setAmountPastD(final Long amountPastD) {
		this.amountPastD = amountPastD;
	}

	/**
	 * @return the amountCycle2
	 */
	public Long getAmountCycle2() {
		return amountCycle2;
	}

	/**
	 * @param amountCycle2 the amountCycle2 to set
	 */
	public void setAmountCycle2(final Long amountCycle2) {
		this.amountCycle2 = amountCycle2;
	}

	/**
	 * @return the amountCycle3
	 */
	public Long getAmountCycle3() {
		return amountCycle3;
	}

	/**
	 * @param amountCycle3 the amountCycle3 to set
	 */
	public void setAmountCycle3(final Long amountCycle3) {
		this.amountCycle3 = amountCycle3;
	}

	/**
	 * @return the amountCycle4
	 */
	public Long getAmountCycle4() {
		return amountCycle4;
	}

	/**
	 * @param amountCycle4 the amountCycle4 to set
	 */
	public void setAmountCycle4(final Long amountCycle4) {
		this.amountCycle4 = amountCycle4;
	}

	/**
	 * @return the amountCycle5
	 */
	public Long getAmountCycle5() {
		return amountCycle5;
	}

	/**
	 * @param amountCycle5 the amountCycle5 to set
	 */
	public void setAmountCycle5(final Long amountCycle5) {
		this.amountCycle5 = amountCycle5;
	}

	/**
	 * @return the amountCycle6
	 */
	public Long getAmountCycle6() {
		return amountCycle6;
	}

	/**
	 * @param amountCycle6 the amountCycle6 to set
	 */
	public void setAmountCycle6(final Long amountCycle6) {
		this.amountCycle6 = amountCycle6;
	}

	/**
	 * @return the amountCycleP
	 */
	public Long getAmountCycleP() {
		return amountCycleP;
	}

	/**
	 * @param amountCycleP the amountCycleP to set
	 */
	public void setAmountCycleP(final Long amountCycleP) {
		this.amountCycleP = amountCycleP;
	}

	/**
	 * @return the billingCycle1
	 */
	public Long getBillingCycle1() {
		return billingCycle1;
	}

	/**
	 * @param billingCycle1 the billingCycle1 to set
	 */
	public void setBillingCycle1(final Long billingCycle1) {
		this.billingCycle1 = billingCycle1;
	}

	/**
	 * @return the billingCycle2
	 */
	public Long getBillingCycle2() {
		return billingCycle2;
	}

	/**
	 * @param billingCycle2 the billingCycle2 to set
	 */
	public void setBillingCycle2(final Long billingCycle2) {
		this.billingCycle2 = billingCycle2;
	}

	/**
	 * @return the billingCycle3
	 */
	public Long getBillingCycle3() {
		return billingCycle3;
	}

	/**
	 * @param billingCycle3 the billingCycle3 to set
	 */
	public void setBillingCycle3(final Long billingCycle3) {
		this.billingCycle3 = billingCycle3;
	}

	/**
	 * @return the billingCycle4
	 */
	public Long getBillingCycle4() {
		return billingCycle4;
	}

	/**
	 * @param billingCycle4 the billingCycle4 to set
	 */
	public void setBillingCycle4(final Long billingCycle4) {
		this.billingCycle4 = billingCycle4;
	}

	/**
	 * @return the billingCycle5
	 */
	public Long getBillingCycle5() {
		return billingCycle5;
	}

	/**
	 * @param billingCycle5 the billingCycle5 to set
	 */
	public void setBillingCycle5(final Long billingCycle5) {
		this.billingCycle5 = billingCycle5;
	}

	/**
	 * @return the billingCycle6
	 */
	public Long getBillingCycle6() {
		return billingCycle6;
	}

	/**
	 * @param billingCycle6 the billingCycle6 to set
	 */
	public void setBillingCycle6(final Long billingCycle6) {
		this.billingCycle6 = billingCycle6;
	}

	/**
	 * @return the billingCycleP
	 */
	public Long getBillingCycleP() {
		return billingCycleP;
	}

	/**
	 * @param billingCycleP the billingCycleP to set
	 */
	public void setBillingCycleP(final Long billingCycleP) {
		this.billingCycleP = billingCycleP;
	}

	/**
	 * @return the billingCycleC
	 */
	public Long getBillingCycleC() {
		return billingCycleC;
	}

	/**
	 * @param billingCycleC the billingCycleC to set
	 */
	public void setBillingCycleC(final Long billingCycleC) {
		this.billingCycleC = billingCycleC;
	}

	/**
	 * @return the lasPaymentAmount
	 */
	public Long getLasPaymentAmount() {
		return lasPaymentAmount;
	}

	/**
	 * @param lasPaymentAmount the lasPaymentAmount to set
	 */
	public void setLasPaymentAmount(final Long lasPaymentAmount) {
		this.lasPaymentAmount = lasPaymentAmount;
	}

	/**
	 * @return the lastPaymentDate
	 */
	public Long getLastPaymentDate() {
		return lastPaymentDate;
	}

	/**
	 * @param lastPaymentDate the lastPaymentDate to set
	 */
	public void setLastPaymentDate(final Long lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	/**
	 * @return the paymentDueDate
	 */
	public Long getPaymentDueDate() {
		return paymentDueDate;
	}

	/**
	 * @param paymentDueDate the paymentDueDate to set
	 */
	public void setPaymentDueDate(final Long paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	/**
	 * @return the higBalance
	 */
	public Long getHigBalance() {
		return higBalance;
	}

	/**
	 * @param higBalance the higBalance to set
	 */
	public void setHigBalance(final Long higBalance) {
		this.higBalance = higBalance;
	}

	/**
	 * @return the optional1
	 */
	public String getOptional1() {
		return optional1;
	}

	/**
	 * @param optional1 the optional1 to set
	 */
	public void setOptional1(final String optional1) {
		this.optional1 = optional1;
	}

	/**
	 * @return the optional2
	 */
	public String getOptional2() {
		return optional2;
	}

	/**
	 * @param optional2 the optional2 to set
	 */
	public void setOptional2(final String optional2) {
		this.optional2 = optional2;
	}

	/**
	 * @return the optional3
	 */
	public String getOptional3() {
		return optional3;
	}

	/**
	 * @param optional3 the optional3 to set
	 */
	public void setOptional3(final String optional3) {
		this.optional3 = optional3;
	}

	/**
	 * @return the optional4
	 */
	public String getOptional4() {
		return optional4;
	}

	/**
	 * @param optional4 the optional4 to set
	 */
	public void setOptional4(final String optional4) {
		this.optional4 = optional4;
	}

	/**
	 * @return the anualPerc
	 */
	public Long getAnualPerc() {
		return anualPerc;
	}

	/**
	 * @param anualPerc the anualPerc to set
	 */
	public void setAnualPerc(final Long anualPerc) {
		this.anualPerc = anualPerc;
	}

	/**
	 * @return the purchaseBalance
	 */
	public Long getPurchaseBalance() {
		return purchaseBalance;
	}

	/**
	 * @param purchaseBalance the purchaseBalance to set
	 */
	public void setPurchaseBalance(final Long purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	/**
	 * @return the rateBalance
	 */
	public Long getRateBalance() {
		return rateBalance;
	}

	/**
	 * @param rateBalance the rateBalance to set
	 */
	public void setRateBalance(final Long rateBalance) {
		this.rateBalance = rateBalance;
	}

	/**
	 * @return the balanceTransfer
	 */
	public Long getBalanceTransfer() {
		return balanceTransfer;
	}

	/**
	 * @param balanceTransfer the balanceTransfer to set
	 */
	public void setBalanceTransfer(final Long balanceTransfer) {
		this.balanceTransfer = balanceTransfer;
	}

	/**
	 * @return the rateCashAdv
	 */
	public Long getRateCashAdv() {
		return rateCashAdv;
	}

	/**
	 * @param rateCashAdv the rateCashAdv to set
	 */
	public void setRateCashAdv(final Long rateCashAdv) {
		this.rateCashAdv = rateCashAdv;
	}

	/**
	 * @return the cashAdvSubj
	 */
	public Long getCashAdvSubj() {
		return cashAdvSubj;
	}

	/**
	 * @param cashAdvSubj the cashAdvSubj to set
	 */
	public void setCashAdvSubj(final Long cashAdvSubj) {
		this.cashAdvSubj = cashAdvSubj;
	}

	/**
	 * @return the rewardsPoints
	 */
	public Long getRewardsPoints() {
		return rewardsPoints;
	}

	/**
	 * @param rewardsPoints the rewardsPoints to set
	 */
	public void setRewardsPoints(final Long rewardsPoints) {
		this.rewardsPoints = rewardsPoints;
	}

	/**
	 * @return the totalRewards
	 */
	public Long getTotalRewards() {
		return totalRewards;
	}

	/**
	 * @param totalRewards the totalRewards to set
	 */
	public void setTotalRewards(final Long totalRewards) {
		this.totalRewards = totalRewards;
	}

	/**
	 * @return the bonusPoints
	 */
	public Long getBonusPoints() {
		return bonusPoints;
	}

	/**
	 * @param bonusPoints the bonusPoints to set
	 */
	public void setBonusPoints(final Long bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	/**
	 * @return the totalCharged
	 */
	public Long getTotalCharged() {
		return totalCharged;
	}

	/**
	 * @param totalCharged the totalCharged to set
	 */
	public void setTotalCharged(final Long totalCharged) {
		this.totalCharged = totalCharged;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountBalance [codCardTp=");
		builder.append(codCardTp);
		builder.append(", codCarga=");
		builder.append(codCarga);
		builder.append(", codAccountNum=");
		builder.append(codAccountNum);
		builder.append(", codCompId=");
		builder.append(codCompId);
		builder.append(", closingDate=");
		builder.append(closingDate);
		builder.append(", period=");
		builder.append(period);
		builder.append(", prevBalance=");
		builder.append(prevBalance);
		builder.append(", currBalance=");
		builder.append(currBalance);
		builder.append(", creditLimit=");
		builder.append(creditLimit);
		builder.append(", amountDue=");
		builder.append(amountDue);
		builder.append(", pastDueCount=");
		builder.append(pastDueCount);
		builder.append(", pastDueAmount=");
		builder.append(pastDueAmount);
		builder.append(", disputedAmount=");
		builder.append(disputedAmount);
		builder.append(", billingCurrency=");
		builder.append(billingCurrency);
		builder.append(", amountPastD=");
		builder.append(amountPastD);
		builder.append(", amountCycle2=");
		builder.append(amountCycle2);
		builder.append(", amountCycle3=");
		builder.append(amountCycle3);
		builder.append(", amountCycle4=");
		builder.append(amountCycle4);
		builder.append(", amountCycle5=");
		builder.append(amountCycle5);
		builder.append(", amountCycle6=");
		builder.append(amountCycle6);
		builder.append(", amountCycleP=");
		builder.append(amountCycleP);
		builder.append(", billingCycle1=");
		builder.append(billingCycle1);
		builder.append(", billingCycle2=");
		builder.append(billingCycle2);
		builder.append(", billingCycle3=");
		builder.append(billingCycle3);
		builder.append(", billingCycle4=");
		builder.append(billingCycle4);
		builder.append(", billingCycle5=");
		builder.append(billingCycle5);
		builder.append(", billingCycle6=");
		builder.append(billingCycle6);
		builder.append(", billingCycleP=");
		builder.append(billingCycleP);
		builder.append(", billingCycleC=");
		builder.append(billingCycleC);
		builder.append(", lasPaymentAmount=");
		builder.append(lasPaymentAmount);
		builder.append(", lastPaymentDate=");
		builder.append(lastPaymentDate);
		builder.append(", paymentDueDate=");
		builder.append(paymentDueDate);
		builder.append(", higBalance=");
		builder.append(higBalance);
		builder.append(", optional1=");
		builder.append(optional1);
		builder.append(", optional2=");
		builder.append(optional2);
		builder.append(", optional3=");
		builder.append(optional3);
		builder.append(", optional4=");
		builder.append(optional4);
		builder.append(", anualPerc=");
		builder.append(anualPerc);
		builder.append(", purchaseBalance=");
		builder.append(purchaseBalance);
		builder.append(", rateBalance=");
		builder.append(rateBalance);
		builder.append(", balanceTransfer=");
		builder.append(balanceTransfer);
		builder.append(", rateCashAdv=");
		builder.append(rateCashAdv);
		builder.append(", cashAdvSubj=");
		builder.append(cashAdvSubj);
		builder.append(", rewardsPoints=");
		builder.append(rewardsPoints);
		builder.append(", totalRewards=");
		builder.append(totalRewards);
		builder.append(", bonusPoints=");
		builder.append(bonusPoints);
		builder.append(", totalCharged=");
		builder.append(totalCharged);
		builder.append("]");
		return builder.toString();
	}

	
}
