package com.bbva.kgcc.visa.entities;

import java.sql.Timestamp;

/**
 * The Class CardTransaction.
 */
public class CardTransaction {

	/** Attribute COD_LOAD_TR. */
	private String codCarga;

	/** Attribute COD_ACC_NUM. */
	private String codAccountNumber;

	/** Attribute COD_COMP_ID. */
	private Long codCompId;

	/** Attribute QNU_POST_DT. */
	private Long postingDate;

	/** Attribute COD_TRANS_NM. */
	private String codTransaction;

	/** Attribute COD_SEQ_TRAN. */
	private Long codSeqFich;

	/** Attribute COD_PERIOD_N. */
	private Integer codPeriod;

	/** Attribute COD_CARD_TP. */
	private Integer codCardTp;

	/** Attribute COD_ACQU_BIN. */
	private Integer codAcquiringBin;

	/** Attribute COD_CARD_ACC. */
	private String codCardAcceptor;

	/** Attribute DES_SUPP_NM. */
	private String supplierName;

	/** Attribute DES_SUPP_CTY. */
	private String supplierCity;

	/** Attribute COD_SUPP_STA. */
	private String supplierState;

	/** Attribute COD_ISO_CTRY. */
	private String codIsoCountry;

	/** Attribute COD_SUPP_PT. */
	private String codSupplrPostal;

	/** Attribute IMP_SOUR_AMT. */
	private Long sourceAmount;

	/** Attribute IMP_BILL_AMT. */
	private Long billingAmount;

	/** Attribute COD_SOU_CURR. */
	private Integer codSourceCurrency;

	/** Attribute COD_MERC_CAT. */
	private Long merchantCat;

	/** Attribute COD_TRN_TYPE. */
	private String codTransType;

	/** Attribute QNU_TRN_DT. */
	private Long transDate;

	/** Attribute COD_BILL_CUR. */
	private Integer codBillingCurrency;

	/** Attribute IMP_TAX_AMT. */
	private Long taxAmount;

	/** Attribute IMP_DISP_AMT. */
	private Long disputeAmount;

	/** Attribute DES_DREASON. */
	private String disputeRea;

	/** Attribute QNU_DISDT. */
	private Long disputeDate;

	/** Attribute COD_COMMODIT. */
	private String codCommodity;

	/** Attribute COD_SUPP_VAT. */
	private String supplierVat;

	/** Attribute DES_ORDER_NM. */
	private String orderNum;

	/** Attribute DES_CUST_VAT. */
	private String customerVat;

	/** Attribute IMP_VAT_AMT. */
	private Long VATAmount;

	/** Attribute IMP_TAX2_AMT. */
	private Long taxAmount2;

	/** Attribute COD_PUR_FORM. */
	private String purchaseFormt;

	/** Attribute COD_CUSTOMR. */
	private String codeCustomer;

	/** Attribute COD_PURCH_ID. */
	private String purchaseId;

	/** Attribute QNU_TRN_TIME. */
	private Long transTime;

	/** Attribute COD_TAX_AMT. */
	private Integer codTaxAmount;

	/** Attribute COD_TAX_AMT2. */
	private Integer codTaxAmount2;

	/** Attribute COD_ORD_TYPE. */
	private Integer codOrderType;

	/** Attribute COD_MSSGE. */
	private String codMessage;

	/** Attribute DES_PROC_ADD. */
	private String processorAdd;

	/** Attribute COD_MER_PROF. */
	private String merchantProf;

	/** Attribute COD_USAGE. */
	private Integer codUsage;

	/** Attribute COD_ENR_TRN. */
	private String enrichedTrans;

	/** Attribute COD_BILL_ACC. */
	private String codBillingAccount;

	/** Attribute QNU_DDA_NUM. */
	private Long ddaNumber;

	/** Attribute QNU_DD_SA_NM. */
	private Long ddaSavingNum;

	/** Attribute COD_DISP_STA. */
	private String codDisputeStatus;

	/** Attribute COD_MATC_IND. */
	private Integer matchedInd;

	/** Attribute QNU_ROUT_NUM. */
	private Long rountungNum;

	/** Attribute COD_AUTHORIZ. */
	private String codAuthorization;

	/** Attribute COD_TRN_APPR. */
	private String codTransApproval;

	/** Attribute COD_EXTRC_ID. */
	private Long codExtract;

	/** Attribute COD_MEMO_PST. */
	private Integer codMemoPost;

	/** Attribute QNU_STTEM_DT. */
	private Long statementDate;

	/** Attribute COD_USR_DAT1. */
	private String codUserData1;

	/** Attribute DES_UDA_DES1. */
	private String userDataDes1;

	/** Attribute COD_USR_DAT2. */
	private String codUserData2;

	/** Attribute DES_UDA_DES2. */
	private String userDataDes2;

	/** Attribute COD_USR_DAT3. */
	private String codUserData3;

	/** Attribute DES_UDA_DES3. */
	private String userDataDes3;

	/** Attribute COD_USR_DAT4. */
	private String codUserData4;

	/** Attribute DES_UDA_DES4. */
	private String userDataDes4;

	/** Attribute COD_USR_DAT5. */
	private String codUserData5;

	/** Attribute DES_UDA_DES5. */
	private String userDataDes5;

	/** Attribute COD_VISA_BAT. */
	private String codVisaBatch;

	/** Attribute QNU_AUTH_DT. */
	private Long authDate;

	/** Attribute COD_LI_IT_ID. */
	private Integer codLineItemId;

	/** Attribute COD_ISS_DEF. */
	private String codIssuerDefined;

	/** Attribute DES_SRCE. */
	private String source;

	/** Attribute DES_OPT_FD. */
	private String optionalField;

	/** Attribute DES_OPT_F2. */
	private String optionalField2;

	/** Attribute DES_OPT_F3. */
	private String optionalField3;

	/** Attribute DES_OPT_F4. */
	private String optionalField4;

	/** Attribute DES_RESEV_F1. */
	private String reservedField;

	/** Attribute DES_RESEV_F2. */
	private String reservedField2;

	/** Attribute DES_RESEV_F3. */
	private String reservedField3;

	/** Attribute DES_RESEV_F4. */
	private String reservedField4;

	/** Attribute COD_TRN_ID. */
	private Long codTransactionId;

	/** Attribute IMP_TIP. */
	private Long tip;

	/** Attribute IMP_FEE_AMT. */
	private Long feeAmount;

	/** Attribute DES_FEE_DESC. */
	private String feeDesc;

	/** Attribute QNU_FORG_EXC. */
	private Long foreingExc;

	/** Attribute QNU_EXCH_RT. */
	private Integer exchangeRate;

	/** Attribute IMP_VOLM_LOC. */
	private Double impVolmLoc;

	/** Attribute IMP_VOLM_USD. */
	private Double impVolmUsd;

	/** Attribute COD_SEQ_ORIG. */
	private Long codSeqOrigen;

	/** Attribute TIM_PRIM_REG. */
	private Timestamp timPrimReg;

	/** The cod service. */
	private Integer codSrvId;

	public CardTransaction(final String codCarga, final String codAccountNumber,
			final Long codCompId, final Long postingDate, final String codTransaction,
			final Long codSeqFich, final Integer codPeriod, final Integer codCardTp,
			final Integer codAcquiringBin, final String codCardAcceptor,
			final String supplierName, final String supplierCity, final String supplierState,
			final String codIsoCountry, final String codSupplrPostal, final Long sourceAmount,
			final Long billingAmount, final Integer codSourceCurrency, final Long merchantCat,
			final String codTransType, final Long transDate, final Integer codBillingCurrency,
			final Long taxAmount, final Long disputeAmount, final String disputeRea,
			final Long disputeDate, final String codCommodity, final String supplierVat,
			final String orderNum, final String customerVat, final Long vATAmount,
			final Long taxAmount2, final String purchaseFormt, final String codeCustomer,
			final String purchaseId, final Long transTime, final Integer codTaxAmount,
			final Integer codTaxAmount2, final Integer codOrderType, final String codMessage,
			final String processorAdd, final String merchantProf, final Integer codUsage,
			final String enrichedTrans, final String codBillingAccount, final Long ddaNumber,
			final Long ddaSavingNum, final String codDisputeStatus, final Integer matchedInd,
			final Long rountungNum, final String codAuthorization, final String codTransApproval,
			final Long codExtract, final Integer codMemoPost, final Long statementDate,
			final String codUserData1, final String userDataDes1, final String codUserData2,
			final String userDataDes2, final String codUserData3, final String userDataDes3,
			final String codUserData4, final String userDataDes4, final String codUserData5,
			final String userDataDes5, final String codVisaBatch, final Long authDate,
			final Integer codLineItemId, final String codIssuerDefined, final String source,
			final String optionalField, final String optionalField2, final String optionalField3,
			final String optionalField4, final String reservedField, final String reservedField2,
			final String reservedField3, final String reservedField4,
			final Long codTransactionId, final Long tip, final Long feeAmount, final String feeDesc,
			final Long foreingExc, final Integer exchangeRate, final Double impVolmLoc,
			final Double impVolmUsd, final Long codSeqOrigen, final Timestamp timPrimReg,
			final Integer codSrvId) {
		super();
		this.codCarga = codCarga;
		this.codAccountNumber = codAccountNumber;
		this.codCompId = codCompId;
		this.postingDate = postingDate;
		this.codTransaction = codTransaction;
		this.codSeqFich = codSeqFich;
		this.codPeriod = codPeriod;
		this.codCardTp = codCardTp;
		this.codAcquiringBin = codAcquiringBin;
		this.codCardAcceptor = codCardAcceptor;
		this.supplierName = supplierName;
		this.supplierCity = supplierCity;
		this.supplierState = supplierState;
		this.codIsoCountry = codIsoCountry;
		this.codSupplrPostal = codSupplrPostal;
		this.sourceAmount = sourceAmount;
		this.billingAmount = billingAmount;
		this.codSourceCurrency = codSourceCurrency;
		this.merchantCat = merchantCat;
		this.codTransType = codTransType;
		this.transDate = transDate;
		this.codBillingCurrency = codBillingCurrency;
		this.taxAmount = taxAmount;
		this.disputeAmount = disputeAmount;
		this.disputeRea = disputeRea;
		this.disputeDate = disputeDate;
		this.codCommodity = codCommodity;
		this.supplierVat = supplierVat;
		this.orderNum = orderNum;
		this.customerVat = customerVat;
		VATAmount = vATAmount;
		this.taxAmount2 = taxAmount2;
		this.purchaseFormt = purchaseFormt;
		this.codeCustomer = codeCustomer;
		this.purchaseId = purchaseId;
		this.transTime = transTime;
		this.codTaxAmount = codTaxAmount;
		this.codTaxAmount2 = codTaxAmount2;
		this.codOrderType = codOrderType;
		this.codMessage = codMessage;
		this.processorAdd = processorAdd;
		this.merchantProf = merchantProf;
		this.codUsage = codUsage;
		this.enrichedTrans = enrichedTrans;
		this.codBillingAccount = codBillingAccount;
		this.ddaNumber = ddaNumber;
		this.ddaSavingNum = ddaSavingNum;
		this.codDisputeStatus = codDisputeStatus;
		this.matchedInd = matchedInd;
		this.rountungNum = rountungNum;
		this.codAuthorization = codAuthorization;
		this.codTransApproval = codTransApproval;
		this.codExtract = codExtract;
		this.codMemoPost = codMemoPost;
		this.statementDate = statementDate;
		this.codUserData1 = codUserData1;
		this.userDataDes1 = userDataDes1;
		this.codUserData2 = codUserData2;
		this.userDataDes2 = userDataDes2;
		this.codUserData3 = codUserData3;
		this.userDataDes3 = userDataDes3;
		this.codUserData4 = codUserData4;
		this.userDataDes4 = userDataDes4;
		this.codUserData5 = codUserData5;
		this.userDataDes5 = userDataDes5;
		this.codVisaBatch = codVisaBatch;
		this.authDate = authDate;
		this.codLineItemId = codLineItemId;
		this.codIssuerDefined = codIssuerDefined;
		this.source = source;
		this.optionalField = optionalField;
		this.optionalField2 = optionalField2;
		this.optionalField3 = optionalField3;
		this.optionalField4 = optionalField4;
		this.reservedField = reservedField;
		this.reservedField2 = reservedField2;
		this.reservedField3 = reservedField3;
		this.reservedField4 = reservedField4;
		this.codTransactionId = codTransactionId;
		this.tip = tip;
		this.feeAmount = feeAmount;
		this.feeDesc = feeDesc;
		this.foreingExc = foreingExc;
		this.exchangeRate = exchangeRate;
		this.impVolmLoc = impVolmLoc;
		this.impVolmUsd = impVolmUsd;
		this.codSeqOrigen = codSeqOrigen;
		this.timPrimReg = timPrimReg;
		this.codSrvId = codSrvId;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the codAccountNumber
	 */
	public String getCodAccountNumber() {
		return codAccountNumber;
	}

	/**
	 * @param codAccountNumber the codAccountNumber to set
	 */
	public void setCodAccountNumber(final String codAccountNumber) {
		this.codAccountNumber = codAccountNumber;
	}

	/**
	 * @return the codCompId
	 */
	public Long getCodCompId() {
		return codCompId;
	}

	/**
	 * @param codCompId the codCompId to set
	 */
	public void setCodCompId(final Long codCompId) {
		this.codCompId = codCompId;
	}

	/**
	 * @return the postingDate
	 */
	public Long getPostingDate() {
		return postingDate;
	}

	/**
	 * @param postingDate the postingDate to set
	 */
	public void setPostingDate(final Long postingDate) {
		this.postingDate = postingDate;
	}

	/**
	 * @return the codTransaction
	 */
	public String getCodTransaction() {
		return codTransaction;
	}

	/**
	 * @param codTransaction the codTransaction to set
	 */
	public void setCodTransaction(final String codTransaction) {
		this.codTransaction = codTransaction;
	}

	/**
	 * @return the codSeqFich
	 */
	public Long getCodSeqFich() {
		return codSeqFich;
	}

	/**
	 * @param codSeqFich the codSeqFich to set
	 */
	public void setCodSeqFich(final Long codSeqFich) {
		this.codSeqFich = codSeqFich;
	}

	/**
	 * @return the codPeriod
	 */
	public Integer getCodPeriod() {
		return codPeriod;
	}

	/**
	 * @param codPeriod the codPeriod to set
	 */
	public void setCodPeriod(final Integer codPeriod) {
		this.codPeriod = codPeriod;
	}

	/**
	 * @return the codCardTp
	 */
	public Integer getCodCardTp() {
		return codCardTp;
	}

	/**
	 * @param codCardTp the codCardTp to set
	 */
	public void setCodCardTp(final Integer codCardTp) {
		this.codCardTp = codCardTp;
	}

	/**
	 * @return the codAcquiringBin
	 */
	public Integer getCodAcquiringBin() {
		return codAcquiringBin;
	}

	/**
	 * @param codAcquiringBin the codAcquiringBin to set
	 */
	public void setCodAcquiringBin(final Integer codAcquiringBin) {
		this.codAcquiringBin = codAcquiringBin;
	}

	/**
	 * @return the codCardAcceptor
	 */
	public String getCodCardAcceptor() {
		return codCardAcceptor;
	}

	/**
	 * @param codCardAcceptor the codCardAcceptor to set
	 */
	public void setCodCardAcceptor(final String codCardAcceptor) {
		this.codCardAcceptor = codCardAcceptor;
	}

	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(final String supplierName) {
		this.supplierName = supplierName;
	}

	/**
	 * @return the supplierCity
	 */
	public String getSupplierCity() {
		return supplierCity;
	}

	/**
	 * @param supplierCity the supplierCity to set
	 */
	public void setSupplierCity(final String supplierCity) {
		this.supplierCity = supplierCity;
	}

	/**
	 * @return the supplierState
	 */
	public String getSupplierState() {
		return supplierState;
	}

	/**
	 * @param supplierState the supplierState to set
	 */
	public void setSupplierState(final String supplierState) {
		this.supplierState = supplierState;
	}

	/**
	 * @return the codIsoCountry
	 */
	public String getCodIsoCountry() {
		return codIsoCountry;
	}

	/**
	 * @param codIsoCountry the codIsoCountry to set
	 */
	public void setCodIsoCountry(final String codIsoCountry) {
		this.codIsoCountry = codIsoCountry;
	}

	/**
	 * @return the codSupplrPostal
	 */
	public String getCodSupplrPostal() {
		return codSupplrPostal;
	}

	/**
	 * @param codSupplrPostal the codSupplrPostal to set
	 */
	public void setCodSupplrPostal(final String codSupplrPostal) {
		this.codSupplrPostal = codSupplrPostal;
	}

	/**
	 * @return the sourceAmount
	 */
	public Long getSourceAmount() {
		return sourceAmount;
	}

	/**
	 * @param sourceAmount the sourceAmount to set
	 */
	public void setSourceAmount(final Long sourceAmount) {
		this.sourceAmount = sourceAmount;
	}

	/**
	 * @return the billingAmount
	 */
	public Long getBillingAmount() {
		return billingAmount;
	}

	/**
	 * @param billingAmount the billingAmount to set
	 */
	public void setBillingAmount(final Long billingAmount) {
		this.billingAmount = billingAmount;
	}

	/**
	 * @return the codSourceCurrency
	 */
	public Integer getCodSourceCurrency() {
		return codSourceCurrency;
	}

	/**
	 * @param codSourceCurrency the codSourceCurrency to set
	 */
	public void setCodSourceCurrency(final Integer codSourceCurrency) {
		this.codSourceCurrency = codSourceCurrency;
	}

	/**
	 * @return the merchantCat
	 */
	public Long getMerchantCat() {
		return merchantCat;
	}

	/**
	 * @param merchantCat the merchantCat to set
	 */
	public void setMerchantCat(final Long merchantCat) {
		this.merchantCat = merchantCat;
	}

	/**
	 * @return the codTransType
	 */
	public String getCodTransType() {
		return codTransType;
	}

	/**
	 * @param codTransType the codTransType to set
	 */
	public void setCodTransType(final String codTransType) {
		this.codTransType = codTransType;
	}

	/**
	 * @return the transDate
	 */
	public Long getTransDate() {
		return transDate;
	}

	/**
	 * @param transDate the transDate to set
	 */
	public void setTransDate(final Long transDate) {
		this.transDate = transDate;
	}

	/**
	 * @return the codBillingCurrency
	 */
	public Integer getCodBillingCurrency() {
		return codBillingCurrency;
	}

	/**
	 * @param codBillingCurrency the codBillingCurrency to set
	 */
	public void setCodBillingCurrency(final Integer codBillingCurrency) {
		this.codBillingCurrency = codBillingCurrency;
	}

	/**
	 * @return the taxAmount
	 */
	public Long getTaxAmount() {
		return taxAmount;
	}

	/**
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(final Long taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the disputeAmount
	 */
	public Long getDisputeAmount() {
		return disputeAmount;
	}

	/**
	 * @param disputeAmount the disputeAmount to set
	 */
	public void setDisputeAmount(final Long disputeAmount) {
		this.disputeAmount = disputeAmount;
	}

	/**
	 * @return the disputeRea
	 */
	public String getDisputeRea() {
		return disputeRea;
	}

	/**
	 * @param disputeRea the disputeRea to set
	 */
	public void setDisputeRea(final String disputeRea) {
		this.disputeRea = disputeRea;
	}

	/**
	 * @return the disputeDate
	 */
	public Long getDisputeDate() {
		return disputeDate;
	}

	/**
	 * @param disputeDate the disputeDate to set
	 */
	public void setDisputeDate(final Long disputeDate) {
		this.disputeDate = disputeDate;
	}

	/**
	 * @return the codCommodity
	 */
	public String getCodCommodity() {
		return codCommodity;
	}

	/**
	 * @param codCommodity the codCommodity to set
	 */
	public void setCodCommodity(final String codCommodity) {
		this.codCommodity = codCommodity;
	}

	/**
	 * @return the supplierVat
	 */
	public String getSupplierVat() {
		return supplierVat;
	}

	/**
	 * @param supplierVat the supplierVat to set
	 */
	public void setSupplierVat(final String supplierVat) {
		this.supplierVat = supplierVat;
	}

	/**
	 * @return the orderNum
	 */
	public String getOrderNum() {
		return orderNum;
	}

	/**
	 * @param orderNum the orderNum to set
	 */
	public void setOrderNum(final String orderNum) {
		this.orderNum = orderNum;
	}

	/**
	 * @return the customerVat
	 */
	public String getCustomerVat() {
		return customerVat;
	}

	/**
	 * @param customerVat the customerVat to set
	 */
	public void setCustomerVat(final String customerVat) {
		this.customerVat = customerVat;
	}

	/**
	 * @return the vATAmount
	 */
	public Long getVATAmount() {
		return VATAmount;
	}

	/**
	 * @param vATAmount the vATAmount to set
	 */
	public void setVATAmount(final Long vATAmount) {
		VATAmount = vATAmount;
	}

	/**
	 * @return the taxAmount2
	 */
	public Long getTaxAmount2() {
		return taxAmount2;
	}

	/**
	 * @param taxAmount2 the taxAmount2 to set
	 */
	public void setTaxAmount2(final Long taxAmount2) {
		this.taxAmount2 = taxAmount2;
	}

	/**
	 * @return the purchaseFormt
	 */
	public String getPurchaseFormt() {
		return purchaseFormt;
	}

	/**
	 * @param purchaseFormt the purchaseFormt to set
	 */
	public void setPurchaseFormt(final String purchaseFormt) {
		this.purchaseFormt = purchaseFormt;
	}

	/**
	 * @return the codeCustomer
	 */
	public String getCodeCustomer() {
		return codeCustomer;
	}

	/**
	 * @param codeCustomer the codeCustomer to set
	 */
	public void setCodeCustomer(final String codeCustomer) {
		this.codeCustomer = codeCustomer;
	}

	/**
	 * @return the purchaseId
	 */
	public String getPurchaseId() {
		return purchaseId;
	}

	/**
	 * @param purchaseId the purchaseId to set
	 */
	public void setPurchaseId(final String purchaseId) {
		this.purchaseId = purchaseId;
	}

	/**
	 * @return the transTime
	 */
	public Long getTransTime() {
		return transTime;
	}

	/**
	 * @param transTime the transTime to set
	 */
	public void setTransTime(final Long transTime) {
		this.transTime = transTime;
	}

	/**
	 * @return the codTaxAmount
	 */
	public Integer getCodTaxAmount() {
		return codTaxAmount;
	}

	/**
	 * @param codTaxAmount the codTaxAmount to set
	 */
	public void setCodTaxAmount(final Integer codTaxAmount) {
		this.codTaxAmount = codTaxAmount;
	}

	/**
	 * @return the codTaxAmount2
	 */
	public Integer getCodTaxAmount2() {
		return codTaxAmount2;
	}

	/**
	 * @param codTaxAmount2 the codTaxAmount2 to set
	 */
	public void setCodTaxAmount2(final Integer codTaxAmount2) {
		this.codTaxAmount2 = codTaxAmount2;
	}

	/**
	 * @return the codOrderType
	 */
	public Integer getCodOrderType() {
		return codOrderType;
	}

	/**
	 * @param codOrderType the codOrderType to set
	 */
	public void setCodOrderType(final Integer codOrderType) {
		this.codOrderType = codOrderType;
	}

	/**
	 * @return the codMessage
	 */
	public String getCodMessage() {
		return codMessage;
	}

	/**
	 * @param codMessage the codMessage to set
	 */
	public void setCodMessage(final String codMessage) {
		this.codMessage = codMessage;
	}

	/**
	 * @return the processorAdd
	 */
	public String getProcessorAdd() {
		return processorAdd;
	}

	/**
	 * @param processorAdd the processorAdd to set
	 */
	public void setProcessorAdd(final String processorAdd) {
		this.processorAdd = processorAdd;
	}

	/**
	 * @return the merchantProf
	 */
	public String getMerchantProf() {
		return merchantProf;
	}

	/**
	 * @param merchantProf the merchantProf to set
	 */
	public void setMerchantProf(final String merchantProf) {
		this.merchantProf = merchantProf;
	}

	/**
	 * @return the codUsage
	 */
	public Integer getCodUsage() {
		return codUsage;
	}

	/**
	 * @param codUsage the codUsage to set
	 */
	public void setCodUsage(final Integer codUsage) {
		this.codUsage = codUsage;
	}

	/**
	 * @return the enrichedTrans
	 */
	public String getEnrichedTrans() {
		return enrichedTrans;
	}

	/**
	 * @param enrichedTrans the enrichedTrans to set
	 */
	public void setEnrichedTrans(final String enrichedTrans) {
		this.enrichedTrans = enrichedTrans;
	}

	/**
	 * @return the codBillingAccount
	 */
	public String getCodBillingAccount() {
		return codBillingAccount;
	}

	/**
	 * @param codBillingAccount the codBillingAccount to set
	 */
	public void setCodBillingAccount(final String codBillingAccount) {
		this.codBillingAccount = codBillingAccount;
	}

	/**
	 * @return the ddaNumber
	 */
	public Long getDdaNumber() {
		return ddaNumber;
	}

	/**
	 * @param ddaNumber the ddaNumber to set
	 */
	public void setDdaNumber(final Long ddaNumber) {
		this.ddaNumber = ddaNumber;
	}

	/**
	 * @return the ddaSavingNum
	 */
	public Long getDdaSavingNum() {
		return ddaSavingNum;
	}

	/**
	 * @param ddaSavingNum the ddaSavingNum to set
	 */
	public void setDdaSavingNum(final Long ddaSavingNum) {
		this.ddaSavingNum = ddaSavingNum;
	}

	/**
	 * @return the codDisputeStatus
	 */
	public String getCodDisputeStatus() {
		return codDisputeStatus;
	}

	/**
	 * @param codDisputeStatus the codDisputeStatus to set
	 */
	public void setCodDisputeStatus(final String codDisputeStatus) {
		this.codDisputeStatus = codDisputeStatus;
	}

	/**
	 * @return the matchedInd
	 */
	public Integer getMatchedInd() {
		return matchedInd;
	}

	/**
	 * @param matchedInd the matchedInd to set
	 */
	public void setMatchedInd(final Integer matchedInd) {
		this.matchedInd = matchedInd;
	}

	/**
	 * @return the rountungNum
	 */
	public Long getRountungNum() {
		return rountungNum;
	}

	/**
	 * @param rountungNum the rountungNum to set
	 */
	public void setRountungNum(final Long rountungNum) {
		this.rountungNum = rountungNum;
	}

	/**
	 * @return the codAuthorization
	 */
	public String getCodAuthorization() {
		return codAuthorization;
	}

	/**
	 * @param codAuthorization the codAuthorization to set
	 */
	public void setCodAuthorization(final String codAuthorization) {
		this.codAuthorization = codAuthorization;
	}

	/**
	 * @return the codTransApproval
	 */
	public String getCodTransApproval() {
		return codTransApproval;
	}

	/**
	 * @param codTransApproval the codTransApproval to set
	 */
	public void setCodTransApproval(final String codTransApproval) {
		this.codTransApproval = codTransApproval;
	}

	/**
	 * @return the codExtract
	 */
	public Long getCodExtract() {
		return codExtract;
	}

	/**
	 * @param codExtract the codExtract to set
	 */
	public void setCodExtract(final Long codExtract) {
		this.codExtract = codExtract;
	}

	/**
	 * @return the codMemoPost
	 */
	public Integer getCodMemoPost() {
		return codMemoPost;
	}

	/**
	 * @param codMemoPost the codMemoPost to set
	 */
	public void setCodMemoPost(final Integer codMemoPost) {
		this.codMemoPost = codMemoPost;
	}

	/**
	 * @return the statementDate
	 */
	public Long getStatementDate() {
		return statementDate;
	}

	/**
	 * @param statementDate the statementDate to set
	 */
	public void setStatementDate(final Long statementDate) {
		this.statementDate = statementDate;
	}

	/**
	 * @return the codUserData1
	 */
	public String getCodUserData1() {
		return codUserData1;
	}

	/**
	 * @param codUserData1 the codUserData1 to set
	 */
	public void setCodUserData1(final String codUserData1) {
		this.codUserData1 = codUserData1;
	}

	/**
	 * @return the userDataDes1
	 */
	public String getUserDataDes1() {
		return userDataDes1;
	}

	/**
	 * @param userDataDes1 the userDataDes1 to set
	 */
	public void setUserDataDes1(final String userDataDes1) {
		this.userDataDes1 = userDataDes1;
	}

	/**
	 * @return the codUserData2
	 */
	public String getCodUserData2() {
		return codUserData2;
	}

	/**
	 * @param codUserData2 the codUserData2 to set
	 */
	public void setCodUserData2(final String codUserData2) {
		this.codUserData2 = codUserData2;
	}

	/**
	 * @return the userDataDes2
	 */
	public String getUserDataDes2() {
		return userDataDes2;
	}

	/**
	 * @param userDataDes2 the userDataDes2 to set
	 */
	public void setUserDataDes2(final String userDataDes2) {
		this.userDataDes2 = userDataDes2;
	}

	/**
	 * @return the codUserData3
	 */
	public String getCodUserData3() {
		return codUserData3;
	}

	/**
	 * @param codUserData3 the codUserData3 to set
	 */
	public void setCodUserData3(final String codUserData3) {
		this.codUserData3 = codUserData3;
	}

	/**
	 * @return the userDataDes3
	 */
	public String getUserDataDes3() {
		return userDataDes3;
	}

	/**
	 * @param userDataDes3 the userDataDes3 to set
	 */
	public void setUserDataDes3(final String userDataDes3) {
		this.userDataDes3 = userDataDes3;
	}

	/**
	 * @return the codUserData4
	 */
	public String getCodUserData4() {
		return codUserData4;
	}

	/**
	 * @param codUserData4 the codUserData4 to set
	 */
	public void setCodUserData4(final String codUserData4) {
		this.codUserData4 = codUserData4;
	}

	/**
	 * @return the userDataDes4
	 */
	public String getUserDataDes4() {
		return userDataDes4;
	}

	/**
	 * @param userDataDes4 the userDataDes4 to set
	 */
	public void setUserDataDes4(final String userDataDes4) {
		this.userDataDes4 = userDataDes4;
	}

	/**
	 * @return the codUserData5
	 */
	public String getCodUserData5() {
		return codUserData5;
	}

	/**
	 * @param codUserData5 the codUserData5 to set
	 */
	public void setCodUserData5(final String codUserData5) {
		this.codUserData5 = codUserData5;
	}

	/**
	 * @return the userDataDes5
	 */
	public String getUserDataDes5() {
		return userDataDes5;
	}

	/**
	 * @param userDataDes5 the userDataDes5 to set
	 */
	public void setUserDataDes5(final String userDataDes5) {
		this.userDataDes5 = userDataDes5;
	}

	/**
	 * @return the codVisaBatch
	 */
	public String getCodVisaBatch() {
		return codVisaBatch;
	}

	/**
	 * @param codVisaBatch the codVisaBatch to set
	 */
	public void setCodVisaBatch(final String codVisaBatch) {
		this.codVisaBatch = codVisaBatch;
	}

	/**
	 * @return the authDate
	 */
	public Long getAuthDate() {
		return authDate;
	}

	/**
	 * @param authDate the authDate to set
	 */
	public void setAuthDate(final Long authDate) {
		this.authDate = authDate;
	}

	/**
	 * @return the codLineItemId
	 */
	public Integer getCodLineItemId() {
		return codLineItemId;
	}

	/**
	 * @param codLineItemId the codLineItemId to set
	 */
	public void setCodLineItemId(final Integer codLineItemId) {
		this.codLineItemId = codLineItemId;
	}

	/**
	 * @return the codIssuerDefined
	 */
	public String getCodIssuerDefined() {
		return codIssuerDefined;
	}

	/**
	 * @param codIssuerDefined the codIssuerDefined to set
	 */
	public void setCodIssuerDefined(final String codIssuerDefined) {
		this.codIssuerDefined = codIssuerDefined;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(final String source) {
		this.source = source;
	}

	/**
	 * @return the optionalField
	 */
	public String getOptionalField() {
		return optionalField;
	}

	/**
	 * @param optionalField the optionalField to set
	 */
	public void setOptionalField(final String optionalField) {
		this.optionalField = optionalField;
	}

	/**
	 * @return the optionalField2
	 */
	public String getOptionalField2() {
		return optionalField2;
	}

	/**
	 * @param optionalField2 the optionalField2 to set
	 */
	public void setOptionalField2(final String optionalField2) {
		this.optionalField2 = optionalField2;
	}

	/**
	 * @return the optionalField3
	 */
	public String getOptionalField3() {
		return optionalField3;
	}

	/**
	 * @param optionalField3 the optionalField3 to set
	 */
	public void setOptionalField3(final String optionalField3) {
		this.optionalField3 = optionalField3;
	}

	/**
	 * @return the optionalField4
	 */
	public String getOptionalField4() {
		return optionalField4;
	}

	/**
	 * @param optionalField4 the optionalField4 to set
	 */
	public void setOptionalField4(final String optionalField4) {
		this.optionalField4 = optionalField4;
	}

	/**
	 * @return the reservedField
	 */
	public String getReservedField() {
		return reservedField;
	}

	/**
	 * @param reservedField the reservedField to set
	 */
	public void setReservedField(final String reservedField) {
		this.reservedField = reservedField;
	}

	/**
	 * @return the reservedField2
	 */
	public String getReservedField2() {
		return reservedField2;
	}

	/**
	 * @param reservedField2 the reservedField2 to set
	 */
	public void setReservedField2(final String reservedField2) {
		this.reservedField2 = reservedField2;
	}

	/**
	 * @return the reservedField3
	 */
	public String getReservedField3() {
		return reservedField3;
	}

	/**
	 * @param reservedField3 the reservedField3 to set
	 */
	public void setReservedField3(final String reservedField3) {
		this.reservedField3 = reservedField3;
	}

	/**
	 * @return the reservedField4
	 */
	public String getReservedField4() {
		return reservedField4;
	}

	/**
	 * @param reservedField4 the reservedField4 to set
	 */
	public void setReservedField4(final String reservedField4) {
		this.reservedField4 = reservedField4;
	}

	/**
	 * @return the codTransactionId
	 */
	public Long getCodTransactionId() {
		return codTransactionId;
	}

	/**
	 * @param codTransactionId the codTransactionId to set
	 */
	public void setCodTransactionId(final Long codTransactionId) {
		this.codTransactionId = codTransactionId;
	}

	/**
	 * @return the tip
	 */
	public Long getTip() {
		return tip;
	}

	/**
	 * @param tip the tip to set
	 */
	public void setTip(final Long tip) {
		this.tip = tip;
	}

	/**
	 * @return the feeAmount
	 */
	public Long getFeeAmount() {
		return feeAmount;
	}

	/**
	 * @param feeAmount the feeAmount to set
	 */
	public void setFeeAmount(final Long feeAmount) {
		this.feeAmount = feeAmount;
	}

	/**
	 * @return the feeDesc
	 */
	public String getFeeDesc() {
		return feeDesc;
	}

	/**
	 * @param feeDesc the feeDesc to set
	 */
	public void setFeeDesc(final String feeDesc) {
		this.feeDesc = feeDesc;
	}

	/**
	 * @return the foreingExc
	 */
	public Long getForeingExc() {
		return foreingExc;
	}

	/**
	 * @param foreingExc the foreingExc to set
	 */
	public void setForeingExc(final Long foreingExc) {
		this.foreingExc = foreingExc;
	}

	/**
	 * @return the exchangeRate
	 */
	public Integer getExchangeRate() {
		return exchangeRate;
	}

	/**
	 * @param exchangeRate the exchangeRate to set
	 */
	public void setExchangeRate(final Integer exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	/**
	 * @return the impVolmLoc
	 */
	public Double getImpVolmLoc() {
		return impVolmLoc;
	}

	/**
	 * @param impVolmLoc the impVolmLoc to set
	 */
	public void setImpVolmLoc(final Double impVolmLoc) {
		this.impVolmLoc = impVolmLoc;
	}

	/**
	 * @return the impVolmUsd
	 */
	public Double getImpVolmUsd() {
		return impVolmUsd;
	}

	/**
	 * @param impVolmUsd the impVolmUsd to set
	 */
	public void setImpVolmUsd(final Double impVolmUsd) {
		this.impVolmUsd = impVolmUsd;
	}

	/**
	 * @return the codSeqOrigen
	 */
	public Long getCodSeqOrigen() {
		return codSeqOrigen;
	}

	/**
	 * @param codSeqOrigen the codSeqOrigen to set
	 */
	public void setCodSeqOrigen(final Long codSeqOrigen) {
		this.codSeqOrigen = codSeqOrigen;
	}

	/**
	 * @return the timPrimReg
	 */
	public Timestamp getTimPrimReg() {
		return timPrimReg;
	}

	/**
	 * @param timPrimReg the timPrimReg to set
	 */
	public void setTimPrimReg(final Timestamp timPrimReg) {
		this.timPrimReg = timPrimReg;
	}

	/**
	 * @return the codSrvId
	 */
	public Integer getCodSrvId() {
		return codSrvId;
	}

	/**
	 * @param codSrvId the codSrvId to set
	 */
	public void setCodSrvId(final Integer codSrvId) {
		this.codSrvId = codSrvId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CardTransaction [codCarga=");
		builder.append(codCarga);
		builder.append(", codAccountNumber=");
		builder.append(codAccountNumber);
		builder.append(", codCompId=");
		builder.append(codCompId);
		builder.append(", postingDate=");
		builder.append(postingDate);
		builder.append(", codTransaction=");
		builder.append(codTransaction);
		builder.append(", codSeqFich=");
		builder.append(codSeqFich);
		builder.append(", codPeriod=");
		builder.append(codPeriod);
		builder.append(", codCardTp=");
		builder.append(codCardTp);
		builder.append(", codAcquiringBin=");
		builder.append(codAcquiringBin);
		builder.append(", codCardAcceptor=");
		builder.append(codCardAcceptor);
		builder.append(", supplierName=");
		builder.append(supplierName);
		builder.append(", supplierCity=");
		builder.append(supplierCity);
		builder.append(", supplierState=");
		builder.append(supplierState);
		builder.append(", codIsoCountry=");
		builder.append(codIsoCountry);
		builder.append(", codSupplrPostal=");
		builder.append(codSupplrPostal);
		builder.append(", sourceAmount=");
		builder.append(sourceAmount);
		builder.append(", billingAmount=");
		builder.append(billingAmount);
		builder.append(", codSourceCurrency=");
		builder.append(codSourceCurrency);
		builder.append(", merchantCat=");
		builder.append(merchantCat);
		builder.append(", codTransType=");
		builder.append(codTransType);
		builder.append(", transDate=");
		builder.append(transDate);
		builder.append(", codBillingCurrency=");
		builder.append(codBillingCurrency);
		builder.append(", taxAmount=");
		builder.append(taxAmount);
		builder.append(", disputeAmount=");
		builder.append(disputeAmount);
		builder.append(", disputeRea=");
		builder.append(disputeRea);
		builder.append(", disputeDate=");
		builder.append(disputeDate);
		builder.append(", codCommodity=");
		builder.append(codCommodity);
		builder.append(", supplierVat=");
		builder.append(supplierVat);
		builder.append(", orderNum=");
		builder.append(orderNum);
		builder.append(", customerVat=");
		builder.append(customerVat);
		builder.append(", VATAmount=");
		builder.append(VATAmount);
		builder.append(", taxAmount2=");
		builder.append(taxAmount2);
		builder.append(", purchaseFormt=");
		builder.append(purchaseFormt);
		builder.append(", codeCustomer=");
		builder.append(codeCustomer);
		builder.append(", purchaseId=");
		builder.append(purchaseId);
		builder.append(", transTime=");
		builder.append(transTime);
		builder.append(", codTaxAmount=");
		builder.append(codTaxAmount);
		builder.append(", codTaxAmount2=");
		builder.append(codTaxAmount2);
		builder.append(", codOrderType=");
		builder.append(codOrderType);
		builder.append(", codMessage=");
		builder.append(codMessage);
		builder.append(", processorAdd=");
		builder.append(processorAdd);
		builder.append(", merchantProf=");
		builder.append(merchantProf);
		builder.append(", codUsage=");
		builder.append(codUsage);
		builder.append(", enrichedTrans=");
		builder.append(enrichedTrans);
		builder.append(", codBillingAccount=");
		builder.append(codBillingAccount);
		builder.append(", ddaNumber=");
		builder.append(ddaNumber);
		builder.append(", ddaSavingNum=");
		builder.append(ddaSavingNum);
		builder.append(", codDisputeStatus=");
		builder.append(codDisputeStatus);
		builder.append(", matchedInd=");
		builder.append(matchedInd);
		builder.append(", rountungNum=");
		builder.append(rountungNum);
		builder.append(", codAuthorization=");
		builder.append(codAuthorization);
		builder.append(", codTransApproval=");
		builder.append(codTransApproval);
		builder.append(", codExtract=");
		builder.append(codExtract);
		builder.append(", codMemoPost=");
		builder.append(codMemoPost);
		builder.append(", statementDate=");
		builder.append(statementDate);
		builder.append(", codUserData1=");
		builder.append(codUserData1);
		builder.append(", userDataDes1=");
		builder.append(userDataDes1);
		builder.append(", codUserData2=");
		builder.append(codUserData2);
		builder.append(", userDataDes2=");
		builder.append(userDataDes2);
		builder.append(", codUserData3=");
		builder.append(codUserData3);
		builder.append(", userDataDes3=");
		builder.append(userDataDes3);
		builder.append(", codUserData4=");
		builder.append(codUserData4);
		builder.append(", userDataDes4=");
		builder.append(userDataDes4);
		builder.append(", codUserData5=");
		builder.append(codUserData5);
		builder.append(", userDataDes5=");
		builder.append(userDataDes5);
		builder.append(", codVisaBatch=");
		builder.append(codVisaBatch);
		builder.append(", authDate=");
		builder.append(authDate);
		builder.append(", codLineItemId=");
		builder.append(codLineItemId);
		builder.append(", codIssuerDefined=");
		builder.append(codIssuerDefined);
		builder.append(", source=");
		builder.append(source);
		builder.append(", optionalField=");
		builder.append(optionalField);
		builder.append(", optionalField2=");
		builder.append(optionalField2);
		builder.append(", optionalField3=");
		builder.append(optionalField3);
		builder.append(", optionalField4=");
		builder.append(optionalField4);
		builder.append(", reservedField=");
		builder.append(reservedField);
		builder.append(", reservedField2=");
		builder.append(reservedField2);
		builder.append(", reservedField3=");
		builder.append(reservedField3);
		builder.append(", reservedField4=");
		builder.append(reservedField4);
		builder.append(", codTransactionId=");
		builder.append(codTransactionId);
		builder.append(", tip=");
		builder.append(tip);
		builder.append(", feeAmount=");
		builder.append(feeAmount);
		builder.append(", feeDesc=");
		builder.append(feeDesc);
		builder.append(", foreingExc=");
		builder.append(foreingExc);
		builder.append(", exchangeRate=");
		builder.append(exchangeRate);
		builder.append(", impVolmLoc=");
		builder.append(impVolmLoc);
		builder.append(", impVolmUsd=");
		builder.append(impVolmUsd);
		builder.append(", codSeqOrigen=");
		builder.append(codSeqOrigen);
		builder.append(", timPrimReg=");
		builder.append(timPrimReg);
		builder.append(", codSrvId=");
		builder.append(codSrvId);
		builder.append("]");
		return builder.toString();
	}


	
}
