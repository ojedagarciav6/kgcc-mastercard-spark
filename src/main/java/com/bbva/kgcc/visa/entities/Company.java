package com.bbva.kgcc.visa.entities;

import java.sql.Timestamp;

public class Company {

	/** Attribute COD_LOAD_TR. */
	private String codCarga;

	/** Attribute COD_COMP_ID. */
	private Long companyId;

	/** Attribute DES_COMP_NM. */
	private String companyName;

	/** Attribute DES_COM_ADL1. */
	private String addressLine1;

	/** Attribute DES_COM_ADL2. */
	private String addressLine2;

	/** Attribute DES_CITY_C. */
	private String cicty;

	/** Attribute COD_STATE_C. */
	private String state;

	/** Attribute COD_ISO_CTRC. */
	private Long codIsoCountry;

	/** Attribute COD_COM_POST. */
	private String codPostal;

	/** Attribute QNU_FIS_YEAR. */
	private Long fiscalYear;

	/** Attribute IMP_SPEN_LIM. */
	private Long spendingLimit;

	/** Attribute COD_CARD_TP. */
	private Integer codCardType;

	/** Attribute DES_ISSU_NM. */
	private String issuerName;

	/** Attribute COD_ORG_HIER. */
	private Integer codOrgHier;

	/** Attribute QNU_EFFEC_DT. */
	private Long effectiveDate;

	/** Attribute DES_COM_ADL3. */
	private String addressLine3;

	/** Attribute COD_FED_ORG. */
	private Integer codFederalOrg;

	/** Attribute DES_OPT1. */
	private String optional1;

	/** Attribute DES_OPT2. */
	private String optional2;

	/** Attribute DES_OPT3. */
	private String optional3;

	/** Attribute DES_OPT4. */
	private String optional4;

	/** Attribute COD_PRIM_IND. */
	private Integer codPrimryIndustry;

	/** Attribute COD_SEC_IND. */
	private Integer codSecIndustry;

	/** Attribute COD_SCTOR. */
	private String codSector;

	/** Attribute DES_WEB_URL. */
	private String webSiteURL;

	/** Attribute QNU_BRADSTR. */
	private Long bradStreet;

	/** Attribute DES_PRIM_CON. */
	private String prymarContact;

	/** Attribute DES_LAST_CON. */
	private String lastContact;

	/** Attribute DES_EMAIL_C. */
	private String email;

	/** Attribute FEC_PROC. */
	private Timestamp fechaAud;

	public Company(final String codCarga, final Long companyId, final String companyName,
			final String addressLine1, final String addressLine2, final String cicty,
			final String state, final Long codIsoCountry, final String codPostal,
			final Long fiscalYear, final Long spendingLimit, final Integer codCardType,
			final String issuerName, final Integer codOrgHier, final Long effectiveDate,
			final String addressLine3, final Integer codFederalOrg, final String optional1,
			final String optional2, final String optional3, final String optional4,
			final Integer codPrimryIndustry, final Integer codSecIndustry,
			final String codSector, final String webSiteURL, final Long bradStreet,
			final String prymarContact, final String lastContact, final String email,
			final Timestamp fechaAud) {
		super();
		this.codCarga = codCarga;
		this.companyId = companyId;
		this.companyName = companyName;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.cicty = cicty;
		this.state = state;
		this.codIsoCountry = codIsoCountry;
		this.codPostal = codPostal;
		this.fiscalYear = fiscalYear;
		this.spendingLimit = spendingLimit;
		this.codCardType = codCardType;
		this.issuerName = issuerName;
		this.codOrgHier = codOrgHier;
		this.effectiveDate = effectiveDate;
		this.addressLine3 = addressLine3;
		this.codFederalOrg = codFederalOrg;
		this.optional1 = optional1;
		this.optional2 = optional2;
		this.optional3 = optional3;
		this.optional4 = optional4;
		this.codPrimryIndustry = codPrimryIndustry;
		this.codSecIndustry = codSecIndustry;
		this.codSector = codSector;
		this.webSiteURL = webSiteURL;
		this.bradStreet = bradStreet;
		this.prymarContact = prymarContact;
		this.lastContact = lastContact;
		this.email = email;
		this.fechaAud = fechaAud;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the companyId
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(final Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(final String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the cicty
	 */
	public String getCicty() {
		return cicty;
	}

	/**
	 * @param cicty the cicty to set
	 */
	public void setCicty(final String cicty) {
		this.cicty = cicty;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(final String state) {
		this.state = state;
	}

	/**
	 * @return the codIsoCountry
	 */
	public Long getCodIsoCountry() {
		return codIsoCountry;
	}

	/**
	 * @param codIsoCountry the codIsoCountry to set
	 */
	public void setCodIsoCountry(final Long codIsoCountry) {
		this.codIsoCountry = codIsoCountry;
	}

	/**
	 * @return the codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}

	/**
	 * @param codPostal the codPostal to set
	 */
	public void setCodPostal(final String codPostal) {
		this.codPostal = codPostal;
	}

	/**
	 * @return the fiscalYear
	 */
	public Long getFiscalYear() {
		return fiscalYear;
	}

	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(final Long fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the spendingLimit
	 */
	public Long getSpendingLimit() {
		return spendingLimit;
	}

	/**
	 * @param spendingLimit the spendingLimit to set
	 */
	public void setSpendingLimit(final Long spendingLimit) {
		this.spendingLimit = spendingLimit;
	}

	/**
	 * @return the codCardType
	 */
	public Integer getCodCardType() {
		return codCardType;
	}

	/**
	 * @param codCardType the codCardType to set
	 */
	public void setCodCardType(final Integer codCardType) {
		this.codCardType = codCardType;
	}

	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(final String issuerName) {
		this.issuerName = issuerName;
	}

	/**
	 * @return the codOrgHier
	 */
	public Integer getCodOrgHier() {
		return codOrgHier;
	}

	/**
	 * @param codOrgHier the codOrgHier to set
	 */
	public void setCodOrgHier(final Integer codOrgHier) {
		this.codOrgHier = codOrgHier;
	}

	/**
	 * @return the effectiveDate
	 */
	public Long getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(final Long effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the addressLine3
	 */
	public String getAddressLine3() {
		return addressLine3;
	}

	/**
	 * @param addressLine3 the addressLine3 to set
	 */
	public void setAddressLine3(final String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	/**
	 * @return the codFederalOrg
	 */
	public Integer getCodFederalOrg() {
		return codFederalOrg;
	}

	/**
	 * @param codFederalOrg the codFederalOrg to set
	 */
	public void setCodFederalOrg(final Integer codFederalOrg) {
		this.codFederalOrg = codFederalOrg;
	}

	/**
	 * @return the optional1
	 */
	public String getOptional1() {
		return optional1;
	}

	/**
	 * @param optional1 the optional1 to set
	 */
	public void setOptional1(final String optional1) {
		this.optional1 = optional1;
	}

	/**
	 * @return the optional2
	 */
	public String getOptional2() {
		return optional2;
	}

	/**
	 * @param optional2 the optional2 to set
	 */
	public void setOptional2(final String optional2) {
		this.optional2 = optional2;
	}

	/**
	 * @return the optional3
	 */
	public String getOptional3() {
		return optional3;
	}

	/**
	 * @param optional3 the optional3 to set
	 */
	public void setOptional3(final String optional3) {
		this.optional3 = optional3;
	}

	/**
	 * @return the optional4
	 */
	public String getOptional4() {
		return optional4;
	}

	/**
	 * @param optional4 the optional4 to set
	 */
	public void setOptional4(final String optional4) {
		this.optional4 = optional4;
	}

	/**
	 * @return the codPrimryIndustry
	 */
	public Integer getCodPrimryIndustry() {
		return codPrimryIndustry;
	}

	/**
	 * @param codPrimryIndustry the codPrimryIndustry to set
	 */
	public void setCodPrimryIndustry(final Integer codPrimryIndustry) {
		this.codPrimryIndustry = codPrimryIndustry;
	}

	/**
	 * @return the codSecIndustry
	 */
	public Integer getCodSecIndustry() {
		return codSecIndustry;
	}

	/**
	 * @param codSecIndustry the codSecIndustry to set
	 */
	public void setCodSecIndustry(final Integer codSecIndustry) {
		this.codSecIndustry = codSecIndustry;
	}

	/**
	 * @return the codSector
	 */
	public String getCodSector() {
		return codSector;
	}

	/**
	 * @param codSector the codSector to set
	 */
	public void setCodSector(final String codSector) {
		this.codSector = codSector;
	}

	/**
	 * @return the webSiteURL
	 */
	public String getWebSiteURL() {
		return webSiteURL;
	}

	/**
	 * @param webSiteURL the webSiteURL to set
	 */
	public void setWebSiteURL(final String webSiteURL) {
		this.webSiteURL = webSiteURL;
	}

	/**
	 * @return the bradStreet
	 */
	public Long getBradStreet() {
		return bradStreet;
	}

	/**
	 * @param bradStreet the bradStreet to set
	 */
	public void setBradStreet(final Long bradStreet) {
		this.bradStreet = bradStreet;
	}

	/**
	 * @return the prymarContact
	 */
	public String getPrymarContact() {
		return prymarContact;
	}

	/**
	 * @param prymarContact the prymarContact to set
	 */
	public void setPrymarContact(final String prymarContact) {
		this.prymarContact = prymarContact;
	}

	/**
	 * @return the lastContact
	 */
	public String getLastContact() {
		return lastContact;
	}

	/**
	 * @param lastContact the lastContact to set
	 */
	public void setLastContact(final String lastContact) {
		this.lastContact = lastContact;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the fechaAud
	 */
	public Timestamp getFechaAud() {
		return fechaAud;
	}

	/**
	 * @param fechaAud the fechaAud to set
	 */
	public void setFechaAud(final Timestamp fechaAud) {
		this.fechaAud = fechaAud;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Company [codCarga=");
		builder.append(codCarga);
		builder.append(", companyId=");
		builder.append(companyId);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", addressLine1=");
		builder.append(addressLine1);
		builder.append(", addressLine2=");
		builder.append(addressLine2);
		builder.append(", cicty=");
		builder.append(cicty);
		builder.append(", state=");
		builder.append(state);
		builder.append(", codIsoCountry=");
		builder.append(codIsoCountry);
		builder.append(", codPostal=");
		builder.append(codPostal);
		builder.append(", fiscalYear=");
		builder.append(fiscalYear);
		builder.append(", spendingLimit=");
		builder.append(spendingLimit);
		builder.append(", codCardType=");
		builder.append(codCardType);
		builder.append(", issuerName=");
		builder.append(issuerName);
		builder.append(", codOrgHier=");
		builder.append(codOrgHier);
		builder.append(", effectiveDate=");
		builder.append(effectiveDate);
		builder.append(", addressLine3=");
		builder.append(addressLine3);
		builder.append(", codFederalOrg=");
		builder.append(codFederalOrg);
		builder.append(", optional1=");
		builder.append(optional1);
		builder.append(", optional2=");
		builder.append(optional2);
		builder.append(", optional3=");
		builder.append(optional3);
		builder.append(", optional4=");
		builder.append(optional4);
		builder.append(", codPrimryIndustry=");
		builder.append(codPrimryIndustry);
		builder.append(", codSecIndustry=");
		builder.append(codSecIndustry);
		builder.append(", codSector=");
		builder.append(codSector);
		builder.append(", webSiteURL=");
		builder.append(webSiteURL);
		builder.append(", bradStreet=");
		builder.append(bradStreet);
		builder.append(", prymarContact=");
		builder.append(prymarContact);
		builder.append(", lastContact=");
		builder.append(lastContact);
		builder.append(", email=");
		builder.append(email);
		builder.append(", fechaAud=");
		builder.append(fechaAud);
		builder.append("]");
		return builder.toString();
	}
	

}
