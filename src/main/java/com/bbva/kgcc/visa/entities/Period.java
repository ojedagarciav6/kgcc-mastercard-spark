package com.bbva.kgcc.visa.entities;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class Period.
 */
@XStreamAlias("period")
public class Period implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Attribute CODE_LOAD_TRANS. */
	private String codCarga;

	/** Attribute COD_COMP_ID. */
	private Long codCompId;

	/** Attribute COD_PERIOD. */
	private Integer codPeriod;

	/** Attribute COD_CARD_TYPE. */
	private Integer codCardType;

	/** Attribute QNU_START_DATE. */
	private Long startDate;

	/** Attribute QNU_END_DATE. */
	private Long endDate;

	/** Attribute COD_PERIOD_COMPLET. */
	private Integer codPeriodComplet;

	/** Attribute OPTINAL_FIELD1. */
	private String optionalField1;

	/** Attribute OPTINAL_FIELD2. */
	private String optionalField2;

	/** Attribute OPTINAL_FIELD3. */
	private String optionalField3;

	/** Attribute OPTINAL_FIELD4. */
	private String optionalField4;
	
	public Period() {
		
	}

	public Period(final String codCarga, final Long codCompId, final Integer codPeriod,
			final Integer codCardType, final Long startDate, final Long endDate,
			final Integer codPeriodComplet, final String optionalField1,
			final String optionalField2, final String optionalField3, final String optionalField4) {
		super();
		this.codCarga = codCarga;
		this.codCompId = codCompId;
		this.codPeriod = codPeriod;
		this.codCardType = codCardType;
		this.startDate = startDate;
		this.endDate = endDate;
		this.codPeriodComplet = codPeriodComplet;
		this.optionalField1 = optionalField1;
		this.optionalField2 = optionalField2;
		this.optionalField3 = optionalField3;
		this.optionalField4 = optionalField4;
	}

	/**
	 * @return the codCarga
	 */
	public String getCodCarga() {
		return codCarga;
	}

	/**
	 * @param codCarga the codCarga to set
	 */
	public void setCodCarga(final String codCarga) {
		this.codCarga = codCarga;
	}

	/**
	 * @return the codCompId
	 */
	public Long getCodCompId() {
		return codCompId;
	}

	/**
	 * @param codCompId the codCompId to set
	 */
	public void setCodCompId(final Long codCompId) {
		this.codCompId = codCompId;
	}

	/**
	 * @return the codPeriod
	 */
	public Integer getCodPeriod() {
		return codPeriod;
	}

	/**
	 * @param codPeriod the codPeriod to set
	 */
	public void setCodPeriod(final Integer codPeriod) {
		this.codPeriod = codPeriod;
	}

	/**
	 * @return the codCardType
	 */
	public Integer getCodCardType() {
		return codCardType;
	}

	/**
	 * @param codCardType the codCardType to set
	 */
	public void setCodCardType(final Integer codCardType) {
		this.codCardType = codCardType;
	}

	/**
	 * @return the startDate
	 */
	public Long getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(final Long startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Long getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(final Long endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the codPeriodComplet
	 */
	public Integer getCodPeriodComplet() {
		return codPeriodComplet;
	}

	/**
	 * @param codPeriodComplet the codPeriodComplet to set
	 */
	public void setCodPeriodComplet(final Integer codPeriodComplet) {
		this.codPeriodComplet = codPeriodComplet;
	}

	/**
	 * @return the optionalField1
	 */
	public String getOptionalField1() {
		return optionalField1;
	}

	/**
	 * @param optionalField1 the optionalField1 to set
	 */
	public void setOptionalField1(final String optionalField1) {
		this.optionalField1 = optionalField1;
	}

	/**
	 * @return the optionalField2
	 */
	public String getOptionalField2() {
		return optionalField2;
	}

	/**
	 * @param optionalField2 the optionalField2 to set
	 */
	public void setOptionalField2(final String optionalField2) {
		this.optionalField2 = optionalField2;
	}

	/**
	 * @return the optionalField3
	 */
	public String getOptionalField3() {
		return optionalField3;
	}

	/**
	 * @param optionalField3 the optionalField3 to set
	 */
	public void setOptionalField3(final String optionalField3) {
		this.optionalField3 = optionalField3;
	}

	/**
	 * @return the optionalField4
	 */
	public String getOptionalField4() {
		return optionalField4;
	}

	/**
	 * @param optionalField4 the optionalField4 to set
	 */
	public void setOptionalField4(final String optionalField4) {
		this.optionalField4 = optionalField4;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Period [codCarga=");
		builder.append(codCarga);
		builder.append(", codCompId=");
		builder.append(codCompId);
		builder.append(", codPeriod=");
		builder.append(codPeriod);
		builder.append(", codCardType=");
		builder.append(codCardType);
		builder.append(", startDate=");
		builder.append(startDate);
		builder.append(", endDate=");
		builder.append(endDate);
		builder.append(", codPeriodComplet=");
		builder.append(codPeriodComplet);
		builder.append(", optionalField1=");
		builder.append(optionalField1);
		builder.append(", optionalField2=");
		builder.append(optionalField2);
		builder.append(", optionalField3=");
		builder.append(optionalField3);
		builder.append(", optionalField4=");
		builder.append(optionalField4);
		builder.append("]");
		return builder.toString();
	}

	
	
}
