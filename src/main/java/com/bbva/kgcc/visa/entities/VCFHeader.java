package com.bbva.kgcc.visa.entities;

import java.sql.Timestamp;

/**
 * The Class VCFHeader.
 */
public class VCFHeader {

	/** Attribute COD_HEAD_ID. */
	private Long codHeadId;

	/** Attribute COD_PARENT_H. */
	private Long codParentHeadId;

	/** Attribute COD_COUNTRY. */
	private String pais;

	/** Attribute COD_TRANSCT. */
	private Integer codTransct;

	/** Attribute COD_COMPANY_ID. */
	private Long idCompany;

	/** Attribute COD_SEQ_NB. */
	private Integer codSequence;

	/** Attribute QNU_DATE. */
	private Timestamp fecha;

	/** Attribute COD_SEQ_NB. */
	private Integer codRecType;

	/** Attribute QNU_REC_COUN. */
	private Long recCount;

	/** Attribute IMP_TOTAL_AM. */
	private Long importeTotalAm;

	/** Attribute DES_FIL_FORM. */
	private String desFilForm;

	/** Attribute DES_IDENT_NM. */
	private String desIdentNm;

	/** Attribute DES_IDENT_NM. */
	private Long codPrcId;

	/** Attribute COD_VISA_REG. */
	private Integer codVisaReg;

	/** Attribute DES_PROC_PLA. */
	private String desProcPla;

	/** Attribute DES_OPTION_1. */
	private String desOption1;

	/** Attribute DES_OPTION_2. */
	private String desOption2;

	/** Attribute DES_OPTION_3. */
	private String desOption3;

	/** Attribute DES_OPTION_4. */
	private String desOption4;

	/** Attribute FEC_PROC. */
	private Timestamp fechaAud;

	/** The cod user. */
	private String codUser;

	/** Attribute xtiRepair. */
	private String xtiRepair;

	/** Attribute codFileId. */
	private Integer codFileId;

	public VCFHeader(final Long codHeadId, final Long codParentHeadId, final String pais,
			final Integer codTransct, final Long idCompany, final Integer codSequence,
			final Timestamp fecha, final Integer codRecType, final Long recCount,
			final Long importeTotalAm, final String desFilForm, final String desIdentNm,
			final Long codPrcId, final Integer codVisaReg, final String desProcPla,
			final String desOption1, final String desOption2, final String desOption3,
			final String desOption4, final Timestamp fechaAud, final String codUser,
			final String xtiRepair, final Integer codFileId) {
		super();
		this.codHeadId = codHeadId;
		this.codParentHeadId = codParentHeadId;
		this.pais = pais;
		this.codTransct = codTransct;
		this.idCompany = idCompany;
		this.codSequence = codSequence;
		this.fecha = fecha;
		this.codRecType = codRecType;
		this.recCount = recCount;
		this.importeTotalAm = importeTotalAm;
		this.desFilForm = desFilForm;
		this.desIdentNm = desIdentNm;
		this.codPrcId = codPrcId;
		this.codVisaReg = codVisaReg;
		this.desProcPla = desProcPla;
		this.desOption1 = desOption1;
		this.desOption2 = desOption2;
		this.desOption3 = desOption3;
		this.desOption4 = desOption4;
		this.fechaAud = fechaAud;
		this.codUser = codUser;
		this.xtiRepair = xtiRepair;
		this.codFileId = codFileId;
	}

	public VCFHeader() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the codHeadId
	 */
	public Long getCodHeadId() {
		return codHeadId;
	}

	/**
	 * @param codHeadId the codHeadId to set
	 */
	public void setCodHeadId(final Long codHeadId) {
		this.codHeadId = codHeadId;
	}

	/**
	 * @return the codParentHeadId
	 */
	public Long getCodParentHeadId() {
		return codParentHeadId;
	}

	/**
	 * @param codParentHeadId the codParentHeadId to set
	 */
	public void setCodParentHeadId(final Long codParentHeadId) {
		this.codParentHeadId = codParentHeadId;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(final String pais) {
		this.pais = pais;
	}

	/**
	 * @return the codTransct
	 */
	public Integer getCodTransct() {
		return codTransct;
	}

	/**
	 * @param codTransct the codTransct to set
	 */
	public void setCodTransct(final Integer codTransct) {
		this.codTransct = codTransct;
	}

	/**
	 * @return the idCompany
	 */
	public Long getIdCompany() {
		return idCompany;
	}

	/**
	 * @param idCompany the idCompany to set
	 */
	public void setIdCompany(final Long idCompany) {
		this.idCompany = idCompany;
	}

	/**
	 * @return the codSequence
	 */
	public Integer getCodSequence() {
		return codSequence;
	}

	/**
	 * @param codSequence the codSequence to set
	 */
	public void setCodSequence(final Integer codSequence) {
		this.codSequence = codSequence;
	}

	/**
	 * @return the fecha
	 */
	public Timestamp getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(final Timestamp fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the codRecType
	 */
	public Integer getCodRecType() {
		return codRecType;
	}

	/**
	 * @param codRecType the codRecType to set
	 */
	public void setCodRecType(final Integer codRecType) {
		this.codRecType = codRecType;
	}

	/**
	 * @return the recCount
	 */
	public Long getRecCount() {
		return recCount;
	}

	/**
	 * @param recCount the recCount to set
	 */
	public void setRecCount(final Long recCount) {
		this.recCount = recCount;
	}

	/**
	 * @return the importeTotalAm
	 */
	public Long getImporteTotalAm() {
		return importeTotalAm;
	}

	/**
	 * @param importeTotalAm the importeTotalAm to set
	 */
	public void setImporteTotalAm(final Long importeTotalAm) {
		this.importeTotalAm = importeTotalAm;
	}

	/**
	 * @return the desFilForm
	 */
	public String getDesFilForm() {
		return desFilForm;
	}

	/**
	 * @param desFilForm the desFilForm to set
	 */
	public void setDesFilForm(final String desFilForm) {
		this.desFilForm = desFilForm;
	}

	/**
	 * @return the desIdentNm
	 */
	public String getDesIdentNm() {
		return desIdentNm;
	}

	/**
	 * @param desIdentNm the desIdentNm to set
	 */
	public void setDesIdentNm(final String desIdentNm) {
		this.desIdentNm = desIdentNm;
	}

	/**
	 * @return the codPrcId
	 */
	public Long getCodPrcId() {
		return codPrcId;
	}

	/**
	 * @param codPrcId the codPrcId to set
	 */
	public void setCodPrcId(final Long codPrcId) {
		this.codPrcId = codPrcId;
	}

	/**
	 * @return the codVisaReg
	 */
	public Integer getCodVisaReg() {
		return codVisaReg;
	}

	/**
	 * @param codVisaReg the codVisaReg to set
	 */
	public void setCodVisaReg(final Integer codVisaReg) {
		this.codVisaReg = codVisaReg;
	}

	/**
	 * @return the desProcPla
	 */
	public String getDesProcPla() {
		return desProcPla;
	}

	/**
	 * @param desProcPla the desProcPla to set
	 */
	public void setDesProcPla(final String desProcPla) {
		this.desProcPla = desProcPla;
	}

	/**
	 * @return the desOption1
	 */
	public String getDesOption1() {
		return desOption1;
	}

	/**
	 * @param desOption1 the desOption1 to set
	 */
	public void setDesOption1(final String desOption1) {
		this.desOption1 = desOption1;
	}

	/**
	 * @return the desOption2
	 */
	public String getDesOption2() {
		return desOption2;
	}

	/**
	 * @param desOption2 the desOption2 to set
	 */
	public void setDesOption2(final String desOption2) {
		this.desOption2 = desOption2;
	}

	/**
	 * @return the desOption3
	 */
	public String getDesOption3() {
		return desOption3;
	}

	/**
	 * @param desOption3 the desOption3 to set
	 */
	public void setDesOption3(final String desOption3) {
		this.desOption3 = desOption3;
	}

	/**
	 * @return the desOption4
	 */
	public String getDesOption4() {
		return desOption4;
	}

	/**
	 * @param desOption4 the desOption4 to set
	 */
	public void setDesOption4(final String desOption4) {
		this.desOption4 = desOption4;
	}

	/**
	 * @return the fechaAud
	 */
	public Timestamp getFechaAud() {
		return fechaAud;
	}

	/**
	 * @param fechaAud the fechaAud to set
	 */
	public void setFechaAud(final Timestamp fechaAud) {
		this.fechaAud = fechaAud;
	}

	/**
	 * @return the codUser
	 */
	public String getCodUser() {
		return codUser;
	}

	/**
	 * @param codUser the codUser to set
	 */
	public void setCodUser(final String codUser) {
		this.codUser = codUser;
	}

	/**
	 * @return the xtiRepair
	 */
	public String getXtiRepair() {
		return xtiRepair;
	}

	/**
	 * @param xtiRepair the xtiRepair to set
	 */
	public void setXtiRepair(final String xtiRepair) {
		this.xtiRepair = xtiRepair;
	}

	/**
	 * @return the codFileId
	 */
	public Integer getCodFileId() {
		return codFileId;
	}

	/**
	 * @param codFileId the codFileId to set
	 */
	public void setCodFileId(final Integer codFileId) {
		this.codFileId = codFileId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VCFHeader [codHeadId=");
		builder.append(codHeadId);
		builder.append(", codParentHeadId=");
		builder.append(codParentHeadId);
		builder.append(", pais=");
		builder.append(pais);
		builder.append(", codTransct=");
		builder.append(codTransct);
		builder.append(", idCompany=");
		builder.append(idCompany);
		builder.append(", codSequence=");
		builder.append(codSequence);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", codRecType=");
		builder.append(codRecType);
		builder.append(", recCount=");
		builder.append(recCount);
		builder.append(", importeTotalAm=");
		builder.append(importeTotalAm);
		builder.append(", desFilForm=");
		builder.append(desFilForm);
		builder.append(", desIdentNm=");
		builder.append(desIdentNm);
		builder.append(", codPrcId=");
		builder.append(codPrcId);
		builder.append(", codVisaReg=");
		builder.append(codVisaReg);
		builder.append(", desProcPla=");
		builder.append(desProcPla);
		builder.append(", desOption1=");
		builder.append(desOption1);
		builder.append(", desOption2=");
		builder.append(desOption2);
		builder.append(", desOption3=");
		builder.append(desOption3);
		builder.append(", desOption4=");
		builder.append(desOption4);
		builder.append(", fechaAud=");
		builder.append(fechaAud);
		builder.append(", codUser=");
		builder.append(codUser);
		builder.append(", xtiRepair=");
		builder.append(xtiRepair);
		builder.append(", codFileId=");
		builder.append(codFileId);
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
