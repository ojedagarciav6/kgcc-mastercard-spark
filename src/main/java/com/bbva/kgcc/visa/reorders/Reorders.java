package com.bbva.kgcc.visa.reorders;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;

public class Reorders {

	public static Dataset<Row> reOrderPERDataset (Dataset<Row> firstIn){
    	
    	Map<String, String> columnsRenamed = Map.ofEntries(
    			//new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
    			new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
    			//new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
    			//new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
    			//new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
    			new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
    			new AbstractMap.SimpleEntry<>("COD_PERIOD_N", "codPeriod"),
    			new AbstractMap.SimpleEntry<>("COD_CARD_TP", "codCardTypePER"),
    			new AbstractMap.SimpleEntry<>("QNU_START_DT", "startDate"),
    			new AbstractMap.SimpleEntry<>("QNU_END_DATE", "endDate"),
    			new AbstractMap.SimpleEntry<>("COD_PER_COMP", "codPeriodComplet")
    			//new AbstractMap.SimpleEntry<>("DES_OPT_FD1", "optionalField1"),
    			//new AbstractMap.SimpleEntry<>("DES_OPT_FD2", "optionalField2"),
    			//new AbstractMap.SimpleEntry<>("DES_OPT_FD3", "optionalField3"),
    			//new AbstractMap.SimpleEntry<>("DES_OPT_FD4", "optionalField4")
    			);
    	
    	Map<String, DataType> castColumns = Map.ofEntries(
    			new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codPeriod", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codCardTypePER", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("startDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("endDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codPeriodComplet", DataTypes.IntegerType)
    			);
    	
    	Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
    	
    	while(renamedColumns.hasNext()) {
    		Map.Entry<String, String> entry = renamedColumns.next();
    		firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
    	}
    	
    	Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
    	
    	while(columnsCasted.hasNext()) {
    		
    		Map.Entry<String, DataType> entry = columnsCasted.next();
    		firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
    		
    	}
    	
    	firstIn = firstIn.select("codCarga","codCompId","codPeriod","codCardTypePER","startDate","endDate","codPeriodComplet");
    	
    	System.out.println("firstIn Period: " + firstIn );
    			    	
    	return firstIn;
    }
    
	public static Dataset<Row> reOrderCHDataset (Dataset<Row> firstIn){
		Map<String, String> columnsRenamed = Map.ofEntries(
				//new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
				new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
				//new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
//				new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
//				new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
				new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
				new AbstractMap.SimpleEntry<>("COD_CARDHOLD", "idCardHolder"),
				new AbstractMap.SimpleEntry<>("COD_HIER_NOD", "hierarchyNode"),
				new AbstractMap.SimpleEntry<>("DES_FIRST_NM", "firstName")
//				new AbstractMap.SimpleEntry<>("DES_SECON_NM", "lastName"),
//				new AbstractMap.SimpleEntry<>("DES_CHL_ADL1", "addressLine1"),
//				new AbstractMap.SimpleEntry<>("DES_CHL_ADL2", "addressLine2"),
//				new AbstractMap.SimpleEntry<>("DES_CITY_C", "city"),
//				new AbstractMap.SimpleEntry<>("COD_STATE_C", "state"),
//				new AbstractMap.SimpleEntry<>("COD_ISO_CTRY", "codISOCountry"),
//				new AbstractMap.SimpleEntry<>("COD_CHL_POST", "codPostal"),
//				new AbstractMap.SimpleEntry<>("DES_CHL_ADL3", "addressLine3"),
//				new AbstractMap.SimpleEntry<>("DES_MAIL_STP", "mailStop"),
//				new AbstractMap.SimpleEntry<>("COD_PHONE_NM", "phoneNumber"),
//				new AbstractMap.SimpleEntry<>("COD_FAX_NUM", "faxNumber"),
//				new AbstractMap.SimpleEntry<>("COD_CH_OTH", "codCardHolderOTH"),
//				new AbstractMap.SimpleEntry<>("QNU_TRAIN_DT", "trainingDate"),
//				new AbstractMap.SimpleEntry<>("DES_MAIL_ADD", "emailAddress"),
//				new AbstractMap.SimpleEntry<>("DES_AUTHORI1", "authorizduser1"),
//				new AbstractMap.SimpleEntry<>("DES_AUTHORI2", "authorizduser2"),
//				new AbstractMap.SimpleEntry<>("DES_AUTHORI3", "authorizduser3"),
//				new AbstractMap.SimpleEntry<>("COD_EMPLOYEE", "codEmployee"),
//				new AbstractMap.SimpleEntry<>("COD_HOME_PHN", "homePhone"),
//				new AbstractMap.SimpleEntry<>("DES_MIDDL_NM", "middleName"),
//				new AbstractMap.SimpleEntry<>("COD_VISA_BUY", "visaBuyer"),
//				new AbstractMap.SimpleEntry<>("COD_VEHICLE", "codVehicle"),
//				new AbstractMap.SimpleEntry<>("DES_MISC_FD1", "miscField1"),
//				new AbstractMap.SimpleEntry<>("DES_MS_F1_DS", "miscField1DS"),
//				new AbstractMap.SimpleEntry<>("DES_MISC_FD2", "miscField2"),
//				new AbstractMap.SimpleEntry<>("DES_MS_F2_DS", "miscField2DS"),
//				new AbstractMap.SimpleEntry<>("DES_OPT1", "optionalField1"),
//				new AbstractMap.SimpleEntry<>("DES_OPT2", "optionalField2"),
//				new AbstractMap.SimpleEntry<>("DES_OPT3", "optionalField3"),
//				new AbstractMap.SimpleEntry<>("DES_OPT4", "optionalField4")
    	);
		
    	Map<String, DataType> castColumns = Map.ofEntries(
    			new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("trainingDate", DataTypes.LongType));
    	
    	Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
    	
    	while(renamedColumns.hasNext()) {
    		Map.Entry<String, String> entry = renamedColumns.next();
    		firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
    	}
    	
    	Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
    	
    	while(columnsCasted.hasNext()) {
    		
    		Map.Entry<String, DataType> entry = columnsCasted.next();
    		firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
    		
    	}
    	    	
    	return firstIn;
    }
    
	public static Dataset<Row> reOrderABDataset (Dataset<Row> firstIn){
    	
    	Map<String, String> columnsRenamed = Map.ofEntries(
    			//new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
    			new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
    			//new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
//    			new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
    			new AbstractMap.SimpleEntry<>("COD_CARD_TP", "codCardTp"),
    			//new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
    			new AbstractMap.SimpleEntry<>("COD_ACC_NUM", "codAccountNum"),
    			new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
//    			new AbstractMap.SimpleEntry<>("QNU_CLOS_DAT", "closingDate"),
//    			new AbstractMap.SimpleEntry<>("COD_PERIOD_N", "period"),
//    			new AbstractMap.SimpleEntry<>("IMP_PREV_BAL", "prevBalance"),
//    			new AbstractMap.SimpleEntry<>("IMP_CURR_BAL", "currBalance"),
//    			new AbstractMap.SimpleEntry<>("QNU_CRED_LIM", "creditLimit"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_DUE", "amountDue"),
//    			new AbstractMap.SimpleEntry<>("QNU_PAS_DUE", "pastDueCount"),
//    			new AbstractMap.SimpleEntry<>("IMP_PST_DUE", "pastDueAmount"),
//    			new AbstractMap.SimpleEntry<>("IMP_DSPT_AMT", "disputedAmount"),
    			new AbstractMap.SimpleEntry<>("COD_BILL_CUR", "billingCurrency")
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_PAST", "amountPastD"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_CYC2", "amountCycle2"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_CYC3", "amountCycle3"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_CYC4", "amountCycle4"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_CYC5", "amountCycle5"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_CYC6", "amountCycle6"),
//    			new AbstractMap.SimpleEntry<>("IMP_AMT_CYCP", "amountCycleP"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYC1", "billingCycle1"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYC2", "billingCycle2"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYC3", "billingCycle3"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYC4", "billingCycle4"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYC5", "billingCycle5"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYC6", "billingCycle6"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYCP", "billingCycleP"),
//    			new AbstractMap.SimpleEntry<>("QNU_BLL_CYCC", "billingCycleC"),
//    			new AbstractMap.SimpleEntry<>("IMP_LST_PAYA", "lasPaymentAmount"),
//    			new AbstractMap.SimpleEntry<>("QNU_LST_PAYD", "lastPaymentDate"),
//    			new AbstractMap.SimpleEntry<>("QNU_PAY_DUE", "paymentDueDate"),
//    			new AbstractMap.SimpleEntry<>("IMP_HIGH_BAL", "higBalance"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT1", "optional1"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT2", "optional2"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT3", "optional3"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT4", "optional4"),
//    			new AbstractMap.SimpleEntry<>("QRA_ANN_PERC", "anualPerc"),
//    			new AbstractMap.SimpleEntry<>("IMP_PURC_BAL", "purchaseBalance"),
//    			new AbstractMap.SimpleEntry<>("QRA_RATE_BAL", "rateBalance"),
//    			new AbstractMap.SimpleEntry<>("IMP_BAL_TRNS", "balanceTransfer"),
//    			new AbstractMap.SimpleEntry<>("QRA_RAT_CASH", "rateCashAdv"),
//    			new AbstractMap.SimpleEntry<>("IMP_CSH_ADV", "cashAdvSubj"),
//    			new AbstractMap.SimpleEntry<>("QNU_TOT_REWA", "rewardsPoints"),
//    			new AbstractMap.SimpleEntry<>("QNU_REWAR_PT", "totalRewards"),
//    			new AbstractMap.SimpleEntry<>("QNU_BONUS_PT", "bonusPoints"),
    			//new AbstractMap.SimpleEntry<>("IMP_TOT_CHRG", "totalCharged")
    			);
    	
    	Map<String, DataType> castColumns = Map.ofEntries(
    			new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("closingDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("period", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("prevBalance", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("currBalance", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCardTp", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("creditLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountDue", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("pastDueCount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("pastDueAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("disputedAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCurrency", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountPastD", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountCycle2", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountCycle3", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountCycle4", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountCycle5", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountCycle6", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("amountCycleP", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycle1", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycle2", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycle3", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycle4", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycle5", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycle6", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycleP", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingCycleC", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("lasPaymentAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("lastPaymentDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("paymentDueDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("higBalance", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("anualPerc", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("purchaseBalance", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("rateBalance", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("balanceTransfer", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("rateCashAdv", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("cashAdvSubj", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("rewardsPoints", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("totalRewards", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("bonusPoints", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("totalCharged", DataTypes.LongType)
    			);
    	
    	Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
    	
    	while(renamedColumns.hasNext()) {
    		Map.Entry<String, String> entry = renamedColumns.next();
    		firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
    	}
    	
    	Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
    	
    	while(columnsCasted.hasNext()) {
    		
    		Map.Entry<String, DataType> entry = columnsCasted.next();
    		firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
    		
    	}
    	
    	return firstIn;
    }
    
	public static Dataset<Row> reOrderCACDataset (Dataset<Row> firstIn){
    	
    	Map<String, String> columnsRenamed = Map.ofEntries(
    			//new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
    			new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
    			//new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
    			new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
    			//new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
    			new AbstractMap.SimpleEntry<>("COD_CARDHOLD", "codCarHolder"),
    			new AbstractMap.SimpleEntry<>("COD_ACC_NUM", "accountNum"),
    			new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
    			new AbstractMap.SimpleEntry<>("COD_HIER_NOD", "hierarchyNode"),
    			new AbstractMap.SimpleEntry<>("QNU_EFFEC_DT", "effectiveDate"),
    			new AbstractMap.SimpleEntry<>("COD_STA_COD", "statusCode"),
    			new AbstractMap.SimpleEntry<>("COD_REAS_STA", "reasonStatus"),
    			new AbstractMap.SimpleEntry<>("COD_CARD_TP", "cardType"),
    			new AbstractMap.SimpleEntry<>("COD_ACC_TYPE", "codAccountType"),
    			new AbstractMap.SimpleEntry<>("QNU_STAT_TP", "statementType"),
    			new AbstractMap.SimpleEntry<>("QNU_CARD_EXP", "accountExpired")
//    			new AbstractMap.SimpleEntry<>("QNU_ACC_OPEN", "accountOpen"),
//    			new AbstractMap.SimpleEntry<>("QNU_ACC_CLOS", "accountClose"),
//    			new AbstractMap.SimpleEntry<>("IMP_SPEN_LIM", "spendingLimit"),
//    			new AbstractMap.SimpleEntry<>("QNU_LAST_REV", "lastRevision"),
//    			new AbstractMap.SimpleEntry<>("IMP_TRN_LIM", "transLimit"),
//    			new AbstractMap.SimpleEntry<>("COD_CORP_PAY", "corporatePay"),
//    			new AbstractMap.SimpleEntry<>("DES_BLL_ACC", "billingAccount"),
//    			new AbstractMap.SimpleEntry<>("DES_COST_CNT", "costCenter"),
//    			new AbstractMap.SimpleEntry<>("DES_SUB_ACCO", "subAccount"),
//    			new AbstractMap.SimpleEntry<>("QNU_DAY_LIM", "dailyLimit"),
//    			new AbstractMap.SimpleEntry<>("QNU_CYC_LIM", "cycleLimit"),
//    			new AbstractMap.SimpleEntry<>("QNU_STA_DT", "statusDate"),
//    			new AbstractMap.SimpleEntry<>("COD_PR_FOU_I", "codPrFoundedInd"),
//    			new AbstractMap.SimpleEntry<>("COD_CT_PA_PR", "codCityPairProg"),
//    			new AbstractMap.SimpleEntry<>("DES_TASK_ORD", "taskOrder"),
//    			new AbstractMap.SimpleEntry<>("COD_FLE_SERV", "codFleetService"),
//    			new AbstractMap.SimpleEntry<>("COD_CRED_RAT", "codCreditRating"),
//    			new AbstractMap.SimpleEntry<>("QRA_RAT_DT", "ratingDate"),
//    			new AbstractMap.SimpleEntry<>("COD_ANN_FEE", "codAnnualFee"),
//    			new AbstractMap.SimpleEntry<>("COD_FEE_MON", "codMonthFee"),
//    			new AbstractMap.SimpleEntry<>("COD_CARD_VER", "codCardVerif"),
//    			new AbstractMap.SimpleEntry<>("XTI_CHCK", "checkIndicator"),
//    			new AbstractMap.SimpleEntry<>("QNU_LST_STOL", "LostStolen"),
//    			new AbstractMap.SimpleEntry<>("QNU_CHAR_OFF", "chargeOff"),
//    			new AbstractMap.SimpleEntry<>("IMP_CH_OF_AM", "chargeOffAMN"),
//    			new AbstractMap.SimpleEntry<>("COD_TRN_ACC", "transferAcc"),
//    			new AbstractMap.SimpleEntry<>("COD_PHONE_TP", "phoneType"),
//    			new AbstractMap.SimpleEntry<>("DES_EMBOSSL1", "embossline1"),
//    			new AbstractMap.SimpleEntry<>("DES_EMBOSSL2", "embossline2"),
//    			new AbstractMap.SimpleEntry<>("QNU_L_CRD_LI", "lCreditLimit"),
//    			new AbstractMap.SimpleEntry<>("QNU_MAINT_DT", "maintDate"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_FD1", "optionalField1"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_FD2", "optionalField2"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_FD3", "optionalField3"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_FD4", "optionalField4")
    		);
    	
    	Map<String, DataType> castColumns = Map.ofEntries(
    			new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("effectiveDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("accountOpen", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("accountClose", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("accountExpired", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("cardType", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("spendingLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("statementType", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("lastRevision", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("transLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("corporatePay", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("dailyLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("cycleLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("cashLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("statusCode", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("reasonStatus", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("statusDate", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codPrFoundedInd", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codCityPairProg", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codFleetService", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("ratingDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codAnnualFee", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codMonthFee", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codCardVerif", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("checkIndicator", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codAccountType", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("LostStolen", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("chargeOff", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("chargeOffAMN", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("lCreditLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("maintDate", DataTypes.LongType)
    		);
    	
    	Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
    	
    	while(renamedColumns.hasNext()) {
    		Map.Entry<String, String> entry = renamedColumns.next();
    		firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
    	}
    	
    	Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
    	
    	while(columnsCasted.hasNext()) {
    		
    		Map.Entry<String, DataType> entry = columnsCasted.next();
    		firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
    		
    	}
    	
    	return firstIn;
    }
    
	public static Dataset<Row> reOrderCTDataset (Dataset<Row> firstIn){
    	
    	Map<String, String> columnsRenamed = Map.ofEntries(
    			//new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
    			new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
    			//new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
    			//new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
    			//new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
    			new AbstractMap.SimpleEntry<>("COD_ACC_NUM", "codAccountNumber"),
    			new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
    			new AbstractMap.SimpleEntry<>("QNU_POST_DT", "postingDate"),
    			new AbstractMap.SimpleEntry<>("COD_TRANS_NM", "codTransaction"),
    			new AbstractMap.SimpleEntry<>("COD_SEQ_TRAN", "codSeqFich"),
    			new AbstractMap.SimpleEntry<>("COD_PERIOD_N", "codPeriod"),
    			new AbstractMap.SimpleEntry<>("COD_CARD_TP", "codCardTp"),
    			new AbstractMap.SimpleEntry<>("COD_ACQU_BIN", "codAcquiringBin"),
    			new AbstractMap.SimpleEntry<>("COD_CARD_ACC", "codCardAcceptor"),
    			new AbstractMap.SimpleEntry<>("DES_SUPP_NM", "supplierName"),
    			new AbstractMap.SimpleEntry<>("DES_SUPP_CTY", "supplierCity"),
    			new AbstractMap.SimpleEntry<>("COD_SUPP_STA", "supplierState"),
    			new AbstractMap.SimpleEntry<>("COD_ISO_CTRY", "codIsoCountry"),
    			new AbstractMap.SimpleEntry<>("COD_SUPP_PT", "codSupplrPostal"),
    			new AbstractMap.SimpleEntry<>("IMP_SOUR_AMT", "sourceAmount"),
    			new AbstractMap.SimpleEntry<>("IMP_BILL_AMT", "billingAmount"),
    			new AbstractMap.SimpleEntry<>("COD_SOU_CURR", "codSourceCurrency"),
    			new AbstractMap.SimpleEntry<>("COD_MERC_CAT", "merchantCat"),
    			new AbstractMap.SimpleEntry<>("COD_TRN_TYPE", "codTransType"),
    			new AbstractMap.SimpleEntry<>("COD_BILL_CUR", "codBillingCurrency"),
    			new AbstractMap.SimpleEntry<>("QNU_AUTH_DT", "authDate"),
    			new AbstractMap.SimpleEntry<>("IMP_TAX_AMT", "taxAmount")
//    			new AbstractMap.SimpleEntry<>("QNU_TRN_DT", "transDate"),   			
//    			new AbstractMap.SimpleEntry<>("IMP_DISP_AMT", "disputeAmount"),
//    			new AbstractMap.SimpleEntry<>("DES_DREASON", "disputeRea"),
//    			new AbstractMap.SimpleEntry<>("QNU_DISDT", "disputeDate"),
//    			new AbstractMap.SimpleEntry<>("COD_COMMODIT", "codCommodity"),
//    			new AbstractMap.SimpleEntry<>("COD_SUPP_VAT", "supplierVat"),
//    			new AbstractMap.SimpleEntry<>("DES_ORDER_NM", "orderNum"),
//    			new AbstractMap.SimpleEntry<>("DES_CUST_VAT", "customerVat"),
//    			new AbstractMap.SimpleEntry<>("IMP_VAT_AMT", "VATAmount"),
//    			new AbstractMap.SimpleEntry<>("IMP_TAX2_AMT", "taxAmount2"),
//    			new AbstractMap.SimpleEntry<>("COD_PUR_FORM", "purchaseFormt"),
//    			new AbstractMap.SimpleEntry<>("COD_CUSTOMR", "codeCustomer"),
//    			new AbstractMap.SimpleEntry<>("COD_PURCH_ID", "purchaseId"),
//    			new AbstractMap.SimpleEntry<>("QNU_TRN_TIME", "transTime"),
//    			new AbstractMap.SimpleEntry<>("COD_TAX_AMT", "codTaxAmount"),
//    			new AbstractMap.SimpleEntry<>("COD_TAX_AMT2", "codTaxAmount2"),
//    			new AbstractMap.SimpleEntry<>("COD_ORD_TYPE", "codOrderType"),
//    			new AbstractMap.SimpleEntry<>("COD_MSSGE", "codMessage"),
//    			new AbstractMap.SimpleEntry<>("DES_PROC_ADD", "processorAdd"),
//    			new AbstractMap.SimpleEntry<>("COD_MER_PROF", "merchantProf"),
//    			new AbstractMap.SimpleEntry<>("COD_USAGE", "codUsage"),
//    			new AbstractMap.SimpleEntry<>("COD_ENR_TRN", "enrichedTrans"),
//    			new AbstractMap.SimpleEntry<>("COD_BILL_ACC", "codBillingAccount"),
//    			new AbstractMap.SimpleEntry<>("QNU_DDA_NUM", "ddaNumber"),
//    			new AbstractMap.SimpleEntry<>("QNU_DD_SA_NM", "ddaSavingNum"),
//    			new AbstractMap.SimpleEntry<>("COD_DISP_STA", "codDisputeStatus"),
//    			new AbstractMap.SimpleEntry<>("COD_MATC_IND", "matchedInd"),
//    			new AbstractMap.SimpleEntry<>("QNU_ROUT_NUM", "rountungNum"),
//    			new AbstractMap.SimpleEntry<>("COD_AUTHORIZ", "codAuthorization"),
//    			new AbstractMap.SimpleEntry<>("COD_TRN_APPR", "codTransApproval"),
//    			new AbstractMap.SimpleEntry<>("COD_EXTRC_ID", "codExtract"),
//    			new AbstractMap.SimpleEntry<>("COD_MEMO_PST", "codMemoPost"),
//    			new AbstractMap.SimpleEntry<>("QNU_STTEM_DT", "statementDate"),
//    			new AbstractMap.SimpleEntry<>("COD_USR_DAT1", "codUserData1"),
//    			new AbstractMap.SimpleEntry<>("DES_UDA_DES1", "userDataDes1"),
//    			new AbstractMap.SimpleEntry<>("COD_USR_DAT2", "codUserData2"),
//    			new AbstractMap.SimpleEntry<>("DES_UDA_DES2", "userDataDes2"),
//    			new AbstractMap.SimpleEntry<>("COD_USR_DAT3", "codUserData3"),
//    			new AbstractMap.SimpleEntry<>("DES_UDA_DES3", "userDataDes3"),
//    			new AbstractMap.SimpleEntry<>("COD_USR_DAT4", "codUserData4"),
//    			new AbstractMap.SimpleEntry<>("DES_UDA_DES4", "userDataDes4"),
//    			new AbstractMap.SimpleEntry<>("COD_USR_DAT5", "codUserData5"),
//    			new AbstractMap.SimpleEntry<>("DES_UDA_DES5", "userDataDes5"),
//    			new AbstractMap.SimpleEntry<>("COD_VISA_BAT", "codVisaBatch"),
//    			new AbstractMap.SimpleEntry<>("COD_LI_IT_ID", "codLineItemId"),
//    			new AbstractMap.SimpleEntry<>("COD_ISS_DEF", "codIssuerDefined"),
//    			new AbstractMap.SimpleEntry<>("DES_SRCE", "source"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_FD", "optionalField"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_F2", "optionalField2"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_F3", "optionalField3"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT_F4", "optionalField4"),
//    			new AbstractMap.SimpleEntry<>("DES_RESEV_F1", "reservedField"),
//    			new AbstractMap.SimpleEntry<>("DES_RESEV_F2", "reservedField2"),
//    			new AbstractMap.SimpleEntry<>("DES_RESEV_F3", "reservedField3"),
//    			new AbstractMap.SimpleEntry<>("DES_RESEV_F4", "reservedField4"),
//    			new AbstractMap.SimpleEntry<>("COD_TRN_ID", "codTransactionId"),
//    			new AbstractMap.SimpleEntry<>("IMP_TIP", "tip"),
//    			new AbstractMap.SimpleEntry<>("IMP_FEE_AMT", "feeAmount"),
//    			new AbstractMap.SimpleEntry<>("DES_FEE_DESC", "feeDesc"),
//    			new AbstractMap.SimpleEntry<>("QNU_FORG_EXC", "foreingExc"),
//    			new AbstractMap.SimpleEntry<>("QNU_EXCH_RT", "exchangeRate"),
//    			new AbstractMap.SimpleEntry<>("IMP_VOLM_LOC", "impVolmLoc"),
//    			new AbstractMap.SimpleEntry<>("IMP_VOLM_USD", "impVolmUsd"),
//    			new AbstractMap.SimpleEntry<>("COD_SEQ_ORIG", "codSeqOrigen"),   
//    			new AbstractMap.SimpleEntry<>("TIM_PRIM_REG", "timPrimReg"),
//    			new AbstractMap.SimpleEntry<>("COD_SRV_ID", "codSrvId")
    		);
    	
    	Map<String, DataType> castColumns = Map.ofEntries(
    			new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("postingDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codSeqFich", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codPeriod", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codCardTp", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codAcquiringBin", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("sourceAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("billingAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codSourceCurrency", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("merchantCat", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("transDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codBillingCurrency", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("taxAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("disputeAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("disputeDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("VATAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("taxAmount2", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("transTime", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codTaxAmount", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codTaxAmount2", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codOrderType", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codUsage", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("ddaNumber", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("ddaSavingNum", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("matchedInd", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("rountungNum", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codExtract", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codMemoPost", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("statementDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("authDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codLineItemId", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codTransactionId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("tip", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("feeAmount", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("foreingExc", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("exchangeRate", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("impVolmLoc", DataTypes.DoubleType),
    			new AbstractMap.SimpleEntry<>("impVolmUsd", DataTypes.DoubleType),
    			new AbstractMap.SimpleEntry<>("codSeqOrigen", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("timPrimReg", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("codSrvId", DataTypes.IntegerType)
    			);
    	
    	Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
    	
    	while(renamedColumns.hasNext()) {
    		Map.Entry<String, String> entry = renamedColumns.next();
    		firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
    	}
    	
    	Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
    	
    	while(columnsCasted.hasNext()) {
    		
    		Map.Entry<String, DataType> entry = columnsCasted.next();
    		firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
    		
    	}
    	
    	return firstIn;
    }

	public static Dataset<Row> reOrderCOMDataset (Dataset<Row> firstIn){
    	
    	Map<String, String> columnsRenamed = Map.ofEntries(
    			//new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
    			new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
    			//new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
    			//new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
    			//new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
    			new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
    			new AbstractMap.SimpleEntry<>("DES_COMP_NM", "companyName"),
    			new AbstractMap.SimpleEntry<>("DES_COM_ADL1", "addressLine1"),
    			new AbstractMap.SimpleEntry<>("DES_COM_ADL2", "addressLine2"),
    			new AbstractMap.SimpleEntry<>("DES_CITY_C", "cicty"),
    			new AbstractMap.SimpleEntry<>("COD_STATE_C", "state"),
    			new AbstractMap.SimpleEntry<>("COD_ISO_CTRC", "codIsoCountry"),
    			new AbstractMap.SimpleEntry<>("COD_COM_POST", "codPostal"),
    			//new AbstractMap.SimpleEntry<>("QNU_FIS_YEAR", "fiscalYear"),
    			//new AbstractMap.SimpleEntry<>("IMP_SPEN_LIM", "spendingLimit"),
    			new AbstractMap.SimpleEntry<>("COD_CARD_TP", "codCardTypeCOM"),
    			new AbstractMap.SimpleEntry<>("DES_EMAIL_C", "email")
    			
//    			new AbstractMap.SimpleEntry<>("DES_ISSU_NM", "issuerName"),
//    			new AbstractMap.SimpleEntry<>("COD_ORG_HIER", "codOrgHier"),
//    			new AbstractMap.SimpleEntry<>("QNU_EFFEC_DT", "effectiveDate"),
//    			new AbstractMap.SimpleEntry<>("DES_COM_ADL3", "addressLine3"),
//    			new AbstractMap.SimpleEntry<>("COD_FED_ORG", "codFederalOrg"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT1", "optional1"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT2", "optional2"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT3", "optional3"),
//    			new AbstractMap.SimpleEntry<>("DES_OPT4", "optional4"),
//    			new AbstractMap.SimpleEntry<>("COD_PRIM_IND", "codPrimryIndustry"),
//    			new AbstractMap.SimpleEntry<>("COD_SEC_IND", "codSecIndustry"),
//    			new AbstractMap.SimpleEntry<>("COD_SCTOR", "codSector"),
//    			new AbstractMap.SimpleEntry<>("DES_WEB_URL", "webSiteURL"),
//    			new AbstractMap.SimpleEntry<>("QNU_BRADSTR", "bradStreet"),
//    			new AbstractMap.SimpleEntry<>("DES_PRIM_CON", "prymarContact"),
//    			new AbstractMap.SimpleEntry<>("DES_LAST_CON", "lastContact"),
    			
    		);
    	
    	Map<String, DataType> castColumns = Map.ofEntries(
    			new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
    			new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codIsoCountry", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("fiscalYear", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("spendingLimit", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codCardTypeCOM", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codOrgHier", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("effectiveDate", DataTypes.LongType),
    			new AbstractMap.SimpleEntry<>("codFederalOrg", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codPrimryIndustry", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("codSecIndustry", DataTypes.IntegerType),
    			new AbstractMap.SimpleEntry<>("bradStreet", DataTypes.LongType)
    		);
    	
    	Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
    	
    	while(renamedColumns.hasNext()) {
    		Map.Entry<String, String> entry = renamedColumns.next();
    		firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
    	}
    	
    	Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
    	
    	while(columnsCasted.hasNext()) {
    		
    		Map.Entry<String, DataType> entry = columnsCasted.next();
    		firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
    		
    	}
    	
    	return firstIn;
    }
	
	public static Dataset<Row> reOrderORDataset (Dataset<Row> firstIn){
		
		Map<String, String> columnsRenamed = Map.ofEntries(
//				new AbstractMap.SimpleEntry<>("AUD_TIM", "fechaAud"),
    			new AbstractMap.SimpleEntry<>("COD_HEAD_ID", "idHeader"),
//    			new AbstractMap.SimpleEntry<>("COD_USER", "codUser"),
//    			new AbstractMap.SimpleEntry<>("TIM_FIRST_IN", "fechaFirstIn"),
//				new AbstractMap.SimpleEntry<>("COD_LOAD_TR", "codCarga"),
				new AbstractMap.SimpleEntry<>("COD_COMP_ID", "codCompId"),
				new AbstractMap.SimpleEntry<>("COD_HIER_NOD", "hierarchyNode"),
				new AbstractMap.SimpleEntry<>("DES_PAR_HCHY", "parntHierrchy"),
				new AbstractMap.SimpleEntry<>("QNU_EFFEC_DT", "effectiveDate")
//				new AbstractMap.SimpleEntry<>("DES_DESCR_O", "description"),
//				new AbstractMap.SimpleEntry<>("DES_MANLSTNM", "managerLastNA"),
//				new AbstractMap.SimpleEntry<>("DES_MAN_NM", "managerName"),
//				new AbstractMap.SimpleEntry<>("DES_MAN_TIT", "managerTitle"),
//				new AbstractMap.SimpleEntry<>("QNU_MAN_TRAI", "managerTrining"),
//				new AbstractMap.SimpleEntry<>("DES_MAN_PHNM", "managerPhoneNumber"),
//				new AbstractMap.SimpleEntry<>("DES_TASK_ORD", "taskOrderNum"),
//				new AbstractMap.SimpleEntry<>("DES_CONT_NM", "contactName"),
//				new AbstractMap.SimpleEntry<>("DES_CON_LANM", "contactLastName"),
//				new AbstractMap.SimpleEntry<>("DES_CON_ADL1", "addressLine1"),
//				new AbstractMap.SimpleEntry<>("DES_CON_ADL2", "addressLine2"),
//				new AbstractMap.SimpleEntry<>("DES_CON_ADL3", "addressLine3"),
//				new AbstractMap.SimpleEntry<>("DES_CONT_CTY", "contactCity"),
//				new AbstractMap.SimpleEntry<>("DES_CONT_STA", "contactState"),
//				new AbstractMap.SimpleEntry<>("DES_CON_CTR", "codContactCountry"),
//				new AbstractMap.SimpleEntry<>("COD_CON_POST", "codPostal"),
//				new AbstractMap.SimpleEntry<>("DES_CONT_PNE", "contactPhoneNumber"),
//				new AbstractMap.SimpleEntry<>("DES_CONT_FAX", "contactFax"),
//				new AbstractMap.SimpleEntry<>("DES_CONTMAIL", "contactEmail"),
//				new AbstractMap.SimpleEntry<>("DES_HIERR_N2", "hierrchyNode2"),
//				new AbstractMap.SimpleEntry<>("DES_HIERR_N3", "hierrchyNode3"),
//				new AbstractMap.SimpleEntry<>("COD_TREE_ID", "treeId"),
//				new AbstractMap.SimpleEntry<>("DES_COST_CNT", "costCenter"),
//				new AbstractMap.SimpleEntry<>("DES_GL_SUBAC", "glSubAccount"),
//				new AbstractMap.SimpleEntry<>("DES_OPT_FD1", "optionalField1"),
//				new AbstractMap.SimpleEntry<>("DES_OPT_FD2", "optionalField2"),
//				new AbstractMap.SimpleEntry<>("DES_OPT_FD3", "optionalField3"),
//				new AbstractMap.SimpleEntry<>("DES_OPT_FD4", "optionalField4")
				);
		
		Map<String, DataType> castColumns = Map.ofEntries(
				new AbstractMap.SimpleEntry<>("fechaAud", DataTypes.TimestampType),
				new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
				new AbstractMap.SimpleEntry<>("idHeader", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("effectiveDate", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("managerTrining", DataTypes.LongType)
				);
		
		Iterator<Map.Entry<String,String>> renamedColumns = columnsRenamed.entrySet().iterator();
		
		while(renamedColumns.hasNext()) {
			Map.Entry<String, String> entry = renamedColumns.next();
			firstIn = firstIn.withColumnRenamed(entry.getKey(), entry.getValue());
		}
		
		Iterator<Map.Entry<String, DataType>> columnsCasted  = castColumns.entrySet().iterator();
		
		while(columnsCasted.hasNext()) {
			
			Map.Entry<String, DataType> entry = columnsCasted.next();
			firstIn = firstIn.withColumn(entry.getKey(), firstIn.col(entry.getKey()).cast(entry.getValue()));
			
		}
		
		return firstIn;
	}
}
