package com.bbva.kgcc.visa.process;

import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import com.bbva.kgcc.processor.visa.entities.GlobalMap;
import com.bbva.kgcc.visa.blocks.FileBlock;

public class ProcessStructureFile {


	public static FileBlock processFileByLine(Map<String, Dataset<Row>> datasetsFromRead) {
		

		
		FileBlock fileBlock = GlobalMap.procesarSource(datasetsFromRead);
		
		

		return fileBlock;

	}

}
