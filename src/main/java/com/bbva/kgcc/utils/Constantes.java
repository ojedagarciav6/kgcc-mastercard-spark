package com.bbva.kgcc.utils;

public class Constantes {
	
	public static final String INPUT_VCF_PERIOD = "period-source.vcf";
	public static final String INPUT_VCF_CARDHOLDER = "cardHolder-source.vcf";
	public static final String INPUT_VCF_ACCOUNTBALANCE = "accountBalance-source.vcf";
	public static final String INPUT_VCF_CARDACCOUNT = "cardAccount-source.vcf";
	public static final String INPUT_VCF_CARDTRANSACTION = "cardTransaction-source.vcf";
	public static final String INPUT_VCF_COMPANY = "company-source.vcf";
	public static final String INPUT_VCF_ORGANIZATION = "organization-source.vcf";
	public static final String INPUT_PERIOD = "aliasPeriod";
	public static final String INPUT_CARDHOLDER = "aliasCardholder";
	public static final String INPUT_ACCOUNTBALANCE = "aliasAccountBalance";
	public static final String INPUT_CARDACCOUNT = "aliasCardAccount";
	public static final String INPUT_CARDTRANSACTION = "aliasCardTransaction";
	public static final String INPUT_COMPANY = "aliasCompany";
	public static final String INPUT_ORGANIZATION = "aliasOrganization";
	public static final String OUTPUT_TARGET = "targetAlias1";
	public static final String SALIDA_ALIAS  = "SALIDA";
	
	/* Datos que nos tiene que proporcionar MasterCard */
	public static final String ICANUMBER = "00000001111";
	public static final String ISSUERNUMBER = "00000001111";
	
	public static final int INCLUIR_VACIOS_FINALES = -1;
	public static final String COMILLAS_DOBLES = "\"";
	public static final String COMILLAS_SIMPLES = "'";
	public static final String TABULACION ="\\t";
	public static final String SIN_ESPACIO = "";
	
	public static final int LINE_TYPE = 0;
	
	//line Type
	public static final int COMPANY_HEADER = 6;
	public static final int HEADER = 8;
	public static final int TRAILER = 9;
	public static final int COMPANY_TRAILER = 7;

}
