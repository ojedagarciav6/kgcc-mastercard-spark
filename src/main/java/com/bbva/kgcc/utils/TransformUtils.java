package com.bbva.kgcc.utils;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;

public class TransformUtils {

	public static Dataset<Row> getGlobalDatasetRow(Map<String, Dataset<Row>> datasetsFromRead) {

		Dataset<Row> datasetPeriod = datasetsFromRead.get(Constantes.INPUT_PERIOD);
		Dataset<Row> datasetCardHolder = datasetsFromRead.get(Constantes.INPUT_CARDHOLDER);
		Dataset<Row> datasetCardAccount = datasetsFromRead.get(Constantes.INPUT_CARDACCOUNT);
		Dataset<Row> datasetCardTransaction = datasetsFromRead.get(Constantes.INPUT_CARDTRANSACTION);
		Dataset<Row> datasetCompany = datasetsFromRead.get(Constantes.INPUT_COMPANY);
		Dataset<Row> datasetOrganization = datasetsFromRead.get(Constantes.INPUT_ORGANIZATION);

		Dataset<Row> allTables = datasetCardTransaction.join(datasetPeriod,
				datasetCardTransaction.col("COD_PERIOD_N").equalTo(datasetPeriod.col("COD_PERIOD_N"))
						.and(datasetCardTransaction.col("COD_COMP_ID").equalTo(datasetPeriod.col("COD_COMP_ID")))
						.and(datasetCardTransaction.col("COD_CARD_TP").equalTo(datasetPeriod.col("COD_CARD_TP"))), "inner")
				.join(datasetCardAccount, datasetCardTransaction.col("COD_ACC_NUM").equalTo(datasetCardAccount.col("COD_ACC_NUM")),	"right_outer")
				.join(datasetCardHolder, datasetCardAccount.col("COD_CARDHOLD").equalTo(datasetCardHolder.col("COD_CARDHOLD"))
						.and(datasetCardTransaction.col("COD_COMP_ID").equalTo(datasetCardHolder.col("COD_COMP_ID"))), "left_outer")
				.join(datasetCompany, datasetCardTransaction.col("COD_COMP_ID").equalTo(datasetCompany.col("COD_COMP_ID")) 
						.and(datasetCardTransaction.col("COD_CARD_TP").equalTo(datasetCompany.col("COD_CARD_TP"))), "inner")
				.join(datasetOrganization, datasetCardTransaction.col("COD_COMP_ID").equalTo(datasetCompany.col("COD_COMP_ID"))
						.and(datasetCardAccount.col("COD_HIER_NOD").equalTo(datasetOrganization.col("COD_HIER_NOD"))), "inner");

		Dataset<Row> selectedColumns = allTables.select(datasetCardTransaction.col("COD_COMP_ID").as("codCompId"),//0
				datasetCardTransaction.col("QNU_POST_DT").as("postingDate"),
				datasetCardTransaction.col("COD_TRANS_NM").as("codTransaction"),
				datasetCardTransaction.col("COD_SEQ_TRAN").as("codSeqFich"),
				datasetCardTransaction.col("COD_CARD_TP").as("codCardTp"),
				datasetCardTransaction.col("COD_ACQU_BIN").as("codAcquiringBin"),//5
				datasetCardTransaction.col("COD_CARD_ACC").as("codCardAcceptor"),
				datasetCardTransaction.col("DES_SUPP_NM").as("supplierName"),
				datasetCardTransaction.col("DES_SUPP_CTY").as("supplierCity"),
				datasetCardTransaction.col("COD_SUPP_STA").as("supplierState"),
				datasetCardTransaction.col("COD_ISO_CTRY").as("codIsoCountryCTR"),//10
				datasetCardTransaction.col("COD_SUPP_PT").as("codSupplrPostal"),
				datasetCardTransaction.col("IMP_SOUR_AMT").as("sourceAmount"),
				datasetCardTransaction.col("IMP_BILL_AMT").as("billingAmount"),
				datasetCardTransaction.col("COD_SOU_CURR").as("codSourceCurrency"),
				datasetCardTransaction.col("COD_MERC_CAT").as("merchantCat"),//15
				datasetCardTransaction.col("COD_TRN_TYPE").as("codTransType"),
				datasetCardTransaction.col("COD_BILL_CUR").as("codBillingCurrency"),
				datasetCardTransaction.col("QNU_TRN_DT").as("transactionDate"),
				datasetCardTransaction.col("QNU_AUTH_DT").as("authDate"),
				datasetCardTransaction.col("IMP_TAX_AMT").as("taxAmount"),//20
				datasetCardAccount.col("TIM_FIRST_IN").as("fechaFirstIn"),
				datasetCardAccount.col("COD_CARDHOLD").as("codCarHolder"),
				datasetCardAccount.col("COD_ACC_NUM").as("accountNum"),
				datasetCardAccount.col("QNU_EFFEC_DT").as("effectiveDate"),
				datasetCardAccount.col("COD_STA_COD").as("statusCode"),//25
				datasetCardAccount.col("COD_REAS_STA").as("reasonStatus"),
				datasetCardAccount.col("COD_CARD_TP").as("cardType"),
				datasetCardAccount.col("COD_ACC_TYPE").as("codAccountType"),
				datasetCardAccount.col("QNU_STAT_TP").as("statementType"),
				datasetCardAccount.col("QNU_CARD_EXP").as("accountExpired"),//30
				datasetCardHolder.col("COD_CARDHOLD").as("idCardHolder"),
				datasetCardHolder.col("DES_FIRST_NM").as("firstName"),
				datasetPeriod.col("COD_PERIOD_N").as("codPeriod"), datasetPeriod.col("QNU_START_DT").as("startDate"),
				datasetPeriod.col("QNU_END_DATE").as("endDate"),//35
				datasetPeriod.col("COD_PER_COMP").as("codPeriodComplet"),
				datasetCompany.col("DES_COMP_NM").as("companyName"),
				datasetCompany.col("DES_COM_ADL1").as("addressLine1"),
				datasetCompany.col("DES_COM_ADL2").as("addressLine2"), datasetCompany.col("DES_CITY_C").as("cicty"),//40
				datasetCompany.col("COD_STATE_C").as("state"), datasetCompany.col("COD_ISO_CTRC").as("codIsoCountry"),
				datasetCompany.col("COD_COM_POST").as("codPostal"), datasetCompany.col("DES_EMAIL_C").as("email"),
				datasetCompany.col("DES_ISSU_NM").as("issueName"),//45
				datasetOrganization.col("COD_HIER_NOD").as("hierarchyNode"),
				datasetOrganization.col("DES_PAR_HCHY").as("parntHierrchy"));

		return selectedColumns;

	}
	
	public static Dataset<Row> columnsDataTypesCast(Dataset<Row> allTables){
		
		Map<String, DataType> columnsDataypes = Map.ofEntries(
		    	new AbstractMap.SimpleEntry<>("codCompId", DataTypes.LongType),
		    	new AbstractMap.SimpleEntry<>("postingDate", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("codSeqFich", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("codPeriod", DataTypes.IntegerType),
				new AbstractMap.SimpleEntry<>("codCardTp", DataTypes.IntegerType),
				new AbstractMap.SimpleEntry<>("codAcquiringBin", DataTypes.IntegerType),
				new AbstractMap.SimpleEntry<>("sourceAmount", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("billingAmount", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("codSourceCurrency", DataTypes.IntegerType),
				new AbstractMap.SimpleEntry<>("merchantCat", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("codBillingCurrency", DataTypes.IntegerType),
				new AbstractMap.SimpleEntry<>("taxAmount", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("authDate", DataTypes.LongType),    	
		    	new AbstractMap.SimpleEntry<>("statementType", DataTypes.IntegerType),
		    	new AbstractMap.SimpleEntry<>("effectiveDate", DataTypes.LongType),
		    	new AbstractMap.SimpleEntry<>("fechaFirstIn", DataTypes.TimestampType),
		    	new AbstractMap.SimpleEntry<>("startDate", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("endDate", DataTypes.LongType),
				new AbstractMap.SimpleEntry<>("codPeriodComplet", DataTypes.IntegerType));
		
		Iterator<Map.Entry<String, DataType>> iteratorDatatypes = columnsDataypes.entrySet().iterator();
    	while (iteratorDatatypes.hasNext()) {
    		Map.Entry<String, DataType> entry = iteratorDatatypes.next();
    		allTables = allTables.withColumn(entry.getKey(), allTables.col(entry.getKey()).cast(entry.getValue()));                        
    	}
		
		return allTables;
	}

}
